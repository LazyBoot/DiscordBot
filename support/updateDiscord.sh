#! /bin/bash

FILE="/var/lib/docker/volumes/discordbot-data/_data/startUpdate"
LOCKFILE="/run/updateDiscord.lock"


function exitError {
        rm $LOCKFILE
        if [ -e $FILE ] ; then
                rm $FILE
        fi
        exit 1
}

if [ -e $LOCKFILE ] ; then
        echo "Update already running"
        exit 1
fi

if [ ! -s $FILE ] && [ "$1" != "force" ] ; then
        echo "Update not requested. Exiting."
        exit 0
fi

touch $LOCKFILE

OLDCONTAINER=$(docker ps -a -q --filter ancestor=registry.gitlab.com/lazyboot/discordbot --format="{{.ID}}")

echo "Trying to pull image..."
export LANG=C
if docker pull registry.gitlab.com/lazyboot/discordbot | grep "Image is up to date"; then
        echo "Pull failed. Exiting."
        exitError
fi

/home/lazy/startDiscordbot.sh
RETCODE="$?"
if [ "$RETCODE" -ne 0 ] ; then
        echo "Failed to start bot again. Exiting."
        exitError
fi

docker rm $(docker stop $OLDCONTAINER)
RETCODE="$?"
if [ "$RETCODE" -ne 0 ] ; then
        echo "Failed to stop container. Exiting."
        exitError
fi

if [ -e $FILE ] ; then
        rm $FILE
fi

if [ -e $LOCKFILE ] ; then
        rm $LOCKFILE
fi
echo "Update success. Removed update flag-file."
