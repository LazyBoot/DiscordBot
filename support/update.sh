#! /bin/sh

set -x
#set -e

cd /srv/discordbot/DiscordBot || exit 1

LASTBUILDGITHEADFILE=".last-successful-build-hash"

# if [ -z "$(find "$LASTBUILDGITHEADFILE" -mmin -120 2> /dev/null)" -o "$1" = "force" ] ; then
        git pull origin master
# fi

LASTBUILD="$(cat "$LASTBUILDGITHEADFILE" 2> /dev/null || true)"
HEADHASH="$(cat .git/refs/heads/master)"

if [ "$LASTBUILD" = "$HEADHASH" ] && [ "$1" != "force" ] ; then
        echo "No changes. Exiting"
        exit 0
fi

dotnet publish LazyDiscordBot.sln -c Release
RETCODE="$?"
echo "Return code from dotnet: $RETCODE"

if [ "$RETCODE" -ne 0 ] ; then
	echo "Failed to build. Exiting"
	exit 1
fi

DOTNETPID="$(cat /run/discordbot/discordbot.pid 2> /dev/null || true)"

#[ -d last-build ] && rm -rf last-build
cp -axTf LazyDiscordBot/bin/Debug/netcoreapp2.1/publish last-build
rm last-build/opus.dll last-build/libsodium.dll

if [ "$(readlink "/proc/$DOTNETPID/exe" 2> /dev/null | xargs -r basename)" = "dotnet" ] ; then
	kill "$DOTNETPID"
fi

echo "$HEADHASH" > "$LASTBUILDGITHEADFILE"

