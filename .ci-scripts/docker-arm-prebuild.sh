#!/bin/sh

# downloads a local copy of qemu on docker-hub build machines
curl -L https://github.com/balena-io/qemu/releases/download/v3.0.0%2Bresin/qemu-3.0.0+resin-arm.tar.gz | tar zxvf - -C .
mv -v qemu-3.0.0+resin-arm/qemu-arm-static ./

# Register qemu-*-static for all supported processors except the 
# current one, but also remove all registered binfmt_misc before
docker run --rm --privileged multiarch/qemu-user-static:register --reset
