using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "newtimeout")]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "newtimeout", ErrorMessage = "")]
    [RequireBotPermission(GuildPermission.ModerateMembers)]
    [Group("newtimeout")]
    [Alias("new-timeout")]
    public class NewTimeoutModule : GuildEnabler<NewTimeoutService>
    {
        public NewTimeoutModule(NewTimeoutService service) : base(service)
        {
        }

        public override Task Disable() => base.Disable();
        public override Task Enable() => base.Enable();
        public override Task ShowStatus() => base.ShowStatus();
    }
}
