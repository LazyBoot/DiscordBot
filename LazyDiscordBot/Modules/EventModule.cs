﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LiteDB;

namespace LazyDiscordBot.Modules
{
    [Group("event")]
    [RequireContext(ContextType.Guild)]
    public class EventModule : ModuleBase<SocketCommandContext>
    {
        private readonly ILiteCollection<CustomEvent> _eventCollection;
        private readonly CommandService _commandService;

        public EventModule(LiteDatabase db, CommandService commandService)
        {
            _commandService = commandService;
            _eventCollection = db.GetCollection<CustomEvent>("CustomEvents");
            var weekAgo = DateTime.UtcNow.AddDays(-7);
            _eventCollection.DeleteMany(e => e.Time < weekAgo);
        }

        [Command, Priority(1)]
        public Task ListEvents()
        {
            var events = _eventCollection.FindAll()?.ToArray();

            if (events == null || !events.Any())
                return ReplyAsync("No events set for this guild");

            var upcomingEvents = events.Where(e => e.Time > DateTime.UtcNow.AddHours(-5)).ToArray();

            if (upcomingEvents.Length == 0)
                return ReplyAsync("No upcoming events set for this guild, use `event list` to show all");

            var builder = new StringBuilder("Upcoming events: ```\n");
            foreach (var customEvent in upcomingEvents.OrderBy(e => e.Time))
            {
                builder.AppendLine(FormatEvent(customEvent));
            }

            builder.Append("```");

            return ReplyAsync(builder.ToString());
        }

        [Command, Priority(20)]
        public async Task<RuntimeResult> ShowEvent([Remainder] string eventName)
        {
            var @event = FindEvent(eventName);
            if (@event == null)
                return CommandResult.FromError("No event by that name");

            var embed = EmbedBuilder(@event);

            await ReplyAsync(embed: embed.Build());
            return CommandResult.FromSuccess();
        }

        [Command("all"), Priority(30)]
        [Alias("list")]
        public Task ShowUpcoming()
        {
            var events = _eventCollection.FindAll()?.ToArray();

            if (events == null || !events.Any())
                return ReplyAsync("No events set for this guild");

            var builder = new StringBuilder("All events: ```\n");
            foreach (var customEvent in events.OrderBy(e => e.Name))
            {
                builder.AppendLine(FormatEvent(customEvent));
            }

            builder.Append("```");

            return ReplyAsync(builder.ToString());

        }

        [Command("add"), Priority(30)]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task<RuntimeResult> AddEvent(DateTime time, string eventName, [Remainder] string eventTitle)
        {
            var thisModule = _commandService.Modules.FirstOrDefault(m => m.Group == "event");

            if (thisModule == null)
                return CommandResult.FromError("Weird error");

            if (thisModule.Commands.Any(c=>c.Name.Equals(eventName, StringComparison.OrdinalIgnoreCase) || c.Aliases.Contains(eventName, StringComparer.OrdinalIgnoreCase)))
                return CommandResult.FromError("Reserved keyword");

            var @event = FindEvent(eventName);
            if (@event != null)
                return CommandResult.FromError("Event already exists");

            var newEvent = new CustomEvent()
            {
                GuildId = Context.Guild.Id,
                Name = eventName,
                Title = eventTitle,
                Time = time
            };

            _eventCollection.Insert(newEvent);

            var embed = EmbedBuilder(newEvent);

            await ReplyAsync(embed: embed.Build());
            return CommandResult.FromSuccess();
        }

        [Command("remove"), Priority(30)]
        [Alias("delete", "del")]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task<RuntimeResult> DeleteEvent([Remainder] string eventName)
        {
            var @event = FindEvent(eventName);
            if (@event == null)
                return CommandResult.FromError("No event by that name");

            _eventCollection.Delete(@event.GuildId + @event.Name);

            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess();
        }

        private CustomEvent FindEvent(string eventName)
        {
            return _eventCollection.FindOne(e => e.GuildId == Context.Guild.Id && e.Name.Equals(eventName, StringComparison.OrdinalIgnoreCase));
        }

#pragma warning disable CA1822 // Mark members as static
        private string FormatEvent(CustomEvent customEvent)
        {
            return $"{customEvent.Time.ToUniversalTime():u}: {customEvent.Name} - {customEvent.Title}";
        }

        private EmbedBuilder EmbedBuilder(CustomEvent @event)
        {
            var embed = new EmbedBuilder()
                        .WithTitle(@event.Title)
                        .WithTimestamp(@event.Time);
            return embed;
        }
#pragma warning restore CA1822 // Mark members as static
    }

    public class CustomEvent : IEquatable<CustomEvent>
    {
        [BsonId] public string BsonId => GuildId + Name;
        public string Name { get; set; }
        public string Title { get; set; }
        public DateTime Time { get; set; }
        public ulong GuildId { get; set; }

        public bool Equals(CustomEvent other)
        {
            return GuildId == other?.GuildId && Name == other.Name;
        }
    }

}
