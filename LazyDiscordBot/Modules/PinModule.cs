using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [Group("pin")]
    [RequireContext(ContextType.Guild)]
    [RequireBotPermission(ChannelPermission.ManageMessages)]
    public class PinModule : InteractiveBase
    {
        public PinService _service { get; set; }

        [Command]
        [Priority(2)]
        [RequireElevatedUser(Group = "pinmodule")]
        [RequirePinPerm(Group = "pinmodule")]
        [RequireUserPermission(ChannelPermission.ManageMessages, Group = "pinmodule")]
        public async Task Pin(ulong messageId)
        {
            if (await Context.Channel.GetMessageAsync(messageId) is IUserMessage message)
            {
                await message.PinAsync();
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
        }

        [Command("unpin")]
        [Priority(5)]
        [RequireElevatedUser(Group = "pinmodule")]
        [RequirePinPerm(Group = "pinmodule")]
        [RequireUserPermission(ChannelPermission.ManageMessages, Group = "pinmodule")]
        public async Task Unpin(ulong messageId)
        {
            if (await Context.Channel.GetMessageAsync(messageId) is IUserMessage message)
            {
                await message.UnpinAsync();
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
        }

        [Command("add")]
        [Priority(5)]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task AddRole(IRole role)
        {
            var result = _service.AddRoleToChannel(Context.Guild.Id, Context.Channel.Id, role.Id);
            if (result)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            else
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
        }

        [Command("remove")]
        [Priority(5)]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task RemoveRole(IRole role)
        {
            var result = _service.RemoveRoleFromChannel(Context.Guild.Id, Context.Channel.Id, role.Id);
            if (result)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            else
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
        }

        [Command("list")]
        [Priority(5)]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task ListRoles()
        {
            var guildChannel = Context.Guild.Id.ToString() + Context.Channel.Id;
            if (_service.Permissions.ContainsKey(guildChannel) && _service.Permissions.Any(p => p.Value.Roles.Any()))
            {
                var sb = new StringBuilder("```\n");
                foreach (var role in _service.Permissions[guildChannel].Roles)
                {
                    sb.AppendLine($"{role} - {Context.Guild.GetRole(role).Name}");
                }
                sb.Append("```");

                await ReplyAsync(sb.ToString());
            }
            else 
            {
                await ReplyAsync("No roles can pin using bot");
            }
        }
    }
}
