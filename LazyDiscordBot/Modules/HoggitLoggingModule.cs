﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireGuild(DoghouseService.Guild)]
    [RequireElevatedUser(Group = "HoggitLoggingModule")]
    [RequireBotPermission(GuildPermission.ManageRoles)]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "HoggitLoggingModule", ErrorMessage = "")]
    [Group("HoggitLogging")]
    public class HoggitLoggingModule : ModuleBase<SocketCommandContext>
    {
        public HoggitLoggerService HoggitLoggerService { get; set; }

        [Command("set")]
        public async Task<RuntimeResult> SetWebhook(string url)
        {
            if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
            { 
                HoggitLoggerService.SetLogWebhook(url);
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                return CommandResult.FromSuccess();
            }

            await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            return CommandResult.FromError();
        }
    }
}
