using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    [Summary("Deals with voice chat")]
    public class AudioModule : ModuleBase<SocketCommandContext>
    {
        // Scroll down further for the AudioService.
        // Like, way down
        private readonly AudioService _service;
        private readonly IHttpClientFactory _httpClient;
        private readonly string _audioPath = Path.Join(Program.ProgramPath, "Data", "Audio");

        // Remember to add an instance of the AudioService
        // to your IServiceCollection when you initialize your bot
        public AudioModule(AudioService service, IHttpClientFactory httpClient)
        {
            _service = service;
            _httpClient = httpClient;
        }

        [Command("join", RunMode = RunMode.Async)]
        [RequireElevatedUser(Group = "audioAdminJoinAnywhere")]
        [RequireUserPermission(GuildPermission.ManageRoles, Group = "audioAdminJoinAnywhere")]
        public async Task<RuntimeResult> JoinCmd([Remainder] IVoiceChannel channel)
        {
            var (result, _) = await _service.JoinAudio(Context, channel);
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }


        // You *MUST* mark these commands with 'RunMode.Async'
        // otherwise the bot will not respond until the Task times out.
        [Command("join", RunMode = RunMode.Async)]
        public async Task<RuntimeResult> JoinCmd()
        {
            var (result, _) = await _service.JoinAudio(Context);
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }

        // Remember to add preconditions to your commands,
        // this is merely the minimal amount necessary.
        // Adding more commands of your own is also encouraged.
        [Command("leave", RunMode = RunMode.Async)]
        public async Task<RuntimeResult> LeaveCmd()
            => await _service.LeaveAudio(Context);

        [Command("stop", RunMode = RunMode.Async)]
        public async Task<RuntimeResult> StopCmd()
        {
            var result = _service.StopAudio();
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }

        [Command("play", RunMode = RunMode.Async), Priority(1)]
        [RequireElevatedUser(Group = "audioAdminPlayAnywhere")]
        [RequireUserPermission(GuildPermission.ManageRoles, Group = "audioAdminPlayAnywhere")]
        public async Task<RuntimeResult> PlayCmd(IVoiceChannel channel, [Remainder] string sound)
        {
            var result = await _service.SendAudioAsync(Context, sound, channel);
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }

        [Command("play", RunMode = RunMode.Async), Priority(0)]
        public async Task<RuntimeResult> PlayCmd([Remainder] string song)
        {
            var result = await _service.SendAudioAsync(Context, song);
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }

        [Command("addsound", RunMode = RunMode.Async)]
        [RequireElevatedUser(Group = "audioAdminAdd")]
        [RequireUserPermission(GuildPermission.ManageRoles, Group = "audioAdminAdd")]
        public async Task<RuntimeResult> AddSound(string name, [Optional, Remainder] string description)
        {
            if (Context.Message.Attachments != null && Context.Message.Attachments.Any())
            {
                var fileName = Guid.NewGuid().ToString();

                var attachment = Context.Message.Attachments.First();

                var bytes = await _httpClient.CreateClient().GetByteArrayAsync(attachment.Url);
                File.WriteAllBytes(Path.Join(_audioPath, fileName), bytes);

                return _service.AddAudioFile(Context.Channel, fileName, name, description);
            }
            else
                return CommandResult.FromError("no file provided");
        }

        [Command("listsounds")]
        public async Task<RuntimeResult> ListSounds()
        {
            var sounds = _service.GetSounds();
            if (sounds.Count == 0)
            {
                return CommandResult.FromError("No sounds defined!");
            }

            var soundNames = new StringBuilder();
            var soundDesc = new StringBuilder();
            var embedBuilder = new EmbedBuilder();
            //embedBuilder.WithTitle("Sounds");

            soundDesc.Append('\u200B');

            foreach (var sound in sounds)
            {
                soundNames.Append(sound.Name + Environment.NewLine);
                soundDesc.Append(sound.Description + Environment.NewLine);
            }

            embedBuilder.AddField(new EmbedFieldBuilder() { Name = "Name", Value = soundNames.ToString(), IsInline = true });
            embedBuilder.AddField(new EmbedFieldBuilder() { Name = "Description", Value = soundDesc.ToString(), IsInline = true });

            //embedBuilder.WithDescription(stringBuilder.ToString());
            //embedBuilder.WithColor(new Color(144, 213, 54));
            await ReplyAsync(string.Empty, embed: embedBuilder.Build());
            return CommandResult.FromSuccess();
        }

        //[Command("spotify", RunMode = RunMode.Async)]
        //public async Task Spotify([Remainder] string song) 
        // => await _service.PlaySpotify(Context.Guild, Context.Channel, song);

        //[Command("youtube", RunMode = RunMode.Async)]
        //public async Task Youtube([Remainder] string song) 
        // => await _service.PlaySpotify(Context.Guild, Context.Channel, song);


        [Command("removesound")]
        [RequireElevatedUser(Group = "audioAdminRemove")]
        [RequireUserPermission(GuildPermission.ManageRoles, Group = "audioAdminRemove")]
        public async Task RemoveSound(string name)
        {
            var audioFile = _service.RemoveSound(name);

            if (audioFile == null)
                return;

            File.Delete(Path.Join(_audioPath, audioFile.FileName));

            await ReplyAsync($"Removed sound {audioFile.Name}");
        }
    }
}
