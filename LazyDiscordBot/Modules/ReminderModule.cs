﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Humanizer;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    [Group("remindme")]
    public class ReminderModule : ModuleBase<SocketCommandContext>
    {
        private readonly ReminderService _service;
        private readonly PrefixService _prefixService;

        public ReminderModule(ReminderService service, PrefixService prefixService)
        {
            _service = service;
            _prefixService = prefixService;
        }

        [Command, Priority(0)]
        public async Task RemindMeHelp()
        {
            await ReplyAsync($"```\nUsage:\n{_prefixService.GetPrefix(Context.Guild?.Id ?? 0)}remindme <timespan> <units> <message>\n\n" +
                             "Valid units are 'days', 'hours', 'minutes', or 'seconds'\n\n" +
                             $"{_prefixService.GetPrefix(Context.Guild?.Id ?? 0)}remindme 3d2h1m <message> is also valid" +
                             "other valid commands: " +
                             "  - list/show: Lists any reminders you've already set" +
                             "  - delete/clear/remove: Deletes all reminders set for you\n```");
        }

        [Command, Priority(19)]
        public async Task<RuntimeResult> RemindMe(TimeSpan time, [Remainder] string message)
        {
            if (time.TotalSeconds < 1)
            {
                return CommandResult.FromError("Please choose a time in the future");
            }

            if (time.TotalDays > 180)
            {
                return CommandResult.FromError("The selected time is far in the future, maybe consider a calendar instead");
            }

            if (string.IsNullOrEmpty(message))
            {
                return CommandResult.FromError("Please provide a message");
            }
            var endTime = DateTime.UtcNow + time;
            var result = _service.AddReminder(Context.User, endTime, message, Context.Message.GetJumpUrl());

            if (result.IsSuccess)
            {
                var reply = $"I will remind you of that in {time.Humanize(3, minUnit: Humanizer.Localisation.TimeUnit.Second, maxUnit: Humanizer.Localisation.TimeUnit.Month)} - At {TimestampTag.FormatFromDateTime(endTime, TimestampTagStyles.ShortDateTime)}";

                await Context.Message.ReplyAsync(reply, allowedMentions: LbAllowedMentions.ReplyOnly);
            }

            return result;
        }

        [Command, Priority(20)]
        public async Task<RuntimeResult> RemindMe(double timeNumber, string timeUnit, [Remainder] string message)
        {
            TimeSpan time;
            if (timeUnit.StartsWith("day", StringComparison.OrdinalIgnoreCase))
                time = TimeSpan.FromDays(timeNumber);
            else if (timeUnit.StartsWith("hour", StringComparison.OrdinalIgnoreCase))
                time = TimeSpan.FromHours(timeNumber);
            else if (timeUnit.StartsWith("min", StringComparison.OrdinalIgnoreCase))
                time = TimeSpan.FromMinutes(timeNumber);
            else if (timeUnit.StartsWith("sec", StringComparison.OrdinalIgnoreCase))
                time = TimeSpan.FromSeconds(timeNumber);
            else
            {
                return CommandResult.FromError("Invalid time unit specified");
            }

            return await RemindMe(time, message);
        }

        [Command("delete"), Priority(20)]
        [Alias("clear", "remove")]
        public async Task<RuntimeResult> DeleteReminders()
        {
            int removed = _service.RemoveRemindersForUser(Context.User);
            if (removed > 0)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                return CommandResult.FromSuccess();
            }

            await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            return CommandResult.FromError("Could not find any reminders to remove");
        }

        [Command("list"), Priority(20)]
        [Alias("show")]
        public Task<RuntimeResult> ListReminders()
            => ListRemindersUser(Context.User);

        [Command("list"), Priority(20)]
        [Alias("show")]
        [RequireElevatedUser] 
        public async Task<RuntimeResult> ListRemindersUser(IUser user)
        {
            var reminders = _service.ListRemindersForUser(user);

            var guildUser = user as IGuildUser;

            if (reminders.Count == 0)
            {
                await ReplyAsync("No reminders found for " + (guildUser?.Nickname ?? user.Username));
                return CommandResult.FromSuccess();
            }

            var stringBuilder = new StringBuilder();

            foreach (var reminder in reminders.OrderBy(r=> r.Time))
            {
                stringBuilder.AppendLine($"{TimestampTag.FormatFromDateTime(reminder.Time, TimestampTagStyles.Relative)} - {reminder.Message}");
            }


            var embed = new EmbedBuilder()
                .WithTitle($"Reminders for {guildUser?.Nickname ?? user.Username}")
                .WithDescription(stringBuilder.ToString());

            await ReplyAsync(string.Empty, embed: embed.Build());
            return CommandResult.FromSuccess();
        }
    }
}
