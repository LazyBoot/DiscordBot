using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Humanizer;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using LazyDiscordBot.TypeReaders;

namespace LazyDiscordBot.Modules
{
    [RequireContext(ContextType.Guild)]
    [RequireElevatedUser(Group = "moderation")]
    [RequireBotPermission(ChannelPermission.ManageChannels)]
    [RequireUserPermission(GuildPermission.ManageMessages, Group = "moderation", ErrorMessage = "")]
    public class ModerationModule : ModuleBase<SocketCommandContext>
    {
        public readonly TimeSpan MaxTime = TimeSpan.FromMinutes(30);

        private readonly HoggitLoggerService _loggerService;

        public ModerationModule(HoggitLoggerService loggerService)
        {
            _loggerService = loggerService;
        }

        [Command("slowmode")]
        [Priority(0)]
        public async Task Slowmode(TimeSpan slowTime)
        {
            if (slowTime > MaxTime)
            {
                await ReplyAsync("Time must be 30 minutes or less");
                return;
            }

            var channel = Context.Channel as ITextChannel;
            await channel.ModifyAsync(c => c.SlowModeInterval = Convert.ToInt32(slowTime.TotalSeconds));

            var logMessage = slowTime.TotalSeconds == 0
                ? $"SLOWMODE: {Context.User.Mention} ({Context.User.ToString()}) disabled slowmode in {(Context.Channel as ITextChannel).Mention} ([go to](<{Context.Message.GetJumpUrl()}>))"
                : $"SLOWMODE: {Context.User.Mention} ({Context.User.ToString()}) enabled slowmode in {(Context.Channel as ITextChannel).Mention} set to {slowTime.Humanize(2)} ([go to](<{Context.Message.GetJumpUrl()}>))";
            var logTask = _loggerService.SendLogMessage(logMessage);

            var reactTask = Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            Task.WaitAll(logTask, reactTask);
        }

        [Command("slowmode")]
        [Priority(5)]
        public Task Slowmode(int slowTime)
            => Slowmode(TimeSpan.FromSeconds(slowTime));

        [Command("GetBan")]
        [Alias("isbanned", "isban")]
        [Priority(10)]
        [RequireBotPermission(GuildPermission.BanMembers)]
        public async Task GetBan([OverrideTypeReader(typeof(LazyUserTypeReader<IUser>))] IUser user)
        {
            var ban = await Context.Guild.GetBanAsync(user);

            await PrintBan(ban);
        }

        [Command("GetBan")]
        [Alias("isbanned", "isban")]
        [Priority(15)]
        [RequireBotPermission(GuildPermission.BanMembers)]
        public async Task GetBan(ulong userId)
        {
            var ban = await Context.Guild.GetBanAsync(userId);

            await PrintBan(ban);
        }

        [Command("GetBan")]
        [Alias("isbanned", "isban")]
        [Priority(0)]
        [RequireBotPermission(GuildPermission.BanMembers)]
        public async Task GetBan(string searchString)
        {
            if (searchString.StartsWith('@'))
                searchString = searchString.Substring(1);

            var allBans = await Context.Guild.GetBansAsync().FlattenAsync();

            var bans = allBans.Where(b => b.User.ToString().Contains(searchString));

            if (!bans.Any())
            {
                await ReplyAsync("No matching bans found");
                return;
            }

            foreach (var ban in bans)
                await PrintBan(ban);
        }

        private async Task PrintBan(IBan ban)
        {
            if (ban is not null)
                await ReplyAsync($"User {ban.User.Mention} ({ban.User.ToString()}) is banned for [{ban.Reason ?? "No reason specified"}]", allowedMentions: AllowedMentions.None);
            else
                await ReplyAsync("User is not banned");
        }
    }
}
