using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "bannotify")]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "bannotify", ErrorMessage = "")]
    [RequireGuild(DoghouseService.Guild)]
    [Group("bannotify")]
    [Alias("ban-notify")]
    public class BanNotifyModule : GuildEnabler<BanNotifyService>
    {
        public BanNotifyModule(BanNotifyService service) : base(service)
        {
        }

        public override Task Disable() => base.Disable();
        public override Task Enable() => base.Enable();
        public override Task ShowStatus() => base.ShowStatus();
    }
}
