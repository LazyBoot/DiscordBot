﻿using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "linkFixer")]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "linkFixer", ErrorMessage = "")]
    [Group("linkfixer")]
    public class LinkFixerModule : GuildEnabler<LinkFixerService>
    {
        public LinkFixerModule(LinkFixerService service) : base(service)
        {
        }

        public override Task Disable() => base.Disable();
        public override Task Enable() => base.Enable();
        public override Task ShowStatus() => base.ShowStatus();
    }
}
