using System;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using static LazyDiscordBot.Services.PictureService;

// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    public class PublicModule : ModuleBase<SocketCommandContext>
    {
        public PictureService PictureService { get; set; }
        public PrefixService PrefixService { get; set; }

        [Command("ping")]
        [Alias("pong", "hello")]
        public Task PingAsync()
            => ReplyAsync("pong!");

        [Command("userinfo")]
        public Task UserInfoAsync()
            => UserInfoAsync(Context.User);

        [Command("unixtime")]
        [Alias("time")]
        public Task UnixTime([Remainder] DateTimeOffset? time = null)
        {
            var unixSeconds = (time ?? DateTimeOffset.UtcNow).ToUnixTimeSeconds();
            return base.ReplyAsync($"<t:{unixSeconds}> (<t:{unixSeconds}:R>) - {unixSeconds}");
        }

        // Get info on a user, or the user who invoked the command if one is not specified
        [Command("userinfo")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        [RequireUserPermission(GuildPermission.BanMembers)]
        [RequireUserPermission(GuildPermission.KickMembers)]
        public async Task UserInfoAsync(IUser user)
        {
            var embed = new EmbedBuilder()
                        .WithAuthor(user)
                        .WithDescription(user.Mention);

            switch (user.Status)
            {
                case UserStatus.Online:
                    embed.WithColor(Color.Green);
                    break;
                case UserStatus.Idle:
                case UserStatus.AFK:
                    embed.WithColor(Color.LightOrange);
                    break;
                case UserStatus.DoNotDisturb:
                    embed.WithColor(Color.Red);
                    break;
                case UserStatus.Offline:
                case UserStatus.Invisible:
                    embed.WithColor(Color.DarkerGrey);
                    break;
            }

            if (user is IGuildUser gUser)
            {
                embed.AddField("Joined server at", gUser.JoinedAt.GetValueOrDefault().UtcDateTime.ToString("yyyy-MM-dd HH:mm:ss UTC"));
            }

            await ReplyAsync(embed: embed.Build());
        }

        [Command("EventTime")]
        public async Task EventTime(DateTime datetime)
        {
            const string tdPrefix = "https://www.timeanddate.com/worldclock/fixedtime.html?iso=";
            var link = tdPrefix + datetime.ToString("s");

            await ReplyAsync(link);
        }

        [Command("source")]
        [Alias("repo")]
        public Task ReplySource()
            => ReplyAsync("Sourcecode:\nhttps://gitlab.com/LazyBoot/DiscordBot");

        [Command("cat")]
        public async Task<RuntimeResult> CatPublicAsync()
        {
            var checkCooldown = PictureService.CheckCooldown(PictureService.PictureType.Cat);
            if (checkCooldown > DateTime.UtcNow)
                return CommandResult.FromError($"Cooldown not expired! Remaining: {(checkCooldown - DateTime.UtcNow).TotalMinutes:N2} minutes");

            return await CatAsync();
        }

        [Command("cat"), Priority(10)]
        [RequireElevatedUser(Group = "catLimit")]
        [RequireUserPermission(ChannelPermission.ManageMessages, Group = "catLimit")]
        public async Task<RuntimeResult> CatAsync()
        {
            try { await Context.Channel.TriggerTypingAsync(); } catch { /* Do fuck all if this fails */ }

            // Get a stream containing an image of a cat
            using var resp = await PictureService.GetCatPictureAsync();
            if (resp == null)
                return CommandResult.FromError("Error getting cat");

            using var stream = await resp.Content.ReadAsStreamAsync();
            // Streams must be seeked to their beginning before being uploaded!
            stream.Seek(0, SeekOrigin.Begin);
            await Context.Channel.SendFileAsync(stream, "cat.png");
            return CommandResult.FromSuccess();
        }

        [Command("cat"), Priority(20)]
        [RequireElevatedUser]
        public async Task<RuntimeResult> CatSetAsync(PictureSettings settings)
        {
            if (PictureService.SetPictureSettings(settings))
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                return CommandResult.FromSuccess();
            }

            await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            return CommandResult.FromError();

        }

        [Command("picture-api")]
        [RequireElevatedUser]
        public async Task<RuntimeResult> SetPictureApiKey(PictureType pictureType, string apiKey)
        {
            if (PictureService.SetApiKey(pictureType, apiKey))
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                return CommandResult.FromSuccess();
            }

            await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            return CommandResult.FromError();
        }

        [Command("dog")]
        public async Task<RuntimeResult> DogPublicAsync()
        {
            var checkCooldown = PictureService.CheckCooldown(PictureService.PictureType.Dog);
            if (checkCooldown > DateTime.UtcNow)
                return CommandResult.FromError($"Cooldown not expired! Remaining: {(checkCooldown - DateTime.UtcNow).TotalMinutes:N2} minutes");

            return await DogAsync();
        }

        [Command("dog"), Priority(10)]
        [RequireElevatedUser(Group = "dogLimit")]
        [RequireUserPermission(ChannelPermission.ManageMessages, Group = "dogLimit")]
        public async Task<RuntimeResult> DogAsync()
        {
            try { await Context.Channel.TriggerTypingAsync(); } catch { /* Do fuck all if this fails */ }

            // Get a stream containing an image of a dog
            using var resp = await PictureService.GetDogPictureAsync();
            using var stream = await resp.Content.ReadAsStreamAsync();
            if (stream == null)
                return CommandResult.FromError("Error getting dog");

            // Streams must be seeked to their beginning before being uploaded!
            stream.Seek(0, SeekOrigin.Begin);
            await Context.Channel.SendFileAsync(stream, "dog.png");
            return CommandResult.FromSuccess();
        }

        [Command("xkcd")]
        public Task<RuntimeResult> GetXkcd()
            => PictureService.GetXkcdAsync();

        // 'params' will parse space-separated elements into a list
        [Command("list")]
        public Task ListAsync(params string[] objects)
            => ReplyAsync("You listed: " + string.Join("; ", objects));

        [Command("names")]
        [Alias("lottery", "draw")]
        public Task DrawRandomNamesAsync(params string[] names)
            => ReplyAsync(RandomNames.DrawLotteryFromNames(Context, names));

        [Command("drawodds")]
        public Task DrawRandomNamesWithOddsAsync(params string[] input)
            => ReplyAsync(RandomNames.DrawLotteryName(input));

        [Command("egg", RunMode = RunMode.Async)]
        [RequireGuild(Guilds.GetAcademy)]
        public async Task DisplayEasterEggAsync(string egg)
        {
            var oldDirectory = Directory.GetCurrentDirectory();
            var imageFolder = Path.Combine(Program.ProgramPath, "Data", @"Images");
            Directory.SetCurrentDirectory(imageFolder);

            var display = Context.Channel;
            egg = egg.ToLower();
            if (egg == "terje" || egg == "spenst")
                await display.SendFileAsync(@"Spenst.mp4");
            else if (egg == "eskil")
                await display.SendFileAsync(@"eskilSlack.jpg");
            else if (egg == "mopp")
                await display.SendFileAsync(@"mopp.jpg");
            else if (egg == "skilt")
                await display.SendFileAsync(@"skilt.jpg");
            else if (egg == "shopping")
                await display.SendFileAsync(@"shopping.jpg");
            else if (egg == "hover")
                await display.SendFileAsync(@"hover.mp4");
            else await ReplyAsync("Egg not found!");

            Directory.SetCurrentDirectory(oldDirectory);
        }

        //[Command("help")]
        //public async Task DisplayAllCommands()
        //{
        //          var prefix = PrefixService.GetPrefix(Context.Guild.Id);
        //	await ReplyAsync($"                         ***The following commands are valid***" + Environment.NewLine +
        //					 $"**-------------------------------------------------------------------------**" + Environment.NewLine +
        //					 $"**{prefix}names** or **{prefix}lottery** can be used to select random names from inputted list." + Environment.NewLine +
        //					 $"E.g : **{prefix}names** Geir Eskil Terje." + Environment.NewLine +
        //					 $"**{prefix}ping** to play ping pong with the bot. Hint, you never lose." + Environment.NewLine +
        //					 $"**{prefix}userinfo** returns information about the discord user." + Environment.NewLine +
        //					 $"**{prefix}echo**, obvious duh!" + Environment.NewLine +
        //					 $"**{prefix}cat**, here are some cat pictures for you feline cravings! :cat2: " + Environment.NewLine +
        //					 $"**{prefix}list** example function for making a list!" + Environment.NewLine +
        //					 $"**{prefix}remindme** function for reminders!" + Environment.NewLine +
        //					 $"**-------------------------------------------------------------------------**" + Environment.NewLine +
        //					 $"__Plus a few secret commands! :)__"
        //	);
        //}
    }
}
