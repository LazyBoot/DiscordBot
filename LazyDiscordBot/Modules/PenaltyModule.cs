using System;
using System.Collections;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Parsing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Fergun.Interactive;
using Fergun.Interactive.Pagination;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using ModelLibrary.PlayerDataModel;
using CommandResult = LazyDiscordBot.Models.CommandResult;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "prefixAdmin", ErrorMessage = "")]
    [RequireUserPermission(GuildPermission.ManageMessages, Group = "prefixAdmin", ErrorMessage = "Staff only command")]
    [@RequireRole(Guilds.DFA, DFA.Roles.RedBoi, Group = "prefixAdmin")]
    [@RequireRole(Guilds.DFA, DFA.Roles.MissionDev, Group = "prefixAdmin")]
    [RequireGuild(Guilds.Hoggit, Group = "penGuilds")]
    [RequireGuild(Guilds.LazyTest, Group = "penGuilds")]
    [RequireGuild(Guilds.DFA, Group = "penGuilds")]
    [DFAchannels(ErrorMessage = "Not allowed in here")]
    [Group("penalty")]
    [Alias("penalties", "pen")]
    public class PenaltyModule : InteractiveBase
    {
        private const string NoPlayersFoundWithThatName = "No players found with that name";
        private const string ErrorGettingStats = "Error getting penalty stats";
        private const string HoggitLogo = "https://i.imgur.com/KEd7OQJ.png";
        private const int HardLimit = 30;

        private readonly TimeSpan _errorTimeout = TimeSpan.FromSeconds(10);

        private readonly PenaltyService _service;

        private readonly Dictionary<IEmote, PaginatorAction> _noEmotes = new Dictionary<IEmote, PaginatorAction>() { { new Emoji("🛑"), PaginatorAction.Exit } };

        private readonly PrefixService _prefix;

        public PenaltyModule(PenaltyService service, PrefixService prefix)
        {
            _prefix = prefix;
            _service = service;
        }


        [Command("test", RunMode = RunMode.Async)]
        [Priority(10)]
        public async Task<RuntimeResult> PenTest()
        {
            var testDataPath = Path.Join(Path.Join(Program.ProgramPath, @"Data", @"TestData"), @"penalties.json");
            if (!File.Exists(testDataPath))
                return CommandResult.FromError("No penalty test data");

            var json = File.ReadAllText(testDataPath);

            var server = new ServerInfo()
            {
                Name = "Test Data",
                PenaltyTimeout = 30,
                ShortId = "Test"
            };

            var stopwatch = new Stopwatch();
            using (Context.Channel.EnterTypingState())
            {
                stopwatch.Start();
                var playerData = JsonSerializer.Deserialize<List<Player>>(json);
                await SendPenaltyEmbeds(server, playerData);

                stopwatch.Stop();
                return CommandResult.FromSuccess($"Time taken: {stopwatch.ElapsedMilliseconds}ms");
            }
        }

        [Command("help")]
        [Priority(20)]
        public async Task ShowHelp()
        {
            var prefix = _prefix.GetPrefix(Context.Guild.Id);
            var helpString = $"```\nUsage:\n" +
                             $"  {prefix}penalty [full] [active] [banned] <server> <player name>\n" +
                             $"  {prefix}penalty [exact] [full|active] [banned] <server> <player name>\n" +
                             $"  {prefix}penalty id [full] <server> <player id>\n" +
                             $"\n[] = optional, <> = required\n```";

            await ReplyAsync(helpString);
        }

        [Command(RunMode = RunMode.Async)]
        [Priority(0)]
        public async Task<RuntimeResult> PenaltyInfo(params string[] input)
        {
            var exactMatch = false;
            var activeOnly = false;
            var bannedOnly = false;
            var fullInfo = false;
            var byId = false;
            var shortInfo = false;
            string userName = null;
            ServerInfo server = null;

            var exactOption = new Option<bool>(name: "exact");
            var activeOption = new Option<bool>(name: "active");
            var bannedOnlyOption = new Option<bool>(name: "banned");
            var fullOption = new Option<bool>(name: "full");
            var byIdOption = new Option<bool>(name: "id");
            var shortOption = new Option<bool>(name: "short");
            var serverArgument = new Argument<string>(name: "server");
            var userArgument = new Argument<string[]>(name: "user");

            var rootCommand = new RootCommand
            {
                exactOption,
                activeOption,
                fullOption,
                byIdOption,
                shortOption,
                bannedOnlyOption,
                serverArgument,
                userArgument
            };
            rootCommand.SetHandler((exactOptionValue, activeOptionValue, fullOptionValue, byIdOptionValue, shortOptionValue, bannedOptionValue, serverArgumentValue, userArgumentValue) =>
            {
                exactMatch = exactOptionValue;
                activeOnly = activeOptionValue;
                bannedOnly = bannedOptionValue;
                fullInfo = fullOptionValue;
                byId = byIdOptionValue;
                shortInfo = shortOptionValue;
                userName = string.Join(' ', userArgumentValue);
                var servers = ServerInfo.GetAll(Context.Guild.Id);
                server = servers.Get(serverArgumentValue);
            }, exactOption, activeOption, fullOption, byIdOption, shortOption, bannedOnlyOption, serverArgument, userArgument);

            var parser = new CommandLineBuilder(rootCommand).Build();

            try
            {
                await parser.InvokeAsync(input);
            }

            catch (InvalidOperationException) { /* Intentionally left blank */ }

            if (server is null || userName is null)
                return CommandResult.FromError();

            using (Context.Channel.EnterTypingState())
            {
                List<Player> penData;
                PenDataResult result;
                string searchedName = string.Empty;

                if (byId)
                    (penData, result) = await _service.GetPenDataById(server, userName);
                else
                {
                    (penData, result) = await _service.GetPenDataByName(server, userName, exactMatch);
                    searchedName = userName;

                    if (bannedOnly)
                        penData = penData.Where(pd => pd.ucidBanned || pd.ipBanned).ToList();

                }

                if (result == PenDataResult.NotFound || !penData.Any())
                {
                    await ReplyAndDeleteAsync(NoMatch(server, userName), timeout: _errorTimeout);
                    return CommandResult.FromError();
                }

                if (result == PenDataResult.Error)
                {
                    await ReplyAndDeleteAsync(ErrorGettingStats, timeout: _errorTimeout);
                    return CommandResult.FromError();
                }

                return await SendPenaltyEmbeds(server, penData, fullInfo, activeOnly, exactMatch, shortInfo, searchedName);
            }
        }

        //[Group("times")]
        //public class PenTimes : PenaltyModule
        //{
        //    public PenTimes(HttpClient client, PrefixService prefix) : base(client, prefix)
        //    {
        //    }
        //}

        internal async Task<RuntimeResult> SendPenaltyEmbeds(ServerInfo server, List<Player> penData,
                                                             bool fullInfo = false, bool activeOnly = false, bool exactMatch = false,
                                                             bool shortInfo = false, string searchedName = "")
        {
            var timeout = DateTime.UtcNow - TimeSpan.FromDays(server.PenaltyTimeout);

            if (penData == null || !penData.Any())
            {
                await ReplyAndDeleteAsync(NoPlayersFoundWithThatName, timeout: _errorTimeout);
                return CommandResult.FromError();
            }

            string returnMessage = null;
            var checkReturns = CheckNumberOfReturns(penData);
            if (!checkReturns && !exactMatch)
                returnMessage = $"Found more than {HardLimit} matches ({penData.Count})";

            var options = new Dictionary<IEmote, PaginatorAction>()
            {
                { new Emoji("⏮️"), PaginatorAction.SkipToStart },
                { new Emoji("◀"), PaginatorAction.Backward },
                { new Emoji("#️⃣"), PaginatorAction.Jump },
                { new Emoji("▶"), PaginatorAction.Forward },
                { new Emoji("⏭️"), PaginatorAction.SkipToEnd },
            };

            var paginatedMessage = new StaticPaginatorBuilder()
                .AddUser(Context.User)
                .WithActionOnCancellation(ActionOnStop.DeleteInput)
                .WithActionOnTimeout(ActionOnStop.DeleteInput)
                .WithInputType(InputType.Buttons)
                .WithOptions(options)
                .WithFooter(PaginatorFooter.None);


            var i = 0;

            paginatedMessage.WithPages(penData.OrderByDescending(p => p.lastJoin)
                                              .ThenByDescending(pl => pl.joinDate)
                                              .Select(pd =>
            {
                var requestUser = (Context.Message.Author as IGuildUser)?.DisplayName ?? Context.Message.Author.ToString();
                var page = new PageBuilder
                {
                    Author = new EmbedAuthorBuilder()
                    {
                        Name = server.Name,
                        IconUrl = HoggitLogo
                    },
                    Footer = new EmbedFooterBuilder().WithText($"Player {++i}/{penData.Count} - Requested by: {requestUser}"),
                    Color = Color.Green,
                };

                return page.BuildPenaltyPage(fullInfo, pd, timeout, activeOnly, shortInfo, searchedName);
            }));

            if (returnMessage is not null)
                await ReplyAsync(returnMessage);

            _ = Interactive.SendPaginatorAsync(paginatedMessage.Build(), Context.Channel, TimeSpan.FromMinutes(5));

            return CommandResult.FromSuccess();
        }

        private bool CheckNumberOfReturns(ICollection penData)
        {
            return !(penData.Count > HardLimit);
        }


        private static string NoMatch(ServerInfo server, string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "No match found";

            if (server == null || string.IsNullOrWhiteSpace(server.Name))
                return $"No player matching {Format.Sanitize(text)} found";

            return $"No player matching `{text}` found on {server.Name}";
        }
    }

    static class PenaltyExtensions
    {

        public static PageBuilder BuildPenaltyPage(this PageBuilder page, bool fullInfo, Player playerData, DateTime timeout,
                                                  bool activeOnly, bool shortInfo = false, string searchedName = "")
        {
            var names = new StringBuilder();



            var description = new List<string>();

            if (!string.IsNullOrWhiteSpace(playerData.slmodId))
                description.Add($"Slmod stats id: {playerData.slmodId}");

            if (!string.IsNullOrWhiteSpace(playerData.joinDate))
                description.Add($"Join date: {playerData.joinDate.DateTimeFromUnixTime().ToUniversalTime():u}");

            if (!string.IsNullOrWhiteSpace(playerData.lastJoin))
                description.Add($"Last joined: {playerData.lastJoin.DateTimeFromUnixTime().ToUniversalTime():u}");

            if (description.Any())
                page.WithDescription(string.Join('\n', description));

            if (playerData.namesList != null)
            {
                if (shortInfo && !string.IsNullOrWhiteSpace(searchedName))
                {
                    var name = playerData.namesList.LastOrDefault(n => n.Contains(searchedName, StringComparison.OrdinalIgnoreCase));
                    if (!string.IsNullOrWhiteSpace(name))
                        page.AddField("Name", Format.Sanitize(name));
                }
                else if (!shortInfo)
                {

                    var index = 0;
                    //foreach (var name in playerData.Value.Names)
                    for (int i = 0; i < playerData.namesList.Count; i++)
                    {
                        if (playerData.namesList.ElementAtOrDefault(i) is null)
                            continue;

                        var name = Format.Sanitize(playerData.namesList[i]);
                        if (names.Length + name.Length > 1000)
                        {
                            index = i;
                            break;
                        }

                        names.AppendLine(name);
                    }

                    if (names.Length == 0)
                        names.AppendLine("<No names found>");

                    page.AddField("Names", names);

                    if (index != 0)
                    {
                        names = new StringBuilder();
                        for (int i = index; i < playerData.namesList.Count; i++)
                        {
                            if (playerData.namesList.ElementAtOrDefault(i) is null)
                                continue;

                            var name = Format.Sanitize(playerData.namesList[i]);
                            if (names.Length + name.Length > 1000)
                            {
                                index = i;
                                break;
                            }

                            names.AppendLine(name);
                        }

                        page.AddField("Names (cont)", names);
                    }
                }
            }

            if (fullInfo || shortInfo)
            {
                if (!string.IsNullOrWhiteSpace(page.Description))
                    page.Description += '\n';
                page.Description += $"UCID: {playerData.ucid}";
            }

            bool activeTk = false;
            bool activeCollision = false;

            if (!shortInfo)
            {
                if (playerData.friendlyKillList != null && playerData.friendlyKillList.Any())
                    activeTk = BuildKillDataText(playerData, timeout, page, fullInfo, activeOnly);

                if (playerData.collisionKillList != null && playerData.collisionKillList.Any())
                    activeCollision = BuildCollisionDataText(playerData, DateTime.UtcNow, page, fullInfo, activeOnly);
            }

            if (activeTk || activeCollision)
                page.Color = Color.Gold;

            if (playerData.ucidBanned || playerData.ipBanned)
            {
                var bannedText = "";
                if (playerData.ucidBanned)
                    bannedText += "UID Banned: Yes\n";
                if (playerData.ipBanned)
                    bannedText += "IP Banned: Yes";

                page.Color = Color.Red;
                page.AddField("Bans", bannedText);
            }

            return page;
        }

        internal static bool BuildKillDataText(Player playerData, DateTime timeout, PageBuilder page,
                                        bool fullInfo = false, bool activeOnly = false, int tkNum = 20)
        {
            if (fullInfo)
                tkNum /= 2;

            List<KeyValuePair<int, Kill>> teamKills;
            if (activeOnly)
                teamKills = playerData.friendlyKillList.Where(tk => !(tk.Value.forgiven || tk.Value.time.DateTimeFromUnixTime().ToUniversalTime() < timeout))
                          .OrderByDescending(tk => tk.Value.time.DateTimeFromUnixTime()).ThenByDescending(tk => tk.Key).Take(tkNum).ToList();
            else
                teamKills = playerData.friendlyKillList.OrderByDescending(tk => tk.Value.time.DateTimeFromUnixTime()).ThenByDescending(tk => tk.Key).Take(tkNum)
                                      .ToList();
            var (penalties, active) = BuildPenaltyDataText(timeout, fullInfo, activeOnly, teamKills);

            var penLabel = "Penalties (Kills)";

            BuildPenaltyText(page, fullInfo, activeOnly, penalties, penLabel);

            return active;

        }

        internal static bool BuildCollisionDataText(Player playerData, DateTime timeout, PageBuilder page,
                                                bool fullInfo = false, bool activeOnly = false, int tkNum = 20)
        {
            if (fullInfo)
                tkNum /= 2;

            List<KeyValuePair<int, Kill>> teamKills;
            if (activeOnly)
                teamKills = playerData.collisionKillList.Where(tk => !(tk.Value.forgiven || tk.Value.time.DateTimeFromUnixTime().ToUniversalTime() < timeout))
                          .OrderByDescending(tk => tk.Value.time.DateTimeFromUnixTime()).ThenByDescending(tk => tk.Key).Take(tkNum).ToList();
            else
                teamKills = playerData.collisionKillList.OrderByDescending(tk => tk.Value.time.DateTimeFromUnixTime()).ThenByDescending(tk => tk.Key).Take(tkNum)
                                      .ToList();

            var (penalties, active) = BuildPenaltyDataText(timeout, fullInfo, activeOnly, teamKills);
            var penLabel = "Penalties (Collisions)";

            BuildPenaltyText(page, fullInfo, activeOnly, penalties, penLabel);

            return active;
        }

        private static void BuildPenaltyText(PageBuilder page, bool fullInfo, bool activeOnly, StringBuilder penalties, string penLabel)
        {
            if (penalties.Length == 0)
                return;

            penLabel = activeOnly ? penLabel + " (Active Only)" : penLabel;

            if (penalties.Length >= 1024)
            {
                var tkNumShow = 10;
                if (fullInfo)
                    tkNumShow = 5;

                var splitPen = Regex.Split(penalties.ToString(), $"{Environment.NewLine}(?!---)").Where(x => !string.IsNullOrWhiteSpace(x))
                                    .ToArray();
                page.AddField(penLabel, string.Join(Environment.NewLine, splitPen.Take(tkNumShow)));
                var i = 0;
                while (splitPen.Length - tkNumShow * ++i > 0)
                {
                    page.AddField(penLabel + " (cont)", string.Join(Environment.NewLine, splitPen.Skip(tkNumShow * i).Take(tkNumShow)));
                }
            }
            else
                page.AddField(penLabel, penalties);
        }

        private static (StringBuilder penalties, bool active) BuildPenaltyDataText(DateTime timeout, bool fullInfo, bool activeOnly,
                                                                                   List<KeyValuePair<int, Kill>> teamKills)
        {
            var penalties = new StringBuilder();
            var active = false;
            foreach (var teamKill in teamKills)
            {
                var timeLeft = teamKill.Value.time.DateTimeFromUnixTime().ToUniversalTime() - timeout;

                if (activeOnly && (teamKill.Value.forgiven || timeLeft.TotalDays < 0))
                    continue;

                penalties.Append(
                    $"{teamKill.Key}: {teamKill.Value.time.DateTimeFromUnixTime().ToUniversalTime():u} - {Format.Sanitize(teamKill.Value.objTypeName)}");
                penalties.Append($" - {teamKill.Value.weapon} (From: {Format.Sanitize(teamKill.Value.shotFrom ?? "")})");

                if (fullInfo)
                {
                    if (teamKill.Value.humans != null)
                    {
                        penalties.AppendLine();
                        penalties.Append("---Victim:");
                        var i = 1;
                        foreach (var human in teamKill.Value.humans)
                        {
                            if (i > 1)
                                penalties.Append(',');

                            penalties.Append($" {human}");
                            i++;
                        }
                    }
                }

                if (teamKill.Value.forgiven)
                    penalties.Append(" [Forgiven]");
                else
                {
                    if (timeLeft.TotalDays < 0)
                        penalties.Append(" [Timed Out]");

                    else
                    {
                        penalties.Append(timeLeft.TotalDays < 2
                                             ? $" [{timeLeft.TotalDays:F2} days left]"
                                             : $" [{timeLeft.TotalDays:F0} days left]");

                        active = true;
                    }
                }

                penalties.AppendLine();
            }

            return (penalties, active);
        }


    }

}
