using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    [Group("customcom")]
    [Alias("cc")]
    [RequireContext(ContextType.Guild)]
    [RequireUserPermission(GuildPermission.ManageChannels, Group = "customcom")]
    [RequireElevatedUser(Group = "customcom")]
    public class CustomCommandModule : InteractiveBase
    {
        private CustomCommandService _customCommandService;

        public CustomCommandModule(CustomCommandService customCommandService)
        {
            _customCommandService = customCommandService;
        }

        [Command("add")]
        public async Task<RuntimeResult> AddCommand(string name, [Remainder] string message)
        {
            var result = await _customCommandService.AddCommandAsync(Context.Guild.Id, name.ToLowerInvariant(), message);
            if (result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            }

            return result;
        }

        [Command("update")]
        [Alias("edit")]
        public async Task<RuntimeResult> UpdateCommand(string name, [Remainder] string message)
        {
            var result = await _customCommandService.UpdateCommandAsync(Context.Guild.Id, name.ToLowerInvariant(), message);
            if (result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            }

            return result;
        }

        [Command("remove")]
        [Alias("delete", "del")]
        public async Task<RuntimeResult> RemoveCommand(string name)
        {
            var result = await _customCommandService.RemoveCommandAsync(Context.Guild.Id, name.ToLowerInvariant());
            if (result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            }

            return result;
        }

        [Command("list")]
        [Alias("show")]
        public async Task ListCommands()
        {
            var stringBuilder = new StringBuilder();
            var commands = _customCommandService.ListCommands(Context.Guild.Id).ToList();
            if (commands.Any())
            {
                stringBuilder.AppendLine("These are the currently set custom commands:");
                foreach (var customCommand in commands)
                {
                    stringBuilder.AppendLine(customCommand.Name);
                }

            }
            else
            {
                stringBuilder.AppendLine("No custom commands set");
            }

            await ReplyAsync(stringBuilder.ToString());
        }
    }
}
