﻿using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "words")]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "words", ErrorMessage = "")]
    [Group("replywords")]
    [Alias("replyword")]
    public class WordModule : ModuleBase<SocketCommandContext>
    {
        private readonly WordService _service;

        public WordModule(WordService service)
        {
            _service = service;
        }

        [Command("add", RunMode = RunMode.Async)]
        public Task<RuntimeResult> AddWord(string name, [Remainder] string message) 
            => _service.AddWord(Context.Guild, name, message).ToTask();

        [Command("delete", RunMode = RunMode.Async)]
        [Alias("remove", "del")]
        public Task<RuntimeResult> RemoveWord([Remainder] string name)
            => _service.DeleteWord(Context.Guild, name).ToTask();

        [Command("update", RunMode = RunMode.Async)]
        [Alias("edit")]
        public Task<RuntimeResult> UpdateWord(string name, [Remainder] string message) 
            => _service.UpdateWord(Context.Guild, name, message).ToTask();

        [Command("list")]
        public async Task ListWords()
        {
            var words = _service.ListWords(Context.Guild);
            var guildWords = words.Select(w => w.Name.ToString()).OrderBy(w => w);

            if (!guildWords.Any())
            {
                await ReplyAsync("No words added for this guild");
                return;
            }

            await ReplyAsync(Format.Code(string.Join("\n", guildWords), "regex"));
        }
    }
}
