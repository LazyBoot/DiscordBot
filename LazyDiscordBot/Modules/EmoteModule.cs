using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;

namespace LazyDiscordBot.Modules
{
    public class EmoteModule : InteractiveBase
    {
        [Command("emote")]
        [Alias("e")]
        [RequireElevatedUser(Group = "emoteGroup", ErrorMessage = "")]
        [RequireUserPermission(ChannelPermission.ManageMessages, Group = "emoteGroup", ErrorMessage = "")]
        [RequireBotPermission(ChannelPermission.UseExternalEmojis)]
        [RequireContext(ContextType.Guild)]
        public async Task<RuntimeResult> Emote(string emoteString, ulong? id = null, ITextChannel channel = null)
        {
            GuildEmote emote = Context.Client.GetGuild(Guilds.LazyTest).Emotes.FirstOrDefault(e => e.Name == emoteString);

            if (emote == null)
            {
                await Context.Message.DeleteAsync();
                return CommandResult.FromError();
            }

            var del = Task.CompletedTask;
            if (Context.Guild.CurrentUser.GetPermissions(Context.Channel as ITextChannel).ManageMessages)
                del = Context.Message.DeleteAsync();

            channel ??= Context.Channel as ITextChannel;

            IMessage message = id == null
                ? (await channel.GetMessagesAsync(2).FlattenAsync()).OrderBy(m => m.Id).FirstOrDefault()
                : await channel.GetMessageAsync(id.Value);

            var reactData = Interactive.NextReactionAsync(rf => rf.MessageId == message.Id && rf.Emote.Name == emote.Name && rf.UserId == Context.User.Id, timeout: TimeSpan.FromSeconds(10));

            var react = message.AddReactionAsync(emote);

            Task.WaitAll(react, del, reactData);

            await message.RemoveReactionAsync(emote, Context.Guild.CurrentUser);

            return CommandResult.FromSuccess();

        }
    }
}
