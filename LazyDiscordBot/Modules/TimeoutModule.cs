using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using LazyDiscordBot.TypeReaders;

namespace LazyDiscordBot.Modules
{
    [RequireGuild(DoghouseService.Guild)]
    [RequireElevatedUser(Group = "timeout")]
    [RequireBotPermission(GuildPermission.ModerateMembers)]
    [RequireUserPermission(GuildPermission.ManageMessages, Group = "timeout", ErrorMessage = "")]
    [Group("timeout")]
    public class TimeoutModule : InteractiveBase
    {
        private readonly HoggitLoggerService _loggerService;

        public TimeoutModule(HoggitLoggerService loggerService)
        {
            _loggerService = loggerService;
        }

        [Command(RunMode = RunMode.Async)]
        [Alias("add")]
        [Summary("Sends a user to the doghouse")]
        [Priority(11)]
        [RequireReply]
        [RequireContext(ContextType.Guild)]
        public Task<RuntimeResult> AddTimeout(double timeNumber, string timeUnit, [Remainder] string reason = "")
            => AddTimeout(Context.Message.ReferencedMessage.Author as IGuildUser, timeNumber, timeUnit, reason);


        [Command]
        [Alias("add")]
        [Summary("Times out a user")]
        [Priority(10)]
        public async Task<RuntimeResult> AddTimeout([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))] IGuildUser user, double timeNumber, string timeUnit, [Remainder] string reason = "")
        {
            TimeSpan time;
            if (timeUnit.StartsWith("week", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber * 7 > TimeSpan.MaxValue.TotalDays)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromDays(timeNumber * 7);
            }
            else if (timeUnit.StartsWith("day", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber > TimeSpan.MaxValue.TotalDays)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromDays(timeNumber);
            }
            else if (timeUnit.StartsWith("hour", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber > TimeSpan.MaxValue.TotalHours)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromHours(timeNumber);
            }
            else if (timeUnit.StartsWith("min", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber > TimeSpan.MaxValue.TotalMinutes)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromMinutes(timeNumber);
            }
            else
            {
                return CommandResult.FromError("Invalid time unit specified");
            }

            return await AddTimeout(user, time, reason);
        }


        [Command(RunMode = RunMode.Async)]
        [Alias("add")]
        [Summary("Sends a user to the doghouse")]
        [Priority(6)]
        [RequireReply]
        [RequireContext(ContextType.Guild)]
        public Task<RuntimeResult> AddTimeout(TimeSpan time, [Remainder] string reason = "")
            => AddTimeout(Context.Message.ReferencedMessage.Author as IGuildUser, time, reason);

        [Command]
        [Alias("add")]
        [Summary("Times out a user")]
        [Priority(5)]
        public async Task<RuntimeResult> AddTimeout([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))] IGuildUser user, TimeSpan time, [Remainder] string reason = "")
        {
            if (user.Id == Context.Client.CurrentUser.Id)
                return CommandResult.FromError("I won't doghouse *myself*!");

            if (Context.Guild.CurrentUser.Hierarchy < user.GetHierarchy())
                return CommandResult.FromError("I do not have permission to doghouse that user");

            if (time.TotalMilliseconds <= 0)
                return CommandResult.FromError("Please choose a time in the future");

            if (time.TotalDays > 28) // Max allowed by discord library
                return CommandResult.FromError("Duration can not be longer than 28 days");

            reason += $" [by {Context.User.Mention} ({Context.User.ToString()})]";

            try
            {
                await user.SetTimeOutAsync(time, new RequestOptions { AuditLogReason = reason });
            }
            catch (Exception)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                throw;
            }

            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess();
        }

        [Command("remove")]
        [Alias("del", "delete")]
        [Summary("Removes timeout from user")]
        [Priority(15)]
        public async Task<RuntimeResult> RemoveTimeout([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))] IGuildUser user)
        {
            try
            {
                await user.RemoveTimeOutAsync();
            }
            catch (Exception)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                throw;
            }

            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess();
        }
    }
}
