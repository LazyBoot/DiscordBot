﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Modules
{
    public partial class AdminModule : ModuleBase<SocketCommandContext>
    {
        [GeneratedRegex("^deleted_user_.+")]
        private static partial Regex DeletedUserRegex();

        [Command("PruneDeletedBans", RunMode = RunMode.Async)]
        [RequireBotPermission(GuildPermission.BanMembers)]
        [RequireUserPermission(GuildPermission.BanMembers)]
        public async Task UnbanDeleted()
        {
            var pause = TimeSpan.FromSeconds(5);
            var bans = await Context.Guild.GetBansAsync().FlattenAsync();

            var deletedBans = bans.Where(b => DeletedUserRegex().IsMatch(b.User.Username));

            var i = 0;
            await ReplyAsync("Starting...");
            foreach (var ban in deletedBans)
            {
                await Context.Guild.RemoveBanAsync(ban.User);
                i++;
                await Task.Delay(pause);
            }
            await ReplyAsync($"Done! Removed [{i}] bans");
        }

        [RequireElevatedUser(Group = "reqAdmin")]
        [RequireUserPermission(GuildPermission.ManageChannels, Group = "reqAdmin")]
        public class ReqAdmin : AdminModule
        {
            [Command("dump", RunMode = RunMode.Async)]
            public async Task DumpChan()
            {
                await Context.Guild.DownloadUsersAsync();
                var messages = await Context.Channel.GetMessagesAsync(50000, CacheMode.AllowDownload).FlattenAsync();

                var builder = new StringBuilder();
                foreach (var message in messages.OrderBy(m => m.Timestamp).ToList())
                {
                    var userMessage = message as IUserMessage;
                    builder.Append($"[{message.Timestamp}] ");
                    var author = message.Author as SocketGuildUser;
                    builder.Append(author?.Nickname ?? message.Author.Username);
                    builder.AppendLine(":");
                    builder.AppendLine(userMessage?.Resolve() ?? message.Content);
                    builder.AppendLine();
                }

                File.WriteAllText(@"dump.txt", builder.ToString());
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                await Context.User.SendFileAsync(@"dump.txt");
            }

            // [Remainder] takes the rest of the command's arguments as one argument, rather than splitting every space
            [Command("echo")]
            public Task EchoAsync([Remainder] string text)
                // Insert a ZWSP before the text to prevent triggering other bots!
                => ReplyAsync('\u200B' + text, allowedMentions: AllowedMentions.None);

            [Command("echo")]
            public async Task EchoAsync(IMessageChannel channel, [Remainder] string text)
            {
                await channel.SendMessageAsync('\u200B' + text, allowedMentions: AllowedMentions.None);
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }

            [Command("echod")]
            [RequireBotPermission(ChannelPermission.ManageMessages)]
            public async Task EchoDAsync([Remainder] string text)
            // Insert a ZWSP before the text to prevent triggering other bots!
            {
                await EchoAsync(text);
                await Context.Channel.DeleteMessageAsync(Context.Message);
            }

            [Command("echod")]
            [RequireBotPermission(ChannelPermission.ManageMessages)]
            public async Task EchoDAsync(IMessageChannel channel, [Remainder] string text)
            // Insert a ZWSP before the text to prevent triggering other bots!
            {
                await EchoAsync(channel, text);
                await Context.Channel.DeleteMessageAsync(Context.Message);
            }


        }

        [RequireElevatedUser]
        public class ReqElevated : AdminModule
        {
            public IHostApplicationLifetime hostApp { get; set; }

            [Command("userdir")]
            public async Task TestUserDir()
            {
                await ReplyAsync((Environment.OSVersion.Platform == PlatformID.Unix ||
                                  Environment.OSVersion.Platform == PlatformID.MacOSX)
                    ? Environment.GetEnvironmentVariable("HOME")
                    : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%"));
            }

            [Command("set-game")]
            public async Task SetGame([Remainder] string text = null)
            {
                await Context.Client.SetGameAsync(text);
            }

            [Command("update")]
            public Task Update()
            {
                Task.Run(() => Utility.Update(Context, hostApp));
                return Task.CompletedTask;
            }

            [Command("restart")]
            public Task Restart()
            {
                hostApp.StopApplication();
                return Task.CompletedTask;
            }

            [Command("version")]
            public Task Version()
            {
                return base.ReplyAsync(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion);
            }
        }

        [RequireOwner]
        public class ReqOwner : AdminModule
        {
            private readonly ILogger _logger;

            public ReqOwner(ILogger<AdminModule> logger)
            {
                _logger = logger;
            }


            [Command("listGuilds")]
            public async Task ListGuilds()
            {
                if (Context.Client.Guilds.Any())
                {
                    var stringBuilder = new StringBuilder();

                    foreach (var guild in Context.Client.Guilds.OrderBy(g => g.Name))
                    {
                        stringBuilder.Append(guild.Id);
                        stringBuilder.Append(" - ");
                        stringBuilder.AppendLine(guild.Name);
                    }

                    await ReplyAsync($"I'm in the following guilds:```\n{stringBuilder}```");
                }
                else
                {
                    await ReplyAsync("I'm not in any guilds!");
                }
            }

            [Command("remove"), Alias("delete")]
            public async Task RemoveMessage(ulong msgId)
            {
                if (await Context.Channel.GetMessageAsync(msgId) is IUserMessage message)
                {
                    await message.DeleteAsync();
                    await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                }
                else
                    await Context.Message.AddReactionAsync(CommandResult.FailedReaction);

            }

            [Command("exceptionTest")]
            public async Task<RuntimeResult> ExceptionTest()
            {
                const string message = "Test exception";
                await ReplyAsync(message);
                try
                {
                    throw new NotImplementedException();
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, message);
                    throw;
                }
            }
        }
    }
}
