﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Fergun.Interactive;
using Fergun.Interactive.Selection;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using LazyDiscordBot.TypeReaders;
using ModelLibrary.DataTransferModels;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "wlAdmin", ErrorMessage = "")]
    [RequireUserPermission(GuildPermission.ManageMessages, Group = "wlAdmin", ErrorMessage = "Staff only command")]
    [@RequireRole(Guilds.Hoggit, HoggitRoles.Instructors, Group = "wlAdmin", ErrorMessage = "Staff only command")]
    [@RequireRole(Guilds.Hoggit, HoggitRoles.SlotRequest, Group = "wlAdmin", ErrorMessage = "Staff only command")]
    [RequireGuild(Guilds.Hoggit, Group = "wlGuilds")]
    [RequireGuild(Guilds.LazyTest, Group = "wlGuilds")]
    [Group("whitelist")]
    [Alias("wl")]
    [Summary("Whitelists users for free aircraft")]
    public class WhitelistModule : InteractiveBase
    {
        private readonly PenaltyService _service;
        private readonly PrefixService _prefix;
        private readonly TimeSpan _errorTimeout = TimeSpan.FromSeconds(10);

        private static Emoji GAWFail = new Emoji("🛰️");
        private static Emoji TNNFail = new Emoji("📡");
        private static Emoji MTMFail = new Emoji("📻");

        public WhitelistModule(PenaltyService service, PrefixService prefix)
        {
            _service = service;
            _prefix = prefix;
        }

        [Command, Priority(0)]
        [Alias("help")]
        public async Task ShowHelp()
        {
            var prefix = _prefix.GetPrefix(Context.Guild.Id);
            var helpString = $"```\nUsage:\n{prefix}whitelist <server> <discord user> <player name>\n```";

            await ReplyAsync(helpString);
        }

        [Command, Priority(15)]
        [RequireReply]
        public Task<RuntimeResult> StartWhitelist(ServerInfo server, [Remainder] string name)
            => StartWhitelist(server, Context.Message.ReferencedMessage.Author, name);

        [Command, Priority(10)]
        public async Task<RuntimeResult> StartWhitelist(ServerInfo server, [OverrideTypeReader(typeof(LazyUserTypeReader<IUser>))] IUser user, [Remainder] string name)
        {
            try { await Context.Channel.TriggerTypingAsync(); } catch { /* Do fuck all if this fails */ }

            var (data, result) = await _service.LookupWlData(server, name);

            if (result == PenDataResult.Error)
                return CommandResult.FromError("Error looking up data. If problem persists: restart API");

            if (result == PenDataResult.NotFound)
                return CommandResult.FromError($"No user found containing `{Format.Sanitize(name)}`");

            var players = new Dictionary<string, WhitelistEntry>();
            foreach (var player in data.Where(pl => pl.name.Contains(name, StringComparison.OrdinalIgnoreCase)))
            {
                players.Add($"{players.Count} - {Format.Sanitize(player.name)}", new WhitelistEntry(player.ucid, player.name, string.Empty));
            }

            if (!players.Any())
                return CommandResult.FromError($"No user found containing `{Format.Sanitize(name)}`");

            var options = players.Select(x => x.Key).ToArray();

            var selectionBuilder = new SelectionBuilder<string>()
                .AddUser(Context.User)
                .WithInputType(InputType.SelectMenus)
                .WithOptions(players.Select(x => x.Key).ToArray())
                .WithSelectionPage(new PageBuilder().WithText("Select user:"))
                .WithActionOnCancellation(ActionOnStop.DeleteMessage)
                .WithActionOnSuccess(ActionOnStop.DeleteMessage)
                .WithActionOnTimeout(ActionOnStop.DeleteMessage)
                .Build();

            var reply = await Interactive.SendSelectionAsync(selectionBuilder, Context.Channel, _errorTimeout);

            if (reply.IsTimeout)
            {
                _ = Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError("No user chosen.");
            }

            if (!reply.IsSuccess)
            {
                _ = Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError("Unknown error with selection.");
            }

            var chosenPlayer = players[reply.Value];
            chosenPlayer.discordHandle = user.ToString();


            var servers = ServerInfo.GetAll(Context.Guild.Id);

#if DEBUG
            try
            {
                var wlResult = await _service.SendWlData(server, chosenPlayer);
                if (wlResult == PenDataResult.Error)
                {
                    await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                    return CommandResult.FromError("Error sending whitelist request");
                }
            }
            catch (Exception ex) when (ex is OperationCanceledException || ex is HttpRequestException)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError("Error sending whitelist request");
            }

#else
            bool gawError = false;
            bool tnnError = false;
            bool mtmError = false;

            try
            {
                var gawResult = await _service.SendWlData(servers.Get("GAW"), chosenPlayer);
                if (gawResult == PenDataResult.Error)
                    gawError = true;
            }
            catch (Exception ex) when (ex is OperationCanceledException || ex is HttpRequestException)
            {
                gawError = true;
            }

            if (gawError)
                await Context.Message.AddReactionAsync(GAWFail);


            try { 
                var tnnResult = await _service.SendWlData(servers.Get("TNN"), chosenPlayer);
                if (tnnResult == PenDataResult.Error)
                    tnnError = true; 
            }
            catch (Exception ex) when (ex is OperationCanceledException || ex is HttpRequestException)
            {
                tnnError = true;
            }

            if (tnnError)
                await Context.Message.AddReactionAsync(TNNFail);

            // try
            // {
            //     var mtmResult = await _service.SendWlData(servers.Get("MTM"), chosenPlayer);
            //     if (mtmResult == PenDataResult.Error)
            //         mtmError = true;
            // }
            // catch (OperationCanceledException)
            // {
            //     mtmError = true;
            // }

            // if (mtmError)
            //     await Context.Message.AddReactionAsync(MTMFail);


            if (gawError && tnnError && mtmError)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError("Error sending whitelist request");
            }
#endif

            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess();
        }
    }
}
