﻿using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [Group("banner")]
    [RequireBotPermission(GuildPermission.ManageGuild)]
    [RequireUserPermission(GuildPermission.ManageGuild, Group = "bannerChanger")]
    [RequireElevatedUser(Group = "bannerChanger", ErrorMessage = "")]
    public class BannerModule : ModuleBase<SocketCommandContext>
    {
        private readonly BannerService _service;

        public BannerModule(BannerService service)
        {
            _service = service;
        }

        [Command("add", RunMode = RunMode.Async)]
        public Task<RuntimeResult> AddGuild(ITextChannel channel, int interval = 3)
            => _service.AddGuild(Context.Guild, channel, interval);

        [Command("show", RunMode = RunMode.Async)]
        [Alias("status")]
        public async Task<RuntimeResult> ShowGuild()
        {
            var bannerData = _service.GetBannerData(Context.Guild);
            if (bannerData == null)
                return CommandResult.FromError("Guild is not set up for banner rotation");

            await ReplyAsync($"Guild is getting banners from: {MentionUtils.MentionChannel(bannerData.BannerChannel)} and interval: {bannerData.ChangeIntervalDays}");
            return CommandResult.FromSuccess();
        }

        [Command("delete")]
        [Alias("remove")]
        public Task<RuntimeResult> RemoveGuild()
            => _service.RemoveGuild(Context.Guild).ToTask();

        [Command("force", RunMode = RunMode.Async)]
        public Task<RuntimeResult> ForceUpdate()
            => _service.ForceChange(Context.Guild);
    }
}
