﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Modules
{
    [Group("inspect")]
    [RequireElevatedUser(Group = "inspectmodule")]
    [RequireUserPermission(ChannelPermission.ManageMessages, Group = "inspectmodule")]
    public class InspectModule : ModuleBase<SocketCommandContext>
    {
        private readonly Regex _pattern;
        private readonly ILogger<InspectModule> _logger;

        public InspectModule(ILogger<InspectModule> logger, QuoteService quoteService)
        {
            _logger = logger;
            _pattern = quoteService.Pattern;
        }

        [Command("reply")]
        [RequireReply(ErrorMessage = "No reply")]
        public async Task InspectReply()
        {
            var reply = $"{Context.Message.ReferencedMessage.Author} - {String.Join(", ", Context.Message.MentionedUsers.Select(u => u.Id))}";
            await ReplyAsync(reply);
        }

        [Command("message")]
        [Priority(5)]
        [RequireReply(ErrorMessage = "No reply")]
        public async Task InspectMessage()
        {
            await SendMessageContent(Context.Message.ReferencedMessage);
        }

        [Command("message")]
        public async Task<RuntimeResult> InspectMessage([Remainder] string messageLink)
        {
            var foundMessages = 0;
            foreach (Match match in _pattern.Matches(messageLink))
            {
                if (!ulong.TryParse(match.Groups["GuildId"].Value, out var guildId) ||
                    !ulong.TryParse(match.Groups["ChannelId"].Value, out var channelId) ||
                    !ulong.TryParse(match.Groups["MessageId"].Value, out var messageId)) continue;

                try
                {
                    var message = await GetMessage(guildId, channelId, messageId);
                    if (message == null) 
                        continue;
                        
                    foundMessages++;
                    await SendMessageContent(message);
                }
                catch (Exception ex)
                {
                    const string errorText = "An error occured while getting message";
                    _logger.LogError(ex, errorText);
                    return CommandResult.FromError(errorText);
                }
            }

            return foundMessages == 0 ? CommandResult.FromError("Invalid message link") : CommandResult.FromSuccess();
        }

        private async Task SendMessageContent(IMessage message, bool longOutput = false)
        {
            var builder = new StringBuilder("```\n");

            foreach (var character in message.Content)
            {
                if (longOutput)
                    builder.AppendLine($" - {EncodeNonAsciiCharacter(character)}");
                else
                    builder.Append(EncodeNonAsciiCharacter(character));

            }

            builder.Append("```");

            await ReplyAsync(builder.ToString());
        }

        private async Task<IMessage> GetMessage(ulong guildId, ulong channelId, ulong messageId)
        {
            var guild = Context.Client.GetGuild(guildId);

            var channel = guild?.GetTextChannel(channelId);
            if (channel == null)
                return null;

            if (!guild.CurrentUser.GetPermissions(channel).ViewChannel)
                return null;

            var message = await channel.GetMessageAsync(messageId);

            return message;
        }

        private string EncodeNonAsciiCharacter(char value)
        {
            if (value > 127)
            {
                // This character is too big for ASCII
                return "\\u" + ((int)value).ToString("x4");
            }

            return value.ToString();
        }
    }
}
