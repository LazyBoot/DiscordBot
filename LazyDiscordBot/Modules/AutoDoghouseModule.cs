﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireGuild(DoghouseService.Guild)]
    [RequireElevatedUser(Group = "autodoghouse")]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "autodoghouse", ErrorMessage = "")]
    [RequireBotPermission(GuildPermission.BanMembers)]
    [Group("autodoghouse")]
    public class TriggerModule : InteractiveBase
    {
        private readonly AutoDoghouseService _service;

        public TriggerModule(AutoDoghouseService service)
        {
            _service = service;
        }

        [Command("add", RunMode = RunMode.Async)]
        public Task<RuntimeResult> AddTrigger(string name, TimeSpan time, [Remainder] string message)
            => _service.AddTrigger(Context.Guild, name, time, message).ToTask();

        [Command("delete", RunMode = RunMode.Async)]
        [Alias("remove", "del")]
        public Task<RuntimeResult> RemoveTrigger([Remainder] string name)
            => _service.DeleteTrigger(Context.Guild, name).ToTask();

        [Command("deltest", RunMode = RunMode.Async)]
        [Alias("remove-test", "del-test")]
        public Task<RuntimeResult> RemoveTriggerTest(string name)
            => _service.DeleteTrigger(Context.Guild, name).ToTask();

        [Command("delete-list", RunMode = RunMode.Async)]
        [Alias("remove-list", "removelist", "dellist", "del-list", "deletelist")]
        public async Task<RuntimeResult> RemoveTriggerList()
        {
            var triggers = _service.ListTriggers(Context.Guild);
            var guildTriggers = triggers.Select(w => w.Name.ToString()).OrderBy(w => w);

            if (!guildTriggers.Any())
            {
                await ReplyAsync("No triggers added for this guild");
                return CommandResult.FromSuccess();
            }

            var text = new StringBuilder(); //string.Join("\n", guildTriggers);

            for (int i = 0; i < guildTriggers.Count(); i++)
            {
                text.AppendLine($"{i}: {guildTriggers.ElementAt(i)}");
            }

            await base.ReplyAsync(Format.Code(text.ToString(), "regex"));

            var nextMessage = await Interactive.NextMessageAsync(m => m.Author.Id == Context.User.Id, timeout: TimeSpan.FromSeconds(15));
            if (!nextMessage.IsSuccess)
            {
                _ = Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError("No trigger chosen.");
            }

            if (!int.TryParse(nextMessage.Value.Content, out var chosenNumber) || chosenNumber < 0 || chosenNumber >= guildTriggers.Count())
            {
                _ = Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError("Invalid trigger chosen.");
            }

            var chosenTrigger = guildTriggers.ElementAt(chosenNumber);


            return _service.DeleteTrigger(Context.Guild, chosenTrigger);
        }

        [Command("update", RunMode = RunMode.Async)]
        [Alias("edit")]
        public Task<RuntimeResult> UpdateTrigger(string name, TimeSpan time, [Remainder] string message)
            => _service.UpdateTrigger(Context.Guild, name, time, message).ToTask();

        [Command("list")]
        public async Task ListTriggers()
        {
            var triggers = _service.ListTriggers(Context.Guild);
            var guildTriggers = triggers.Select(w => $"{w.Name}   :   {w.Message}").OrderBy(w => w);

            if (!guildTriggers.Any())
            {
                await ReplyAsync("No triggers added for this guild");
                return;
            }

            await ReplyAsync(Format.Code(string.Join("\n", guildTriggers), "regex"));
        }

        [Group("NoVideo")]
        [Alias("video")]
        public class AutoDoghouseVideo : TriggerModule
        {
            public AutoDoghouseVideo(AutoDoghouseService service) : base(service)
            {
            }

            [Command("add", RunMode = RunMode.Async)]
            public Task<RuntimeResult> AddNoVideoChannel(ITextChannel channel, TimeSpan time)
               => _service.AddNoVideoChannel(Context.Guild, channel, time).ToTask();

            [Command("delete", RunMode = RunMode.Async)]
            [Alias("remove", "del")]
            public Task<RuntimeResult> DeleteNoVideoChannel(ITextChannel channel)
                => _service.DeleteNoVideoChannel(Context.Guild, channel).ToTask();

            [Command("update", RunMode = RunMode.Async)]
            [Alias("edit")]
            public Task<RuntimeResult> UpdateTrigger(ITextChannel channel, TimeSpan time)
                => _service.UpdateNoVideoChannel(Context.Guild, channel, time).ToTask();

            [Command("list")]
            public async Task ListNoVideoTriggers()
            {
                var triggers = _service.ListNoVideoTriggers(Context.Guild);
                var guildTriggers = triggers.Select(w => $"#{Context.Guild.GetChannel(w.ChannelId).Name}").OrderBy(w => w);

                if (!guildTriggers.Any())
                {
                    await ReplyAsync("No triggers added for this guild");
                    return;
                }

                await ReplyAsync(Format.Code(string.Join("\n", guildTriggers), "regex"));
            }

        }
    }
}
