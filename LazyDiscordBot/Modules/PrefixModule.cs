﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    [RequireContext(ContextType.Guild)]
    public class PrefixModule : ModuleBase<SocketCommandContext>
    {
        private PrefixService _prefixService;

        public PrefixModule(PrefixService prefix)
        {
            _prefixService = prefix;
        }

        [Command("ShowPrefix")]
        [Summary("Shows the current prefix config")]
        public Task ShowDefaultPrefixAsync()
        {
            return ReplyAsync("**Prefix Info**\n" +
                              $"Current Server Prefix: `{_prefixService.GetPrefix(Context.Guild.Id)}`\n" +
                              $"Default Prefix: `{_prefixService.GetDefaultPrefix()}`");
        }

        [Command("CustomPrefix")]
        [Summary("Sets or resets the guild's prefix")]
        [RequireElevatedUser(Group = "prefixgroup")]
        [RequireUserPermission(GuildPermission.ManageChannels, Group = "prefixgroup", ErrorMessage = "You don't have permission to do that")]
        public Task CustomPrefixAsync([Remainder] string prefix = null)
        {
            var res = _prefixService.SetPrefix(Context.Guild.Id, prefix);

            if (res == PrefixService.PrefixSetResult.Success)
            {
                return ReplyAsync($"Success, your prefix is now: `{prefix}`");
            }

            return ReplyAsync($"Prefix is now: `{_prefixService.GetPrefix(Context.Guild.Id)}`");
        } 

        [Command("DefaultPrefix")]
        [Summary("Sets the default prefix for the bot")]
        [RequireElevatedUser]
        public Task<RuntimeResult> DefaultPrefix([Remainder] string prefix)
        {
            if (string.IsNullOrWhiteSpace(prefix))
                return CommandResult.FromError("Default Prefix cannot be null or whitespace").ToTask();

            _prefixService.SetDefaultPrefix(prefix);
            return CommandResult.FromSuccess($"Default prefix is now: {_prefixService.GetDefaultPrefix()}").ToTask();
        }
    }
}
