using System.Diagnostics;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using LazyDiscordBot.ExtensionClasses;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "messagenotify")]
    [RequireBotPermission(GuildPermission.ViewAuditLog)]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "messagenotify", ErrorMessage = "")]
    [RequireContext(ContextType.Guild)]
    [Group("messagedeletenotify")]
    [Alias("messdelnotify")]
    [DebuggerDisplay("messagedeletenotify")]
    public class MessageDeleteNotifyModule : ModuleBase<SocketCommandContext>
    {
        private readonly MessageDeleteNotifyService _service;

        public MessageDeleteNotifyModule(MessageDeleteNotifyService service)
        {
            _service = service;
        }

        [Command("enable")]
        public async Task<RuntimeResult> Enable(ITextChannel channel)
        {
            _service.Enable(Context.Guild, channel);
            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess();
        }

        [Command("disable")]
        public async Task<RuntimeResult> Disable()
        {
            if (_service.Disable(Context.Guild))
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                return CommandResult.FromSuccess();
            }

            await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            return CommandResult.FromError("Error disabling in guild");
        }

        [Command("status")]
        public Task<RuntimeResult> Status()
        {
            if (_service.IsGuildEnabled(Context.Guild.Id, out ITextChannel channel))
            {
                return CommandResult.FromSuccess($"Log channel is [{channel.Id}]").ToTask();
            }

            return CommandResult.FromSuccess("Not enabled in guild").ToTask();
        }

        [Command("cleandb")]
        [RequireOwner]
        public async Task<RuntimeResult> CleanDB()
        {
            if (_service.CleanDB())
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
                return CommandResult.FromSuccess();
            }

            await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            return CommandResult.FromError("Error disabling in guild");
        }

        [Command("test")]
        public async Task Test()
        {
            await _service.Test(Context.Guild);
        }
    }
}
