﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace LazyDiscordBot.Modules
{
    public class HelpModule : ModuleBase
    {
        private readonly CommandService _commands;
        private readonly IServiceProvider _map;

        public HelpModule(IServiceProvider map, CommandService commands)
        {
            _commands = commands;
            _map = map;
        }

        [Command("help")]
        [Summary("Lists this bot's commands.")]
        public async Task Help([Remainder] string path = "")
        {
            var output = new List<EmbedBuilder>();
            var embed = new EmbedBuilder();
            output.Add(embed);
            if (path == "")
            {
                embed.Title = "LazyBot - help";

                foreach (var mod in _commands.Modules.Where(m => m.Parent == null && !string.IsNullOrEmpty(m.Name) && m.GetExecutableCommandsAsync(Context, _map).GetAwaiter().GetResult().Any()))
                {
                    if (embed.Fields.Count >= EmbedBuilder.MaxFieldCount || embed.Length >= EmbedBuilder.MaxEmbedLength - 500)
                    {
                        embed = new EmbedBuilder();
                        output.Add(embed);
                    }
                    AddHelp(mod, ref embed);
                }

                embed.Footer = new EmbedFooterBuilder
                {
                    Text = "Use 'help <module>' to get help with a module."
                };
            }
            else
            {
                var splitPath = path.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                var mod = _commands.Modules.FirstOrDefault(m => m.Name.ToLower() == splitPath[0].ToLower());
                if (mod == null) { await ReplyAsync("No module could be found with that name."); return; }

                var subPath = string.Join(' ', splitPath.Skip(1));
                embed.Title = mod.Name;
                var mods = mod.Submodules.Where(m => m.GetExecutableCommandsAsync(Context, _map).GetAwaiter().GetResult().Any());
                embed.Description = $"{mod.Summary}\n" +
                (!string.IsNullOrEmpty(mod.Remarks) ? $"({mod.Remarks})\n" : "") +
                (mod.Aliases.Any() ? $"Prefix(es): {string.Join(",", mod.Aliases)}\n" : "") +
                (mods.Any() ? $"Submodules: {string.Join(", ", mods.Select(m => m.Name))}\n" : "") + " ";
                foreach (var command in mod.GetExecutableCommandsAsync(Context, _map).GetAwaiter().GetResult().Where((m => subPath.Length > 0 && m.Name.Equals(subPath, StringComparison.OrdinalIgnoreCase) || subPath.Length == 0)))
                {
                    if (embed.Fields.Count >= EmbedBuilder.MaxFieldCount || embed.Length >= EmbedBuilder.MaxEmbedLength - 500)
                    {
                        embed = new EmbedBuilder();
                        output.Add(embed);
                    }
                    AddCommand(command, ref embed);
                }

            }

            foreach (var em in output.Select(e => e.Build()))
                await ReplyAsync(embed: em);
        }

        public void AddHelp(ModuleInfo module, ref EmbedBuilder builder)
        {
            builder.AddField(f =>
            {
                f.Name = $"**{module.Name}**";
                f.Value = $"Submodules: {string.Join(", ", module.Submodules.Where(m => m.GetExecutableCommandsAsync(Context, _map).GetAwaiter().GetResult().Any()).Select(m => m.Name))}" +
                $"\n" +
                $"Commands: {string.Join(", ", module.Commands.Where(m => m.CheckPreconditionsAsync(Context, _map).GetAwaiter().GetResult().IsSuccess).Select(x => $"`{x.Name}`"))}";
                f.IsInline = true;
            });
        }

        public void AddCommand(CommandInfo command, ref EmbedBuilder builder)
        {
            builder.AddField(f =>
            {
                f.Name = $"**{command.Name}**";
                f.Value = $"{command.Summary}\n" +
                (!string.IsNullOrEmpty(command.Remarks) ? $"({command.Remarks})\n" : "") +
                (command.Aliases.Any() ? $"**Aliases:** {string.Join(", ", command.Aliases.Select(x => $"`{x}`"))}\n" : "") +
                $"**Usage:** `{GetPrefix(command)} {GetAliases(command)}`";
            });
        }

        public string GetAliases(CommandInfo command)
        {
            StringBuilder output = new StringBuilder();
            if (!command.Parameters.Any()) return output.ToString();
            foreach (var param in command.Parameters)
            {
                if (param.IsOptional)
                    output.Append($"[{param.Name} = {param.DefaultValue}] ");
                else if (param.IsMultiple)
                    output.Append($"|{param.Name}| ");
                else if (param.IsRemainder)
                    output.Append($"...{param.Name} ");
                else
                    output.Append($"<{param.Name}> ");
            }
            return output.ToString();
        }
        public string GetPrefix(CommandInfo command)
        {
            //var output = GetPrefix(command.Module);
            var output = $"{command.Aliases.FirstOrDefault()}";
            return output;
        }
        public string GetPrefix(ModuleInfo module)
        {
            string output = "";
            if (module.Parent != null) output = $"{GetPrefix(module.Parent)}{output}";
            if (module.Aliases.Any())
                output += string.Concat(module.Aliases.FirstOrDefault(), " ");
            return output;
        }
    }
}
