﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Humanizer;
using Humanizer.Localisation;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using LazyDiscordBot.TypeReaders;

namespace LazyDiscordBot.Modules
{
    [RequireGuild(DoghouseService.Guild)]
    [RequireElevatedUser(Group = "doghouse")]
    [RequireBotPermission(GuildPermission.ManageRoles)]
    [RequireUserPermission(GuildPermission.ManageMessages, Group = "doghouse", ErrorMessage = "")]
    [Group("doghouse")]
    [Alias("dh")]
    [Summary("Doghoused users can't chat")]
    public class DoghouseModule : InteractiveBase
    {
        private readonly DoghouseService _service;
        private readonly DoghouseHistoryService _historyService;
        private readonly HoggitLoggerService _loggerService;

        public DoghouseModule(DoghouseService service, HoggitLoggerService loggerService, DoghouseHistoryService historyService)
        {
            _service = service;
            _historyService = historyService;
            _loggerService = loggerService;
        }

        [Command(RunMode = RunMode.Async)]
        [Alias("add")]
        [Summary("Sends a user to the doghouse")]
        [Priority(11)]
        [RequireReply]
        public Task<RuntimeResult> AddDoghouse(double timeNumber, string timeUnit, [Remainder] string reason = "")
            => AddDoghouse(Context.Message.ReferencedMessage.Author, timeNumber, timeUnit, reason);

        [Command(RunMode = RunMode.Async)]
        [Alias("add")]
        [Summary("Sends a user to the doghouse")]
        [Priority(6)]
        [RequireReply]
        public Task<RuntimeResult> AddDoghouse(TimeSpan time, [Remainder] string reason = "")
            => AddDoghouse(Context.Message.ReferencedMessage.Author, time, reason);

        [Command(RunMode = RunMode.Async)]
        [Alias("add")]
        [Summary("Sends a user to the doghouse")]
        [Priority(10)]
        public async Task<RuntimeResult> AddDoghouse([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))] IUser user, double timeNumber, string timeUnit, [Remainder] string reason = "")
        {
            TimeSpan time;
            if (timeUnit.StartsWith("week", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber * 7 > TimeSpan.MaxValue.TotalDays)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromDays(timeNumber * 7);
            }
            else if (timeUnit.StartsWith("day", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber > TimeSpan.MaxValue.TotalDays)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromDays(timeNumber);
            }
            else if (timeUnit.StartsWith("hour", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber > TimeSpan.MaxValue.TotalHours)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromHours(timeNumber);
            }
            else if (timeUnit.StartsWith("min", StringComparison.OrdinalIgnoreCase))
            {
                if (timeNumber > TimeSpan.MaxValue.TotalMinutes)
                    return CommandResult.FromError("Duration is too long");

                time = TimeSpan.FromMinutes(timeNumber);
            }
            else
            {
                return CommandResult.FromError("Invalid time unit specified");
            }

            return await AddDoghouse(user, time, reason);
        }

        [Command(RunMode = RunMode.Async)]
        [Alias("add")]
        [Summary("Sends a user to the doghouse")]
        [Priority(5)]
        public async Task<RuntimeResult> AddDoghouse([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))] IUser user, TimeSpan time, [Remainder] string reason = "")
        {
            var guildUser = user as IGuildUser ?? await Context.Client.Rest.GetGuildUserAsync(Context.Guild.Id, user.Id);

            if (guildUser is null)
                return CommandResult.FromError("Unable to find user!");

            if (guildUser.Id == Context.Client.CurrentUser.Id)
                return CommandResult.FromError("I won't doghouse *myself*!");

            if (Context.Guild.CurrentUser.Hierarchy < guildUser.GetHierarchy())
                return CommandResult.FromError("I do not have permission to doghouse that user");

            if (time.TotalMilliseconds <= 0)
                return CommandResult.FromError("Please choose a time in the future");

            var role = Context.Guild.GetRole(DoghouseService.DoghouseRole);
            var result = await _service.AddDoghouse(Context, role, guildUser, time, reason);

            if (!result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError(result.Reason);
            }

            var humanTime = time.TotalHours > 2
                ? time.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour)
                : time.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);

            var history = _historyService.FetchHistory(guildUser.Id) switch
            {
                null => "(No known priors.)",
                var h => $"({h.DoghouseCount} previous doghousings, totalling ~ {h.Duration.Humanize(2, maxUnit: TimeUnit.Day, minUnit: TimeUnit.Hour)}.)",
            };

            await _loggerService.SendLogMessage($"DOGHOUSE: {Context.User.Mention} ({Context.User.ToString()}) added {guildUser.Mention} ({guildUser.ToString()}) to {role.Mention} for {humanTime} in {(Context.Channel as ITextChannel).Mention} reason: \"{Format.Sanitize(reason)}\" ([go to](<{Context.Message.GetJumpUrl()}>))\n{history}");
            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess(result.Reason);
        }


        [Command("list"), Priority(0)]
        [Alias("l")]
        [Summary("Lists doghoused users")]
        public async Task ListDoghouse([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))][Remainder] IGuildUser guildUser = null)
        {
            var doghouses = guildUser == null
                ? _service.GetDoghouseDatas()
                : _service.GetDoghouseDatas().Where(dh => dh.UserId == guildUser.Id);

            if (!doghouses.Any())
            {
                if (guildUser is null)
                    await ReplyAsync("No users currently doghoused");
                else
                    await ReplyAsync("Searched user is not doghoused");

                return;
            }

            var builder = new StringBuilder("```\n");

            foreach (var doghouse in doghouses.OrderBy(d => d.StartTime))
            {
                IUser user = Context.Guild.GetUser(doghouse.UserId);
                user ??= await Context.Client.Rest.GetUserAsync(doghouse.UserId);


                builder.Append($"{doghouse.StartTime.ToUniversalTime():u} - {user} - Time left: ");

                var timeLeft = doghouse.EndTime.ToUniversalTime() - DateTime.UtcNow;

                var humanTimeLeft = timeLeft.TotalHours > 2
                    ? timeLeft.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour)
                    : timeLeft.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);

                builder.Append(humanTimeLeft);

                if (!string.IsNullOrWhiteSpace(doghouse.Reason))
                    builder.Append($", Reason: {doghouse.Reason}");

                builder.AppendLine();
            }

            builder.Append(@"```");

            await ReplyAsync(builder.ToString());
        }

        [Command("listid")]
        public async Task ListDoghouseId([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))][Remainder] IGuildUser guildUser = null)
        {
            var doghouses = guildUser == null
                ? _service.GetDoghouseDatas()
                : _service.GetDoghouseDatas().Where(dh => dh.UserId == guildUser.Id);

            if (!doghouses.Any())
            {
                if (guildUser is null)
                    await ReplyAsync("No users currently doghoused");
                else
                    await ReplyAsync("Searched user is not doghoused");

                return;
            }

            var builder = new StringBuilder("```\n");

            foreach (var doghouse in doghouses.OrderBy(d => d.StartTime))
            {
                IUser user = Context.Guild.GetUser(doghouse.UserId);
                user ??= await Context.Client.Rest.GetUserAsync(doghouse.UserId);


                builder.Append($"{doghouse.StartTime.ToUniversalTime():u} - {user.Id} - {user} - Time left: ");

                var timeLeft = doghouse.EndTime.ToUniversalTime() - DateTime.UtcNow;

                var humanTimeLeft = timeLeft.TotalHours > 2
                    ? timeLeft.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour)
                    : timeLeft.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);

                builder.Append(humanTimeLeft);

                if (!string.IsNullOrWhiteSpace(doghouse.Reason))
                    builder.Append($", Reason: {doghouse.Reason}");

                builder.AppendLine();
            }

            builder.Append(@"```");

            await ReplyAsync(builder.ToString());
        }

        [Command("remove", RunMode = RunMode.Async), Priority(20)]
        [Summary("Removes a user from the doghouse")]
        [Alias("del")]
        public async Task<RuntimeResult> RemoveDoghouse([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))][Remainder] IGuildUser user)
        {
            var result = await _service.RemoveDoghouse(Context.Guild.GetRole(DoghouseService.DoghouseRole), user);

            if (!result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError(result.Reason);
            }

            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess(result.Reason);
        }

        [Command("remove", RunMode = RunMode.Async), Priority(15)]
        [Summary("Removes a user from the doghouse")]
        [Alias("del")]
        public async Task<RuntimeResult> RemoveDoghouse(ulong userId)
        {
            var result = _service.RemoveDoghouse(Context.Guild.GetRole(DoghouseService.DoghouseRole), userId);

            if (!result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
                return CommandResult.FromError(result.Reason);
            }

            await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            return CommandResult.FromSuccess(result.Reason);
        }

        [Command("history", RunMode = RunMode.Async), Priority(15)]
        [Summary("Displays a history of a user's doghouses.")]
        [Alias("h")]
        public async Task DoghouseHistory(ulong userId)
        {
            var historyData = _historyService.FetchHistory(userId);
            static string FormatHours(TimeSpan dur) => dur.TotalHours > 2
                    ? dur.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour)
                    : dur.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);
            string DogHouseFormat(DoghouseData data) => $"{data.StartTime.ToUniversalTime():u} - {FormatHours(data.Duration)} - {(string.IsNullOrWhiteSpace(data.Reason) ? "No Reason Given" : data.Reason)} ({Context.Guild.GetUser(data.PunisherId)})";


            if (historyData == null)
            {
                await ReplyAndDeleteAsync("Could not find history for user with that Id.", TimeSpan.FromSeconds(30));
                return;
            }
            IUser user = Context.Guild.GetUser(userId);
            user ??= await Context.Client.Rest.GetUserAsync(userId);

            StringBuilder builder = new StringBuilder($"{user?.Mention ?? MentionUtils.MentionUser(userId)} ({user?.ToString() ?? userId.ToString()}) has been doghoused for {FormatHours(historyData.Duration)} and served {historyData.DoghouseCount} sentences.\n```\n");
            historyData.Doghouses.ForEach(d => builder.AppendLine(DogHouseFormat(d)));
            builder.AppendLine("```\n");
            await ReplyAsync(builder.ToString(), allowedMentions: AllowedMentions.None);
            return;
        }

        [Command("history", RunMode = RunMode.Async), Priority(20)]
        [Summary("Displays a history of a user's doghouses.")]
        [Alias("h")]
        public async Task DoghouseHistory([OverrideTypeReader(typeof(LazyUserTypeReader<IGuildUser>))][Remainder] IGuildUser user)
        {
            await DoghouseHistory(user.Id);
            return;
        }
    }
}
