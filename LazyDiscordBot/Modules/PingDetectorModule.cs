﻿using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "pingDetector")]
    [RequireUserPermission(GuildPermission.ManageRoles, Group = "pingDetector", ErrorMessage = "")]
    [Group("pingdetector")]
    public class PingDetectorModule : GuildEnabler<PingDetectorService>
    {
        public PingDetectorModule(PingDetectorService service) : base(service)
        {
        }

        public override Task Disable() => base.Disable();
        public override Task Enable() => base.Enable();
        public override Task ShowStatus() => base.ShowStatus();

    }
}
