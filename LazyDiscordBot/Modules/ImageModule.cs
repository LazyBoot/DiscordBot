﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;
using Image = SixLabors.ImageSharp.Image;

namespace LazyDiscordBot.Modules
{
    [RequireContext(ContextType.Guild)]
    public class ImageModule : InteractiveBase
    {
        private readonly HttpClient _client;
        private readonly ElevationService _elevationService;

        public ImageModule(IHttpClientFactory client, ElevationService elevationService)
        {
            _elevationService = elevationService;
            _client = client.CreateClient();
        }

        [Command("jpegcompress", RunMode = RunMode.Async)]
        [Alias("jpeg")]
        [RequireElevatedUser(Group = "jpegcompress")]
        [RequireUserPermission(GuildPermission.ManageRoles, Group = "jpegcompress")]
        public async Task<RuntimeResult> JpegCompress(int quality = 1, int? pixelate = null)
        {
            const int numMessages = 20;

            _ = Context.Message.DeleteAsync();

            var messages = (await Context.Channel.GetMessagesAsync(numMessages).FlattenAsync()).OrderByDescending(m => m.Timestamp)
                                                                                               .FirstOrDefault(
                                                                                                   m => m.Attachments != null &&
                                                                                                        m.Attachments.FirstOrDefault()
                                                                                                         ?.Width > 0);

            if (messages == null)
                return CommandResult.FromError($"No image in the last {numMessages} messages");

            return await CompressTheImage(messages, quality, pixelate);
        }

        [Command("jpegcompress", RunMode = RunMode.Async)]
        [Alias("jpeg")]
        [RequireElevatedUser(Group = "jpegcompress")]
        [RequireUserPermission(GuildPermission.ManageRoles, Group = "jpegcompress")]
        [RequireReply]
        [Priority(10)]
        public async Task<RuntimeResult> JpegCompressReply(int quality = 1, int? pixelate = null)
        {
            _ = Context.Message.DeleteAsync();

            if (!Context.Message.ReferencedMessage.Attachments.Any(a => a.Width > 0))
                return CommandResult.FromError("Message has no image");

            return await CompressTheImage(Context.Message.ReferencedMessage, quality, pixelate);
        }

        public async Task<RuntimeResult> CompressTheImage(IMessage message, int quality = 1, int? pixelate = null)
        {

            var messageImage = message.Attachments.FirstOrDefault(a => a.Width > 0);

            var stream = new MemoryStream();
            using (var response = await _client.GetAsync(messageImage?.Url))
            {
                if (!response.IsSuccessStatusCode)
                    return CommandResult.FromError();

                var imageStream = response.Content;

                using var image = Image.Load(await imageStream.ReadAsStreamAsync());
                if (pixelate != null && pixelate > 0)
                    try
                    {
                        image.Mutate(i => i.Pixelate(pixelate.Value));
                    }
                    catch (ImageProcessingException e)
                    {
                        if (e.InnerException is ImageProcessingException ex)
                            e = ex;

                        if (e.InnerException is ArgumentOutOfRangeException)
                        {
                            await ReplyAndDeleteAsync("Pixel size too large", timeout: TimeSpan.FromSeconds(10));
                            return CommandResult.FromError();
                        }
                    }

                image.SaveAsJpeg(stream, new JpegEncoder() { Quality = quality });
            }

            stream.Position = 0;
            await Context.Channel.SendFileAsync(stream, Guid.NewGuid() + ".jpg");

            return CommandResult.FromSuccess();
        }
    }
}
