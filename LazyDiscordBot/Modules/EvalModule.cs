using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Preconditions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Modules
{

    [RequireOwner]
    public class EvalModule : ModuleBase<SocketCommandContext>
    {
        private readonly ILogger<EvalModule> _logger;
        private readonly IHttpClientFactory _httpClient;

        public EvalModule(ILogger<EvalModule> logger, IHttpClientFactory httpClient)
        {
            _logger = logger;
            _httpClient = httpClient;
        }

        [Command("eval"), Summary("Evaluates a snippet of C# code, in context.")]
        public async Task EvaluateAsync([Remainder, Summary("Code to evaluate.")] string code)
        {
            var cs1 = code.IndexOf("```") + 3;
            cs1 = code.IndexOf('\n', cs1) + 1;
            var cs2 = code.LastIndexOf("```");

            if (cs1 == -1 || cs2 == -1)
                throw new ArgumentException("You need to wrap the code into a code block.", nameof(code));

            code = code.Substring(cs1, cs2 - cs1);

            var embed = new EmbedBuilder
            {
                Title = "Evaluating...",
                Color = new Color(0xD091B2)
            };
            var msg = await ReplyAsync("", embed: embed.Build());

            var globals = new EvaluationEnvironment(Context);
            var sopts = ScriptOptions.Default
                .WithImports("System", "System.Collections.Generic", "System.Diagnostics", "System.Linq", "System.Net.Http", "System.Net.Http.Headers", "System.Reflection", "System.Text",
                             "System.Threading.Tasks",
                             "Discord", "Discord.Commands", "Discord.Rest", "Discord.WebSocket", "Fergun.Interactive",
                             "LazyDiscordBot", "LazyDiscordBot.ExtensionClasses", "LazyDiscordBot.Models", "LazyDiscordBot.Modules", "LazyDiscordBot.Services",
                             "Humanizer", "Humanizer.Localisation")
                .WithReferences(AppDomain.CurrentDomain.GetAssemblies().Where(xa => !xa.IsDynamic && !string.IsNullOrWhiteSpace(xa.Location)));

            var sw1 = Stopwatch.StartNew();
            var cs = CSharpScript.Create(code, sopts, typeof(EvaluationEnvironment));
            var csc = cs.Compile();
            sw1.Stop();

            if (csc.Any(xd => xd.Severity == DiagnosticSeverity.Error))
            {
                embed = new EmbedBuilder
                {
                    Title = "Compilation failed",
                    Description = string.Concat("Compilation failed after ", sw1.ElapsedMilliseconds.ToString("#,##0"), "ms with ", csc.Length.ToString("#,##0"), " errors."),
                    Color = new Color(0xD091B2)
                };
                foreach (var xd in csc.Take(3))
                {
                    var ls = xd.Location.GetLineSpan();
                    embed.AddField(string.Concat("Error at ", ls.StartLinePosition.Line.ToString("#,##0"), ", ", ls.StartLinePosition.Character.ToString("#,##0")), Format.Code(xd.GetMessage()), false);
                }
                if (csc.Length > 3)
                {
                    embed.AddField("Some errors ommited", string.Concat((csc.Length - 3).ToString("#,##0"), " more errors not displayed"), false);
                }
                await msg.ModifyAsync(m => m.Embed = embed.Build());
                return;
            }

            Exception rex = null;
            ScriptState<object> css = null;
            var sw2 = Stopwatch.StartNew();
            try
            {
                css = await cs.RunAsync(globals);
                rex = css.Exception;
            }
            catch (Exception ex)
            {
                rex = ex;
            }
            sw2.Stop();

            if (rex != null)
            {
                embed = new EmbedBuilder
                {
                    Title = "Execution failed",
                    Description = string.Concat("Execution failed after ", sw2.ElapsedMilliseconds.ToString("#,##0"), "ms with `", rex.GetType(), ": ", rex.Message, "`."),
                    Color = new Color(0xD091B2),
                };
                await msg.ModifyAsync(m => m.Embed = embed.Build());
                return;
            }

            // execution succeeded
            embed = new EmbedBuilder
            {
                Title = "Evaluation successful",
                Color = new Color(0xD091B2),
            };

            embed.AddField("Result", css.ReturnValue != null ? css.ReturnValue.ToString() : "No value returned", false)
                .AddField("Compilation time", string.Concat(sw1.ElapsedMilliseconds.ToString("#,##0"), "ms"), true)
                .AddField("Execution time", string.Concat(sw2.ElapsedMilliseconds.ToString("#,##0"), "ms"), true);

            if (css.ReturnValue != null)
                embed.AddField("Return type", css.ReturnValue.GetType().ToString(), true);

            await msg.ModifyAsync(m => m.Embed = embed.Build());
        }

    }

    public sealed class EvaluationEnvironment
    {
        public SocketCommandContext Context { get; }

        public SocketUserMessage Message => Context.Message;
        public ISocketMessageChannel Channel => Context.Channel;
        public SocketGuild Guild => Context.Guild;
        public SocketUser User => Context.User;
        public SocketGuildUser Member => Context.User as SocketGuildUser;
        public DiscordSocketClient Client => Context.Client;

        public EvaluationEnvironment(SocketCommandContext context)
        {
            Context = context;
        }

        public async Task<IUserMessage> ReplyAsync(string message = null, bool isTTS = false, Embed embed = null, RequestOptions options = null, AllowedMentions allowedMentions = null)
        {
            return await Context.Channel.SendMessageAsync(message, isTTS, embed, options, allowedMentions).ConfigureAwait(false);
        }
    }
}
