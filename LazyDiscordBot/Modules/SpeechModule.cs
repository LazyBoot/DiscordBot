﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    //   public class SpeechModule : InteractiveModule
    //   {
    //       private readonly AudioService _audioService;
    //       private readonly SpeechService _speechService;

    //       public SpeechModule(AudioService audioService, SpeechService speechService)
    //       {
    //           _audioService = audioService;
    //           _speechService = speechService;
    //       }

    //       [Command("say", RunMode = RunMode.Async)]
    //	[RequireElevatedUser]
    //       public async Task Say([Remainder] string text)
    //       {
    //		await _speechService.Say(_audioService, Context, text);
    //		await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
    //       }

    //       [Command("sayfile", RunMode = RunMode.Async)]
    //       [RequireElevatedUser]
    //       public async Task SayFile()
    //       {
    //        Attachment attachment = Context.Message.Attachments.FirstOrDefault();
    //        if (attachment != null)
    //        {
    //	        string stringToSay;
    //	        using (var client = new HttpClient())
    //	        {
    //		        using (Stream stream = await client.GetStreamAsync(attachment.Url))
    //		        using (var streamReader = new StreamReader(stream))
    //		        {
    //			        stringToSay = streamReader.ReadToEnd();
    //		        }
    //	        }
    //			await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
    //	        await _speechService.Say(_audioService, Context, stringToSay);
    //		}
    //		else
    //	        await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
    //       }
    //}
}
