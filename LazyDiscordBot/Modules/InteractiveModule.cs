﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;

// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    [RequireElevatedUser(Group = "interactiveAdmin")]
    [RequireUserPermission(GuildPermission.ManageChannels, Group = "interactiveAdmin")]
    public class InteractiveModule : InteractiveBase
    {
        [Command("timedecho", RunMode = RunMode.Async)]
        public Task TimedEchoAsync(int timeout, [Remainder] string text)
            // Insert a ZWSP before the text to prevent triggering other bots!
            => ReplyAndDeleteAsync('\u200B' + text, timeout: TimeSpan.FromSeconds(timeout));

        [Command("timedechod", RunMode = RunMode.Async)]
        public async Task TimedEchoDAsync(int timeout, [Remainder] string text)
        {
            await TimedEchoAsync(timeout, text);
            await Context.Message.DeleteAsync();
        }

        [Command("react")]
        public Task React(string emote = "🎉")
            => ReactToMessage(emote, Context.Message.Id);

        [Command("reactnext", RunMode = RunMode.Async)]
        public async Task ReactNext(string emote = "🎉")
        {
            var botMessage = await ReplyAsync("Reacting to next message with " + emote);
            var nextMessage = await Interactive.NextMessageAsync();
            if (nextMessage.IsSuccess && nextMessage.Value is IUserMessage message)
            {
                await message.AddReactionAsync(GetEmote(emote));
            }

            await botMessage.DeleteAsync();
        }

        private static IEmote GetEmote(string inEmote) 
            => inEmote.GetEmote();

        [Command("react", RunMode = RunMode.Async)]
        [Priority(10)]
        public async Task<RuntimeResult> React(string emote, ulong id, IMessageChannel chan)
            => await React(emote, id, chan.Id);

        [Command("react", RunMode = RunMode.Async)]
        [Priority(5)]
        public async Task<RuntimeResult> React(string emote, ulong id, ulong? chan = null)
        {
            var result = await ReactToMessage(emote, id, chan);
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }

        [Command("reactd", RunMode = RunMode.Async)]
        [Priority(10)]
        public async Task<RuntimeResult> ReactD(string emote, ulong id, IMessageChannel chan)
            => await ReactD(emote, id, chan.Id);

        [Command("reactd", RunMode = RunMode.Async)]
        [Priority(5)]
        public async Task<RuntimeResult> ReactD(string emote, ulong id, ulong? chan = null)
        {
            var result = await ReactToMessage(emote, id, chan);
            if (result.IsSuccess)
                await Context.Message.DeleteAsync();

            return result;
        }

        [Command("reactrem", RunMode = RunMode.Async)]
        [Priority(10)]
        public async Task<RuntimeResult> ReactRem(string emote, ulong id, IMessageChannel chan)
            => await ReactRem(emote, id, chan.Id);

        [Command("reactrem", RunMode = RunMode.Async)]
        [Priority(5)]
        public async Task<RuntimeResult> ReactRem(string emote, ulong id, ulong? chan = null)
        {
            var result = await ReactToMessage(emote, id, chan, true);
            if (result.IsSuccess)
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);

            return result;
        }


        private async Task<RuntimeResult> ReactToMessage(string emote, ulong message, ulong? channel = null, bool remove = false)
        {
            channel ??= Context.Channel.Id;

            if (channel != null)
            {
                if (remove)
                    await Context.Client.Rest.RemoveReactionAsync(channel.Value, message, Context.Client.CurrentUser.Id, GetEmote(emote));
                else
                    await Context.Client.Rest.AddReactionAsync(channel.Value, message, GetEmote(emote));

                return CommandResult.FromSuccess();

            }

            return CommandResult.FromError("Can't find channel");
        }
    }
}
