using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Services;

// ReSharper disable UnusedMember.Global

namespace LazyDiscordBot.Modules
{
    [Group("customembed")]
    [Alias("cce")]
    [RequireContext(ContextType.Guild)]
    [RequireUserPermission(GuildPermission.ManageChannels, Group = "customembed")]
    [RequireElevatedUser(Group = "customembed")]
    public class CustomEmbedModule : InteractiveBase
    {
        private readonly CustomEmbedService _customEmbedService;

        public CustomEmbedModule(CustomEmbedService customEmbedService)
        {
            _customEmbedService = customEmbedService;
        }

        [Command("add")]
        public async Task<RuntimeResult> AddCommand(string name, string title, [Remainder] string message)
        {
            var result = await _customEmbedService.AddCommandAsync(Context.Guild.Id, name.ToLowerInvariant(), title, message, Context.Message.Attachments.FirstOrDefault());
            if (result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            }

            return result;
        }

        [Command("update")]
        [Alias("edit")]
        public async Task<RuntimeResult> UpdateCommand(string name, string title, [Remainder] string message)
        {
            var result = await _customEmbedService.UpdateCommandAsync(Context.Guild.Id, name.ToLowerInvariant(), title, message, Context.Message.Attachments.FirstOrDefault());
            if (result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            }

            return result;
        }

        [Command("remove")]
        [Alias("delete", "del")]
        public async Task<RuntimeResult> RemoveCommand(string name)
        {
            var result = await _customEmbedService.RemoveCommandAsync(Context.Guild.Id, name.ToLowerInvariant());
            if (result.IsSuccess)
            {
                await Context.Message.AddReactionAsync(CommandResult.SuccessReaction);
            }
            else
            {
                await Context.Message.AddReactionAsync(CommandResult.FailedReaction);
            }

            return result;
        }

        [Command("list")]
        [Alias("show")]
        public async Task ListCommands()
        {
            var stringBuilder = new StringBuilder();
            var commands = _customEmbedService.ListCommands(Context.Guild.Id).ToList();
            if (commands.Any())
            {
                stringBuilder.AppendLine("These are the currently set custom commands:");
                foreach (var customCommand in commands)
                {
                    stringBuilder.AppendLine(customCommand.Name);
                }

            }
            else
            {
                stringBuilder.AppendLine("No custom commands set");
            }

            await ReplyAsync(stringBuilder.ToString());
        }
    }
}
