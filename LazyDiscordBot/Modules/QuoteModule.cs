﻿using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Modules
{
    public class QuoteModule : ModuleBase<SocketCommandContext>
    {
        private readonly QuoteService _quoteService;

        public QuoteModule(QuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        [Command("quote")]
        [Alias("q")]
        [RequireBotPermission(ChannelPermission.EmbedLinks)]
        [RequireContext(ContextType.Guild)]
        public async Task<RuntimeResult> QuoteMessage(ulong id, ulong guildId = 0)
        {
            var message = await Context.Channel.GetMessageAsync(id);

            if (message == null)
            {
                var guild = Context.Client.GetGuild(guildId) ?? Context.Guild;

                foreach (var channel in guild.TextChannels)
                {
                    if (channel.Id == Context.Channel.Id)
                        continue;

                    if (!Context.Guild.CurrentUser.GetPermissions(channel).ViewChannel)
                        continue;

                    message = await channel.GetMessageAsync(id);

                    if (message != null)
                        break;
                }
            }

            if (message == null)
                return CommandResult.FromError("Could not find message");

            return await _quoteService.SendQuote(Context, message);
        }

        [Command("quote")]
        [Alias("q")]
        [RequireBotPermission(ChannelPermission.EmbedLinks)]
        [RequireContext(ContextType.Guild)]
        public async Task<RuntimeResult> QuoteMessage(string messageLink)
        {
            return await _quoteService.SendLinkQuote(Context, messageLink);
        }

    }
}
