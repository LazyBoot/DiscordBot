﻿using System.Collections.Generic;

namespace LazyDiscordBot.Models
{
    public class BotConfig
    {
        public string BotToken { get; set; }
        public List<Admin> Admins { get; set; }
	    public ulong? LogChannel { get; set; }
	    public string LogWebhook { get; set; }
        public string DefaultPrefix { get; set; } = ".";
        public bool CheckReboot { get; set; } = false;
    }

    public class Admin
    {
        public string Name { get; set; }
        public ulong Id { get; set; }
    }
}
