﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace LazyDiscordBot;

public class TimerData : IDisposable
{
    public Guid Id { get; set; }
    public CancellationTokenSource cancellationTokenSource { get; set; }
    public bool Running { get; private set; } = false;
    private Task _task;

    public TimerData()
    {
        cancellationTokenSource = new CancellationTokenSource();
    }

    public TimerData(Guid guid) : this()
    {
        Id = guid;
    }

    public bool StopTimer()
    {
        try
        {
            cancellationTokenSource.Cancel();
        }
        catch
        {
            return false;
        }
        return true;
    }

    public bool StartTimer(DateTime endTime, Action action)
        => StartTimer(endTime.ToUniversalTime() - DateTime.UtcNow, action);

    public bool StartTimer(TimeSpan timeSpan, Action action)
    {
        if (cancellationTokenSource.IsCancellationRequested)
            if (!cancellationTokenSource.TryReset())
                return false;

        if (timeSpan < TimeSpan.Zero)
            action.Invoke();
        else
        {
            _task = Task.Delay(timeSpan, cancellationTokenSource.Token)
                        .ContinueWith(t => action.Invoke(), TaskContinuationOptions.NotOnCanceled);
            Running = true;
        }

        return true;
    }


    public void Dispose()
    {
        cancellationTokenSource.Cancel();
        cancellationTokenSource.Dispose();
        cancellationTokenSource = null;
    }
}
