﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LazyDiscordBot.Models
{
    public class WhiteListData
    {
        public string Ucid { get; set; }
        public string Name { get; set; }
        public string DiscordUser { get; set; }

        public override string ToString()
        {
            return $"ucid={Uri.EscapeDataString(Ucid)}&name={Uri.EscapeDataString(Name)}&discordHandle={Uri.EscapeDataString(DiscordUser)}";
        }
    }
}
