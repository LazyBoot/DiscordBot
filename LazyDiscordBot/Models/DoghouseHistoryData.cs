using System;
using System.Linq;
using LiteDB;
using System.Collections.Generic;

namespace LazyDiscordBot.Models
{
    /// <summary>
    /// DTO for history data about people in the doghouse.
    /// </summary>
    public class DoghouseHistoryData
    {
        [BsonId(false)]
        public double UserId { get; set; }
        [BsonIgnore]
        public TimeSpan Duration
        {
            get { return Doghouses.Aggregate(TimeSpan.FromSeconds(0), (acc, dh) => acc + dh.Duration); }
        }
        [BsonIgnore]
        public int DoghouseCount
        {
            get { return Doghouses.Count; }
        }
        public List<DoghouseData> Doghouses { get; set; }

        private DoghouseHistoryData() { }

        public DoghouseHistoryData(double userId, List<DoghouseData> doghouses)
        {
            UserId = userId;
            Doghouses = doghouses;
        }

        public void AddDoghouseData(DoghouseData dhData)
        {
            //Ensure they are for the same ID. Adding another user's history makes no sense.
            if (dhData.UserId != UserId) throw new ArgumentException("Can not add doghouse info from a different user.", nameof(dhData));
            Doghouses.Add(dhData);
            return;
        }

    }
}
