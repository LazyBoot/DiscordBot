﻿using Discord;
using Discord.Commands;

namespace LazyDiscordBot.Models
{
    public class CommandResult : RuntimeResult
    {
        public static Emoji SuccessReaction = new Emoji("✅");
        public static Emoji FailedReaction = new Emoji("❌");

        public CommandResult(CommandError? error, string reason) : base(error, reason)
        {
        }

        public static CommandResult FromError(string reason = null)
            => new CommandResult(CommandError.Unsuccessful, reason);

        public static CommandResult FromSuccess(string reason = null)
            => new CommandResult(null, reason);

    }
}
