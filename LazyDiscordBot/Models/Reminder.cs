﻿using System;
using LiteDB;

namespace LazyDiscordBot.Models
{
    public class Reminder : ISupportsTimer
    {
        [BsonId]
        public Guid Id { get; set; }
        public ulong User { get; set; }
        [BsonIgnore] public DateTime EndTime => Time;
        public DateTime Time { get; set; }
        public string Message { get; set; }
        public string MessageLink { get; set; }

        public Reminder()
        {

        }

        public Reminder(ulong user, DateTime time, string message)
        {
            Id = Guid.NewGuid();
            User = user;
            Time = time;
            Message = message;
        }

        public Reminder(ulong user, DateTime time, string message, string messageLink) 
            : this(user, time, message)
        {
            MessageLink = messageLink;
        }
    }
}
