﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace LazyDiscordBot.Models
{
	public class AzureAuthentication
	{
		private readonly string _subscriptionKey;
		private readonly string _tokenFetchUri;

		public AzureAuthentication(string tokenFetchUri, string subscriptionKey)
		{
			if (string.IsNullOrWhiteSpace(tokenFetchUri))
			{
				throw new ArgumentNullException(nameof(tokenFetchUri));
			}
			if (string.IsNullOrWhiteSpace(subscriptionKey))
			{
				throw new ArgumentNullException(nameof(subscriptionKey));
			}
			this._tokenFetchUri = tokenFetchUri;
			this._subscriptionKey = subscriptionKey;
		}

		public async Task<string> FetchTokenAsync()
        {
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", this._subscriptionKey);
            UriBuilder uriBuilder = new UriBuilder(this._tokenFetchUri);

            var result = await client.PostAsync(uriBuilder.Uri.AbsoluteUri, null).ConfigureAwait(false);
            return await result.Content.ReadAsStringAsync().ConfigureAwait(false);
        }
	}
}