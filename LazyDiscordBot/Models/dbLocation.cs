﻿namespace LazyDiscordBot.Models
{
    public class DbLocation
    {
        public string Path { get; }

        public DbLocation(string path)
        {
            Path = path;
        }
    }
}
