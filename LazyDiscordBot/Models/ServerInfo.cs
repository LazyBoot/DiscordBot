﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace LazyDiscordBot.Models
{
    public class ServerInfo
    {
        public string ShortId { get; set; }
        public string Name { get; set; }
        public Uri StatsUrl { get; set; }
        public int PenaltyTimeout { get; set; }
        public string Token { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string BearerToken = "";
        public DateTime ValidTo = DateTime.MinValue;



        public static List<ServerInfo> GetAll(ulong discordServer)
        {
            if (discordServer == Guilds.LazyTest)
                return ServerInfoContainer.GetAll().SelectMany(s => s.DCSServers).ToList();

            return ServerInfoContainer.GetAll().Where(s => s.DiscordServer == discordServer).FirstOrDefault()?.DCSServers;
        }
    }

    internal static class ServerInfoHelper
    {
        public static ServerInfo Get(this List<ServerInfo> serverInfo, string shortId)
        {
            return serverInfo?.FirstOrDefault(s => s.ShortId.Equals(shortId, StringComparison.OrdinalIgnoreCase));
        }
    }

    public class ServerInfoContainer
    {
        public ulong DiscordServer { get; set; }
        public List<ServerInfo> DCSServers { get; set; }

        private static readonly string ServerDataJson = Path.Join(Program.ProgramPath, @"Data/server-data.json");

        public static List<ServerInfoContainer> GetAll()
        {
            return JsonSerializer.Deserialize<List<ServerInfoContainer>>(File.ReadAllText(ServerDataJson));
        }
    }
}
