﻿namespace LazyDiscordBot.Models
{
    public struct DFA
    {
        public struct Roles
        {
            public const ulong RedBoi = 991337529649418340;
            public const ulong MissionDev = 567548777385164822;
        }

        public struct Channels
        {
            public const ulong TheRedZone = 991336992031916053;
            public const ulong MissionDevPeeps = 567548026034585600;
            public const ulong TagsOnly = 502859966424678410;
            public const ulong LazybotLookups = 1229748247854059550;

        }
    }
}
