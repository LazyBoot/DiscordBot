﻿using System.Collections.Generic;

namespace LazyDiscordBot.Models
{
    public class AudioFiles
    {
        public IList<AudioFile> Files;

        public AudioFiles()
        {
            Files = new List<AudioFile>();
        }
    }

    public class AudioFile
    {
        public string Name { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }

        public AudioFile()
        {
        }

        public AudioFile(string fileName, string name, string description)
        {
            Name = name;
            FileName = fileName;
            Description = description;
        }
        public AudioFile(string fileName, string name) : this(fileName, name, string.Empty)
        {
        }
    }
}
