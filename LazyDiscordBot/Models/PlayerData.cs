﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace LazyDiscordBot.Models
{
    public partial class PlayerData
    {
        [JsonPropertyName("uID")]
        public string UId { get; set; }

        [JsonPropertyName("slID")]
        public long SlId { get; set; }

        [JsonPropertyName("joinDate")]
        public DateTime? JoinDate { get; set; }

        [JsonPropertyName("lastJoin")]
        public DateTime? LastJoin { get; set; }

        [JsonPropertyName("uIDbanned")]
        public bool UidBanned { get; set; }

        [JsonPropertyName("IPbanned")]
        public bool IpBanned { get; set; }

        [JsonPropertyName("friendlyKills")]
        public Dictionary<int, Teamkill> Teamkills { get; set; }

        [JsonPropertyName("friendlyCollisionKills")]
        public Dictionary<int, Teamkill> TeamCollisions { get; set; }

        [JsonPropertyName("names")]
        public Dictionary<int, string> Names { get; set; }

        [JsonPropertyName("admin")]
        public bool Admin { get; set; } = false;
    }

    public class Teamkill
    {
        [JsonPropertyName("objCat")]
        public string ObjCat { get; set; }

        [JsonPropertyName("objTypeName")]
        public string ObjTypeName { get; set; }

        [JsonPropertyName("shotFrom")]
        public string ShotFrom { get; set; }

        [JsonPropertyName("forgiven")]
        public bool Forgiven { get; set; }

        [JsonPropertyName("human")]
        public Dictionary<int, string> Humans { get; set; }

        [JsonPropertyName("time")]
        public DateTime Time { get; set; }

        [JsonPropertyName("weapon")]
        public string Weapon { get; set; }
    }

    public partial class PlayerData
    {
        public static Dictionary<string, PlayerData> FromJson(string json)
            => JsonSerializer.Deserialize<Dictionary<string, PlayerData>>(json);
    }

    public static class Serialize
    {
        public static string ToJson(this PlayerData self) => JsonSerializer.Serialize(self);
    }
}
