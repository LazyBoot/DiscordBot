﻿namespace LazyDiscordBot.Models
{
    internal enum PenDataResult
    {
        Found,
        NotFound,
        Error
    }
}
