﻿namespace LazyDiscordBot.Models
{
    public struct HoggitRoles
    {
        public const ulong Instructors = 286510497145159680;
        public const ulong DiscordModerator = 712381879210213446;
        public const ulong SlotRequest = 802296464666918923;

        public const ulong Doghouse = 271116476839100416;
        public const ulong Inert = 645033870810415137;
        public const ulong TestRole = 637085337990070274;
    }
}
