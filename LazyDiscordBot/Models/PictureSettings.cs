﻿using System.Collections.Generic;
using Discord.Commands;
using LiteDB;
using static LazyDiscordBot.Services.PictureService;

namespace LazyDiscordBot.Models
{
    [NamedArgumentType]
    public class PictureSettings
    {
        [BsonId]
        public int Id { get; set; }
        public int BaseCooldown { get; set; }
        public int MaxCoolDown { get; set; }
        public int TimerInterval { get; set; }
        public Dictionary<PictureType, string> ApiKeys { get; set; }
    }
}
