﻿using System;
using LiteDB;

namespace LazyDiscordBot.Models
{
    public class DoghouseData : ISupportsTimer
    {
        [BsonId]
        public Guid Id { get; set; }
        public ulong UserId { get; set; }
        public ulong PunisherId { get; set; }
        public string Reason { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        [BsonIgnore]
        public TimeSpan Duration
        {
            get { return EndTime - StartTime; }
        }

        public DoghouseData()
        {
        }

        public DoghouseData(ulong userId, ulong punisherId, TimeSpan duration, string reason)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            PunisherId = punisherId;
            StartTime = DateTime.UtcNow;
            EndTime = duration == TimeSpan.MaxValue ? DateTime.MaxValue : StartTime + duration;
            Reason = reason;
        }

        public DoghouseData Copy(TimeSpan? newDuration = null, string newReason = null)
        {
            var newData = new DoghouseData
            {
                Id = Id,
                UserId = UserId,
                StartTime = StartTime,
                EndTime = EndTime,
                Reason = Reason
            };

            if (newDuration.HasValue)
            {
                newData.StartTime = DateTime.UtcNow;
                newData.EndTime = newDuration == TimeSpan.MaxValue ? DateTime.MaxValue : newData.StartTime + newDuration.Value;
            }

            if (!string.IsNullOrWhiteSpace(newReason))
                newData.Reason = newReason;

            return newData;
        }

    }
    public class InertData
    {
        [BsonId]
        public Guid Id { get; set; }
        public ulong UserId { get; set; }
        public string Reason { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public InertData()
        {
        }

        public InertData(ulong userId, TimeSpan duration, string reason = null)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            StartTime = DateTime.UtcNow;
            EndTime = duration == TimeSpan.MaxValue ? DateTime.MaxValue : StartTime + duration;
            Reason = reason ?? string.Empty;
        }
    }
}
