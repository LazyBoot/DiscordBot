﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using LiteDB;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace LazyDiscordBot
{
    class Program
    {
        public static string ProgramPath { get; private set; }
        internal static ILiteCollection<Utility.UpdateStatus> UpdateStatus;

        public static void Main(string[] args)
        {
            ProgramPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog((context, configuration) =>
                {
                    const string outputTemplate =
                        "[{Timestamp:yyyy-MM-dd HH:mm:ss zzz}] {Level:u3} <{SourceContext}> \t{Message:lj}{NewLine}{Exception}";

                    if (context.HostingEnvironment.IsDevelopment())
                        configuration.MinimumLevel.Verbose()
                                     .MinimumLevel.Override("Microsoft.Extensions.Http.DefaultHttpClientFactory", LogEventLevel.Information);

                    configuration
                        .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                        .MinimumLevel.Override("System.Net.Http.HttpClient", LogEventLevel.Information)
                        .WriteTo.Console(outputTemplate: outputTemplate, theme: AnsiConsoleTheme.Code)
                        .WriteTo.Async(a => a.File(Path.Combine(ProgramPath, "Data", "Logs", ".log"),
                            rollingInterval: RollingInterval.Day,
                            retainedFileCountLimit: 5,
                            restrictedToMinimumLevel: LogEventLevel.Information,
                            outputTemplate: outputTemplate))
                        .Enrich.FromLogContext();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>().ConfigureAppConfiguration(config =>
                    {
                        config.AddJsonFile(Path.Join(ProgramPath, @"token.json"), true)
                              .AddEnvironmentVariables("DISCORDBOT_");
                    });
                }).ConfigureServices((hostContext, services) => { services.AddHostedService<DiscordInteractions>(); });


        public static Task LogInfo(LogMessage msg)
        {
            var logger = Log.Logger;
            if (msg.Source != null)
                logger = logger.ForContext(Constants.SourceContextPropertyName, msg.Source);

            var level = LogLevelFromSeverity(msg.Severity);

            if (msg.Exception != null)
                logger.Write(level, msg.Exception, msg.Message);
            else
                logger.Write(level, msg.Message);

            return Task.CompletedTask;
        }

        private static LogEventLevel LogLevelFromSeverity(LogSeverity severity)
            => (LogEventLevel) Math.Abs((int) severity - 5);
    }
}
