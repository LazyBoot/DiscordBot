﻿using System.Threading.Tasks;
using Discord.Commands;
using LazyDiscordBot.Models;


namespace LazyDiscordBot.Interfaces_Abstracts
{
    public abstract class GuildEnabler<T> : ModuleBase<SocketCommandContext> where T : IEnableDisableGuildService
    {
        private readonly T _service;

        public GuildEnabler(T service)
        {
            _service = service;
        }

        [Command()]
        [Alias("show", "status")]
        [Priority(0)]
        public virtual Task ShowStatus()
            => ReplyAsync(_service.ServiceName + " is currently: " + (_service.IsGuildEnabled(Context.Guild.Id) ? "`Enabled`" : "`Disabled`"));


        [Command("enable")]
        [Priority(5)]
        public virtual Task Enable()
            => _service.EnableGuild(Context.Guild.Id)
                ? Context.Message.AddReactionAsync(CommandResult.SuccessReaction)
                : Context.Message.AddReactionAsync(CommandResult.FailedReaction);

        [Command("disable")]
        [Priority(5)]
        public virtual Task Disable()
            => _service.DisableGuild(Context.Guild.Id)
                ? Context.Message.AddReactionAsync(CommandResult.SuccessReaction)
                : Context.Message.AddReactionAsync(CommandResult.FailedReaction);

    }
}
