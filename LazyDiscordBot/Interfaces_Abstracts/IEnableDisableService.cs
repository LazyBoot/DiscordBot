﻿
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Interfaces_Abstracts
{
    public interface IEnableDisableGuildService : IBotService
    {
        public string ServiceName { get; }
        public bool EnableGuild(ulong guildId);
        public bool DisableGuild(ulong guildId);
        public bool IsGuildEnabled(ulong guildId);

    }
}
