using System.Threading.Tasks;
using LazyDiscordBot.Services;

namespace LazyDiscordBot.Interfaces_Abstracts
{
    public interface IStopOnShutdown : IBotService
    {
        Task Stop();
    }
}
