﻿using System;

namespace LazyDiscordBot;

public interface ISupportsTimer
{
    public Guid Id { get; }
    public DateTime EndTime { get; }
}
