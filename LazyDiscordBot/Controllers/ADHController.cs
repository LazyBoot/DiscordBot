using System.Linq;
using LazyDiscordBot.Services;
using Microsoft.AspNetCore.Mvc;

namespace LazyDiscordBot.Controllers;

[Route("api/ADH")]
[ApiController]
public class ADHController : ControllerBase
{
    private readonly AutoDoghouseService _autoDoghouseService;

    public ADHController(AutoDoghouseService autoDoghouseService)
    {
        _autoDoghouseService = autoDoghouseService;
    }

    [HttpGet("{id}")]
    public IActionResult ListADH(ulong id)
    {
        return Ok(_autoDoghouseService.ListTriggers(id).Select(w => w.Name.ToString()).OrderBy(w => w));
    }

    [HttpGet("delete/{id}/{number}")]
    public IActionResult DeleteADH(ulong id, int number)
    {
        var triggers = _autoDoghouseService.ListTriggers(id).Select(w => w.Name.ToString()).OrderBy(w => w);
        if (triggers.Any())
        {
            _autoDoghouseService.DeleteTrigger(id, triggers.ElementAt(number));
            return Ok();
        }
        return BadRequest();
    }
}
