﻿using System.Collections.Generic;
using System.Linq;
using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.Mvc;

namespace LazyDiscordBot.Controllers
{
    [Route("api/test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly DiscordSocketClient _client;

        public TestController(DiscordSocketClient client)
        {
            _client = client;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DGuild>> GetGuilds()
        {
            var guilds = _client.Guilds.OrderBy(g => g.Name).Select(g=> new DGuild(g)).ToList();
            return guilds;
        }
    }

    public class DGuild
    {
        public ulong Id { get; set; }
        public string Name { get; set; }

        public DGuild(IGuild guild)
        {
            Id = guild.Id;
            Name = guild.Name;
        }
    }
}
