using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Fergun.Interactive;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using LazyDiscordBot.Services;
using LiteDB;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LazyDiscordBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            ConfigureDiscordBotServices(services);

            services.AddOptions().Configure<BotConfig>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public void ConfigureDiscordBotServices(IServiceCollection serviceCollection)
        {
            var liteDb = new LiteDatabase($"Filename={Path.Combine(Program.ProgramPath, "Data", @"discordBot.db")}");
            liteDb.UtcDate = true;

            IEnumerable<Type> services = Utility.GetBotServices<IBotService>();

            var intentsList = services.Select(s => s.GetCustomAttribute<RequireIntents>()).Distinct().Where(i => i is not null);

            GatewayIntents intents = GatewayIntents.Guilds | GatewayIntents.GuildMembers;

            foreach (var item in Enum.GetValues<GatewayIntents>())
            {
                if (!intents.HasFlag(item) && intentsList.Any(i => i.Intents.HasFlag(item)))
                    intents = intents | item;
            }


            serviceCollection
                .AddLogging(
                    builder => builder.AddSerilog()
                )
                .AddSingleton(liteDb)
                .AddSingleton(new DiscordSocketClient(new DiscordSocketConfig() { MessageCacheSize = 200, LogLevel = LogSeverity.Verbose, GatewayIntents = intents, FormatUsersInBidirectionalUnicode = false }))
                .AddSingleton(new LogWebhookClient(Configuration.Get<BotConfig>().LogWebhook))
                .AddSingleton<InteractiveService>()
                .AddSingleton(new CommandService(new CommandServiceConfig() { IgnoreExtraArgs = true, DefaultRunMode = RunMode.Async }));

            foreach (var service in services)
            {
                serviceCollection.AddSingleton(service);
            }

            serviceCollection
                .AddHttpClient()
                .AddHttpClient("noRedirect").ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler() { AllowAutoRedirect = false });
        }
    }
}
