using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace LazyDiscordBot.Preconditions
{
    public class RequireReply : PreconditionAttribute
    {
        public override string ErrorMessage { get; set; } = string.Empty;

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult(context.Message.ReferencedMessage != null
                                       ? PreconditionResult.FromSuccess()
                                       : PreconditionResult.FromError(ErrorMessage));
        }
    }
}
