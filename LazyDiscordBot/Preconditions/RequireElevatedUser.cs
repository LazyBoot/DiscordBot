﻿using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LazyDiscordBot.Preconditions
{
    public class RequireElevatedUser : PreconditionAttribute
    {
        public override string ErrorMessage { get; set; } = "You do not have access to that command.";

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            var filter = services.GetRequiredService<ElevationService>();
            return Task.FromResult(filter.IsAdmin(context.User as SocketUser)
                                       ? PreconditionResult.FromSuccess()
                                       : PreconditionResult.FromError(ErrorMessage));
        }
    }
}
