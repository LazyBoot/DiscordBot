﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace LazyDiscordBot.Preconditions
{
    public class RequireRole : PreconditionAttribute
    {
        public ulong GuildId { get; }
        public ulong RoleId { get; }
        public override string ErrorMessage { get; set; } = string.Empty;

        public RequireRole(ulong guildId, ulong roleId)
        {
            GuildId = guildId;
            RoleId = roleId;
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (context.Guild is null || GuildId != context.Guild.Id)
                return Task.FromResult(PreconditionResult.FromError(string.Empty));

            if (!(context.User is IGuildUser user))
                    return Task.FromResult(PreconditionResult.FromError(ErrorMessage));

            return Task.FromResult(user.RoleIds.Any(rId => rId == RoleId)
                                       ? PreconditionResult.FromSuccess()
                                       : PreconditionResult.FromError(ErrorMessage));
        }
    }
}
