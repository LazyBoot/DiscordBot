﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace LazyDiscordBot.Preconditions
{
    public class RequireGuild : PreconditionAttribute
    {
        public ulong GuildId { get; }
        public override string ErrorMessage { get; set; } = string.Empty;

        public RequireGuild(ulong guildId)
        {
            GuildId = guildId;
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult(context.Guild is not null && GuildId == context.Guild.Id
                                       ? PreconditionResult.FromSuccess()
                                       : PreconditionResult.FromError(ErrorMessage));
        }
    }
}
