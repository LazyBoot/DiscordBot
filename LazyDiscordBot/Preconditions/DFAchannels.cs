using System;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Models;

namespace LazyDiscordBot.Preconditions
{
    public class DFAchannels : PreconditionAttribute
    {
        private readonly ulong[] _DFAchannels = [DFA.Channels.TheRedZone, DFA.Channels.MissionDevPeeps, DFA.Channels.TagsOnly, DFA.Channels.LazybotLookups];

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (context.Guild.Id != Guilds.DFA)
                return Task.FromResult(PreconditionResult.FromSuccess());

            var channelId = context.Channel is SocketThreadChannel channel ? channel.ParentChannel.Id : context.Channel.Id;
            return Task.FromResult(_DFAchannels.Contains(channelId)
                                ? PreconditionResult.FromSuccess()
                                : PreconditionResult.FromError(string.Empty));

        }
    }
}
