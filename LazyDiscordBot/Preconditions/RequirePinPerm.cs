using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LazyDiscordBot.Preconditions
{
    public class RequirePinPerm : PreconditionAttribute
    {
        public override string ErrorMessage { get; set; } = "You do not have access to that command.";

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (!(context.Channel is ITextChannel))
                return Task.FromResult(PreconditionResult.FromError("Not available in DMs"));
                
            var filter = services.GetRequiredService<PinService>();
            return Task.FromResult(filter.UserCanPinInChannel(context.Guild.Id, context.Channel.Id, (context.User as IGuildUser).RoleIds)
                                       ? PreconditionResult.FromSuccess()
                                       : PreconditionResult.FromError(ErrorMessage));
        }
    }
}
