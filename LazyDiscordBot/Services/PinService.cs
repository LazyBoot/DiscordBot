using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessageReactions)]
    public class PinService : IBotService
    {
        private readonly ILogger<LinkFixerService> _logger;
        private readonly DiscordSocketClient _client;
        private readonly ElevationService _elevationService;
        private readonly ILiteCollection<PinPermissions> _permissionsDb;
        public ConcurrentDictionary<string, PinPermissions> Permissions { get; private set; }

        internal Emoji PinEmote = new Emoji("📌");

        public PinService(ILogger<LinkFixerService> logger, LiteDatabase db, DiscordSocketClient client, ElevationService elevationService)
        {
            _logger = logger;
            _client = client;
            _elevationService = elevationService;
            _permissionsDb = db.GetCollection<PinPermissions>("PinPermissions");
            Permissions = new ConcurrentDictionary<string, PinPermissions>(_permissionsDb.FindAll().ToDictionary(p => p.GuildId.ToString() + p.ChannelId));

            _client.ReactionAdded += ReactionAddedHander;
            _client.ReactionRemoved += ReactionRemovedHander;
        }

        private async Task ReactionAddedHander(Cacheable<IUserMessage, ulong> msg, Cacheable<IMessageChannel, ulong> messageChannel, SocketReaction reaction)
        {
            var chan = await messageChannel.GetOrDownloadAsync();

            if (!(chan is ITextChannel channel) || reaction.Emote.Name != PinEmote.Name)
                return;

            if (!(reaction.User.IsSpecified && reaction.User.Value is IGuildUser user))
                user = await _client.Rest.GetGuildUserAsync(channel.GuildId, reaction.UserId);

            if (!UserCanPinInChannel(channel.GuildId, channel.Id, user.RoleIds) && !_elevationService.IsAdmin(user))
                return;

            var message = await msg.GetOrDownloadAsync();

            await message.PinAsync();
        }

        private async Task ReactionRemovedHander(Cacheable<IUserMessage, ulong> msg, Cacheable<IMessageChannel, ulong> messageChannel, SocketReaction reaction)
        {
            var chan = await messageChannel.GetOrDownloadAsync();

            if (!(chan is ITextChannel channel) || reaction.Emote.Name != PinEmote.Name)
                return;

            if (!(reaction.User.IsSpecified && reaction.User.Value is IGuildUser user))
                user = await _client.Rest.GetGuildUserAsync(channel.GuildId, reaction.UserId);

            if (!UserCanPinInChannel(channel.GuildId, channel.Id, user.RoleIds) && !_elevationService.IsAdmin(user))
                return;

            var message = await msg.GetOrDownloadAsync();

            await message.UnpinAsync();
        }

        public bool UserCanPinInChannel(ulong guildId, ulong channelId, IEnumerable<ulong> roleIds)
        {
            var guildChannel = guildId.ToString() + channelId;
            bool result = Permissions.ContainsKey(guildChannel) && Permissions[guildChannel].Roles.Any(u => roleIds.Any(r => r == u));
            return result;
        }

        public bool AddRoleToChannel(ulong guildId, ulong channelId, ulong roleId)
        {
            var guildChannel = Permissions.GetOrAdd(guildId.ToString() + channelId, (key) => new PinPermissions
            {
                GuildId = guildId,
                ChannelId = channelId,
                Roles = new List<ulong>()
            });
            if (guildChannel.Roles.Any(r => r == roleId))
                return false;

            guildChannel.Roles.Add(roleId);
            _permissionsDb.Upsert(guildChannel);
            return true;
        }

        public bool RemoveRoleFromChannel(ulong guildId, ulong channelId, ulong userId)
        {
            if (!Permissions.ContainsKey(guildId.ToString() + channelId))
                return false;

            var guildChannel = Permissions.GetOrAdd(guildId.ToString() + channelId, (key) => new PinPermissions
            {
                GuildId = guildId,
                ChannelId = channelId,
                Roles = new List<ulong>()
            });
            guildChannel.Roles.Remove(userId);
            _permissionsDb.Upsert(guildChannel);
            return true;
        }
    }

    public class PinPermissions
    {
        [BsonId] public string BsonId => GuildId.ToString() + ChannelId;
        public ulong ChannelId { get; set; }
        public List<ulong> Roles { get; set; }
        public ulong GuildId { get; set; }

        public bool Equals(PinPermissions other)
        {
            return GuildId == other?.GuildId && ChannelId == other.ChannelId;
        }
    }

    public class PinReactPerms
    {
        public ulong Guild { get; private set; }
        public ulong Channel { get; private set; }
        public ulong User { get; private set; }

        internal CancellationTokenSource CancellationToken = new CancellationTokenSource();

        public PinReactPerms()
        {
        }

        public PinReactPerms(ulong guild, ulong channel, ulong user)
        {
            Guild = guild;
            Channel = channel;
            User = user;
        }
    }
}
