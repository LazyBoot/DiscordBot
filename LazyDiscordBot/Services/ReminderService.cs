using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMembers)]
    public class ReminderService : IBotService, IRunOnStartup, IStopOnShutdown
    {
        private readonly DiscordSocketClient _client;
        private readonly ILiteCollection<Reminder> _reminderCollection;
        public ConcurrentDictionary<Guid, Reminder> Reminders { get; private set; }
        private readonly ILogger _logger;
        private readonly TimerService _timerService;

        public ReminderService(DiscordSocketClient client, ILogger<ReminderService> logger, LiteDatabase db, TimerService timerService)
        {
            _logger = logger;
            _timerService = timerService;
            _client = client;
            _reminderCollection = db.GetCollection<Reminder>("Reminder");

            Reminders = new ConcurrentDictionary<Guid, Reminder>(_reminderCollection.FindAll().ToDictionary(r => r.Id));
        }

        public Task Initialize()
        {
            _logger.LogInformation("Starting Reminder Service");

            foreach (var (id, reminder) in Reminders)
            {
                StartTimer(reminder);
            }

            return Task.CompletedTask;
        }

        public Task Stop()
        {
            return Task.CompletedTask;
        }

        public bool StartTimer(Reminder reminder)
        {
            return _timerService.StartTimer(reminder, async () => await ProcessReminder(reminder.Id, reminder));
        }

        public async Task ProcessReminder(Guid id, Reminder reminder)
        {
            IUser user = _client.GetUser(reminder.User);
            user ??= await _client.Rest.GetUserAsync(reminder.User);

            var shouldRemove = false;

            try
            {

                var replyEmbed = new EmbedBuilder()
                    .WithColor(Color.Green)
                    .WithTitle("Reminder!")
                    .AddField("You asked me to remind you of this:", reminder.Message);

                if (!string.IsNullOrEmpty(reminder.MessageLink))
                    replyEmbed = replyEmbed.AddField("\u200b", $"[Original Message]({reminder.MessageLink})");

                await user.SendMessageAsync(embed: replyEmbed.Build());
                shouldRemove = true;
            }
            catch (HttpException ex)
            {
                if (ex.DiscordCode == DiscordErrorCode.CannotSendMessageToUser)
                {
                    shouldRemove = true;
                    _logger.LogWarning("Unable to send reminder to user: {User}", user);
                }
            }

            if (shouldRemove && Reminders.TryRemove(id, out _))
                _reminderCollection.Delete(reminder.Id);
        }

        public RuntimeResult AddReminder(SocketUser user, DateTime time, string message, string messageLink)
        {
            if (Reminders.Count(r => r.Value.User == user.Id) > 15)
                return CommandResult.FromError("You have too many reminders already");

            var reminder = new Reminder(user.Id, time, message, messageLink);
            Reminders.TryAdd(reminder.Id, reminder);
            _reminderCollection.Insert(reminder);
            StartTimer(reminder);
            return CommandResult.FromSuccess();
        }

        public int RemoveRemindersForUser(SocketUser user)
        {
            var removedEntries = 0;
            foreach (var (id, reminder) in Reminders.Where(r => r.Value.User == user.Id))
            {
                _timerService.StopTimer(id);
                var removed = Reminders.TryRemove(id, out _);
                var deleted = _reminderCollection.Delete(id);

                if (removed || deleted)
                    removedEntries++;
            }

            return removedEntries;
        }

        public List<Reminder> ListRemindersForUser(IUser user)
        {
            return Reminders.Values.Where(r => r.User == user.Id).ToList();
        }
    }
}
