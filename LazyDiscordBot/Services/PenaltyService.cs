﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using LazyDiscordBot.Models;
using Microsoft.Extensions.Logging;
using ModelLibrary.DataTransferModels;
using ModelLibrary.PlayerDataModel;

namespace LazyDiscordBot.Services
{
    public class PenaltyService : IBotService
    {
        private readonly IHttpClientFactory _client;
        private readonly ILogger<PenaltyService> _logger;
        private readonly SemaphoreSlim _mutex = new SemaphoreSlim(1, 1);

        public PenaltyService(IHttpClientFactory client, ILogger<PenaltyService> logger)
        {
            _client = client;
            _logger = logger;
        }

        internal async Task<(List<Player> penData, PenDataResult result)> GetPenData(string token, Uri url)
        {
            var (data, result) = await GetFromPenApi(token, url);

            if (result != PenDataResult.Found)
                return (null, result);

            return (JsonSerializer.Deserialize<List<Player>>(data), result);
        }

        internal async Task<(string data, PenDataResult result)> GetFromPenApi(string token, Uri url)
        {
            // if (token != null)
            //     url = new Uri(url + "&token=" + token);

            await _mutex.WaitAsync();

            HttpResponseMessage response;
            try
            {
                HttpClient httpClient = _client.CreateClient();
                httpClient.Timeout = TimeSpan.FromSeconds(60);

                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                response = await httpClient.GetAsync(url);
            }
            catch (TaskCanceledException ex)
            {
                _logger.LogWarning(ex, "Timeout getting penalty data");
                return (null, PenDataResult.Error);
            }
            finally
            {
                _mutex.Release();
            }

            if (response.StatusCode == HttpStatusCode.NoContent || response.StatusCode == HttpStatusCode.NotFound)
            {
                return (null, PenDataResult.NotFound);
            }

            if (!response.IsSuccessStatusCode)
                return (null, PenDataResult.Error);

            return (await response.Content.ReadAsStringAsync(), PenDataResult.Found);
        }

        internal async Task<(List<Player> penData, PenDataResult result)> GetPenDataByName(
            ServerInfo server, string userName, bool exactMatch = false)
        {
            if (!await CheckAndUpdateServerToken(server))
                return (null, PenDataResult.Error);

            var url = new Uri(server.StatsUrl, "api/Player/byName?name=" + Uri.EscapeDataString(userName));

            return await GetPenData(server.BearerToken, url);
        }

        internal async Task<(List<Player> penData, PenDataResult result)> GetPenDataById(ServerInfo server, string ucid)
        {
            if (!await CheckAndUpdateServerToken(server))
                return (null, PenDataResult.Error);

            var url = new Uri(server.StatsUrl, "api/Player/byUcid?ucid=" + Uri.EscapeDataString(ucid));

            return await GetPenData(server.BearerToken, url);
        }

        internal async Task<(List<Player> penData, PenDataResult result)> GetBanDataById(ServerInfo server, string ucid)
        {
            if (!await CheckAndUpdateServerToken(server))
                return (null, PenDataResult.Error);

            var url = new Uri(server.StatsUrl, "api/Player/byUcid?ucid=" + Uri.EscapeDataString(ucid));
            var (penData, result) = await GetPenData(server.BearerToken, url);
            if (result == PenDataResult.Found && penData.FirstOrDefault().ucidBanned == false && penData.FirstOrDefault().ipBanned == false)
                return (null, PenDataResult.NotFound);

            return (penData, result);
        }

        internal async Task<(List<FailedSlot> wlData, PenDataResult result)> LookupWlData(ServerInfo server, string name)
        {
            if (!await CheckAndUpdateServerToken(server))
                return (null, PenDataResult.Error);

            var url = new Uri(server.StatsUrl, "Whitelist/pendingUsers");

            var (data, result) = await GetFromPenApi(server.BearerToken, url);
            if (result != PenDataResult.Found)
                return (null, result);

            return (JsonSerializer.Deserialize<List<FailedSlot>>(data), result);
        }

        internal async Task<PenDataResult> SendWlData(ServerInfo server, WhitelistEntry data)
        {
            if (!await CheckAndUpdateServerToken(server))
                return PenDataResult.Error;

            var url = new Uri(server.StatsUrl, "Whitelist/whitelistUser");

            await _mutex.WaitAsync();

            HttpResponseMessage response;
            try
            {
                HttpClient httpClient = _client.CreateClient();
                httpClient.Timeout = TimeSpan.FromSeconds(60);

                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", server.BearerToken);
                response = await httpClient.PutAsJsonAsync(url, data);
            }
            catch (TaskCanceledException ex)
            {
                _logger.LogWarning(ex, "Timeout getting penalty data");
                return PenDataResult.Error;
            }
            finally
            {
                _mutex.Release();
            }

            if (response.StatusCode == HttpStatusCode.NoContent || response.StatusCode == HttpStatusCode.NotFound)
            {
                return PenDataResult.NotFound;
            }

            if (!response.IsSuccessStatusCode)
                return PenDataResult.Error;

            return PenDataResult.Found;
        }

        internal async Task<bool> CheckAndUpdateServerToken(ServerInfo server)
        {
            if (DateTime.UtcNow <= server.ValidTo && !string.IsNullOrWhiteSpace(server.BearerToken))
                return true;

            HttpResponseMessage response;
            try
            {
                var url = new Uri(server.StatsUrl, "api/Login");
                var httpClient = _client.CreateClient();
                var loginData = new UserLogin { Username = server.Username, Password = server.Password };
                response = await httpClient.PostAsJsonAsync(url, loginData);
            }
            catch (TaskCanceledException ex)
            {
                _logger.LogWarning(ex, "Timeout logging into API");
                throw;
            }

            if (!response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.NoContent)
                return false;

            var data = await response.Content.ReadAsStringAsync();
            var json = JsonSerializer.Deserialize<ReturnToken>(data);

            server.ValidTo = json.validTo;
            server.BearerToken = json.token;

            return true;
        }
    }
}
