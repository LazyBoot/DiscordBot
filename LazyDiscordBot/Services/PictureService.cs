using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using Discord.Commands;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    public class PictureService : IBotService
    {
        private readonly IHttpClientFactory _http;
        private readonly ILogger<PictureService> _logger;
        private DateTime _lastCat = DateTime.MinValue;
        private DateTime _lastDog = DateTime.MinValue;

        private int _useCountCat = 0;
        private int _useCountDog = 0;
        private readonly Timer _timer;
        private readonly PictureSettings _pictureSettings;
        private readonly ILiteCollection<PictureSettings> _liteCollection;

        public PictureService(IHttpClientFactory http, LiteDatabase db, ILogger<PictureService> logger)
        {
            _http = http;
            _logger = logger;
            _liteCollection = db.GetCollection<PictureSettings>("PictureSettings");
            _pictureSettings = _liteCollection.FindById(0);

            if (_pictureSettings == null)
            {
                _pictureSettings = new PictureSettings
                {
                    Id = 0,
                    BaseCooldown = 2,
                    MaxCoolDown = 3600,
                    TimerInterval = 10,
                    ApiKeys = new Dictionary<PictureType, string>()
                };
                _liteCollection.Upsert(0, _pictureSettings);
            }

            _timer = new Timer() { Interval = TimeSpan.FromMinutes(_pictureSettings.TimerInterval).TotalMilliseconds, AutoReset = true };
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_useCountCat > 0)
                _useCountCat--;

            if (_useCountDog > 0)
                _useCountDog--;
        }

        public bool SetPictureSettings(PictureSettings settings)
        {
            var setSomething = false;

            if (settings.BaseCooldown != 0)
            {
                _pictureSettings.BaseCooldown = settings.BaseCooldown;
                setSomething = true;
            }

            if (settings.MaxCoolDown != 0)
            {
                _pictureSettings.MaxCoolDown = settings.MaxCoolDown;
                setSomething = true;
            }

            if (settings.TimerInterval != 0)
            {
                _pictureSettings.TimerInterval = settings.TimerInterval;
                _timer.Interval = TimeSpan.FromMinutes(_pictureSettings.TimerInterval).TotalMilliseconds;
                setSomething = true;
            }

            return setSomething && _liteCollection.Update(0, _pictureSettings);
        }

        public bool SetApiKey(PictureType pictureType, string apiKey)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
                return false;

            if (_pictureSettings.ApiKeys == null)
                _pictureSettings.ApiKeys = new Dictionary<PictureType, string>();

            _pictureSettings.ApiKeys[pictureType] = apiKey;
            return _liteCollection.Update(0, _pictureSettings);
        }

        public DateTime CheckCooldown(PictureType pictureType)
        {
            return pictureType switch
            {
                PictureType.Cat =>
                    _lastCat + TimeSpan.FromSeconds(Math.Min(Math.Pow(_pictureSettings.BaseCooldown, _useCountCat), _pictureSettings.MaxCoolDown)),
                PictureType.Dog =>
                    _lastDog + TimeSpan.FromSeconds(Math.Min(Math.Pow(_pictureSettings.BaseCooldown, _useCountDog), _pictureSettings.MaxCoolDown)),
                _ => DateTime.MinValue,
            };
        }

        private string GetApiKey(PictureType pictureType)
        {
            if (!_pictureSettings.ApiKeys.ContainsKey(pictureType))
                throw new KeyNotFoundException($"No api key set for {nameof(pictureType)} pictures");

            return _pictureSettings.ApiKeys[pictureType];
        }

        public async Task<HttpResponseMessage> GetCatPictureAsync()
        {
            _lastCat = DateTime.UtcNow;
            _useCountCat++;

            var apiKey = GetApiKey(PictureType.Cat);

            var httpClient = _http.CreateClient();
            httpClient.Timeout = TimeSpan.FromSeconds(20);
            try
            {
                using var message = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("https://api.thecatapi.com/v1/images/search"),
                    Headers = { { "x-api-key", apiKey } }
                };

                using var jsonResp = await httpClient.SendAsync(message);
                jsonResp.EnsureSuccessStatusCode();

                var images = System.Text.Json.JsonSerializer.Deserialize<CatResponse[]>(await jsonResp.Content.ReadAsStringAsync());

                using var imageMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(images.FirstOrDefault().url),
                    Headers = { { "x-api-key", apiKey } }
                };

                var resp = await httpClient.SendAsync(imageMessage);
                if (resp?.StatusCode == HttpStatusCode.Found || resp?.StatusCode == HttpStatusCode.OK)
                    return resp;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }

            return null;
        }

        internal async Task<HttpResponseMessage> GetDogPictureAsync()
        {
            _lastDog = DateTime.UtcNow;
            _useCountDog++;

            var httpClient = _http.CreateClient();
            httpClient.Timeout = TimeSpan.FromSeconds(20);
            try
            {
                using var jsonResp = await httpClient.GetAsync("https://dog.ceo/api/breeds/image/random");
                jsonResp.EnsureSuccessStatusCode();

                var image = System.Text.Json.JsonSerializer.Deserialize<DogResponse>(await jsonResp.Content.ReadAsStringAsync());

                var resp = await httpClient.GetAsync(image.message);

                if (resp?.StatusCode == HttpStatusCode.Found || resp?.StatusCode == HttpStatusCode.OK)
                    return resp;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }

            return null;
        }


        public async Task<RuntimeResult> GetXkcdAsync()
        {
            var client = _http.CreateClient("noRedirect");

            using var response = await client.GetAsync(@"https://c.xkcd.com/random/comic/", HttpCompletionOption.ResponseHeadersRead);

            return response.StatusCode == HttpStatusCode.Found
                ? CommandResult.FromSuccess("Random xkcd: " + response.Headers.Location.ToString())
                : CommandResult.FromError("Unable to get xkcd link");
        }

        public enum PictureType
        {
            Cat,
            Dog
        }

        private class DogResponse
        {
            public Uri message { get; set; }
            public string status { get; set; }
        }

        public class CatResponse
        {
            public object[] breeds { get; set; }
            public string id { get; set; }
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

    }
}
