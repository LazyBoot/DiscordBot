﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using Discord.Commands;
using LazyDiscordBot.Models;

namespace LazyDiscordBot.Services
{
    //public class SpeechService
    //{
    //    // https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/quickstart-dotnet-text-to-speech
    //    private string _accessToken;

    //    private string _host = @"https://westeurope.tts.speech.microsoft.com/cognitiveservices/v1";
    //    private readonly ILogger _logger;

    //public SpeechService(ILogger<SpeechService> logger)
    //{
    //    _logger = logger;

    //    AzureAuthentication auth = new AzureAuthentication("https://westeurope.api.cognitive.microsoft.com/sts/v1.0/issueToken", DiscordInteractions.Tokens.WebHookToken);
    //    try
    //    {
    //        _accessToken = auth.FetchTokenAsync().ConfigureAwait(false).GetAwaiter().GetResult();
    //        Program.LogInfo("Successfully obtained an access token. \n");
    //    }
    //    catch (Exception ex)
    //    {
    //        _logger.Error(ex, "Failed to obtain an access token.");
    //    }
    //}


    //    public async Task Say(AudioService audioService, ICommandContext context, string text)
    //    {
    //        string body = @"<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>
    //                  <voice name='Microsoft Server Speech Text to Speech Voice (en-US, JessaNeural)'>" +
    //                      text + "</voice></speak>";

    //        using (var client = new HttpClient())
    //        {
    //            using (var request = new HttpRequestMessage())
    //            {
    //                // Set the HTTP method
    //                request.Method = HttpMethod.Post;
    //                // Construct the URI
    //                request.RequestUri = new Uri(_host);
    //                // Set the content type header
    //                request.Content = new StringContent(body, Encoding.UTF8, "application/ssml+xml");
    //                // Set additional header, such as Authorization and User-Agent
    //                request.Headers.Add("Authorization", "Bearer " + _accessToken);
    //                request.Headers.Add("Connection", "Keep-Alive");
    //                // Update your resource name
    //                request.Headers.Add("User-Agent", "LazyBot");
    //                request.Headers.Add("X-Microsoft-OutputFormat", "riff-24khz-16bit-mono-pcm");
    //                // Create a request
    //                _logger.Debug("Calling the TTS service. Please wait...");

    //                try
    //                {
    //                    using (var response = await client.SendAsync(request).ConfigureAwait(false))
    //                    {
    //                        response.EnsureSuccessStatusCode();
    //                        Program.LogInfo(response.StatusCode);
    //                        // Asynchronously read the response
    //                        using (var dataStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
    //                        {
    //                            //IAudioClient audioClient = audioService.GetClient(context.Guild);
    //                            //using (AudioOutStream outStream = audioClient.CreatePCMStream(AudioApplication.Music, 240))
    //                            //{
    //                            // try
    //                            // {
    //                            //  await audioClient.SetSpeakingAsync(true);
    //                            //  await dataStream.CopyToAsync(outStream);
    //                            // }
    //                            // finally
    //                            // {
    //                            //  await outStream.FlushAsync();
    //                            //  await audioClient.SetSpeakingAsync(false);
    //                            // }
    //                            //}

    //                            _logger.Debug("Your speech file is being written to file...");
    //                            using (var fileStream = new FileStream(@"sample.wav", FileMode.Create, FileAccess.Write, FileShare.Write))
    //                            {
    //                                await dataStream.CopyToAsync(fileStream).ConfigureAwait(false);
    //                                fileStream.Close();
    //                            }
    //                            _logger.Debug("Your file is ready.");

    //                            await audioService.SendAudioFileAsync(context, @"sample.wav");
    //                        }
    //                    }
    //                }
    //                catch (Exception e)
    //                {
    //                    _logger.Error(e);
    //                    throw;
    //                }
    //            }
    //        }
    //    }
    //}
}
