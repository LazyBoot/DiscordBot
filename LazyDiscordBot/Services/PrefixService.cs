﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace LazyDiscordBot.Services
{

    /// <summary>
    /// A prefix service designed for discord.NET
    /// </summary>
    public class PrefixService : IBotService
    {
        private PrefixObj PrefixObject { get; } = new PrefixObj();

        public class PrefixObj
        {
            /// <summary>
            /// Gets or sets the default prefix.
            /// </summary>
            public string DefaultPrefix { get; set; }

            /// <summary>
            /// Gets or sets the prefix dictionary.
            /// </summary>
            public Dictionary<ulong, string> PrefixDictionary { get; set; } = new Dictionary<ulong, string>();

            [BsonId]
            // ReSharper disable once InconsistentNaming
            public int _discarded { get; set; } = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrefixService"/> class. 
        /// </summary>
        /// <param name="defaultPrefix">
        /// The default prefix.
        /// </param>
        /// <param name="dbLocation">
        /// The db Location.
        /// </param>
        /// <exception cref="NullReferenceException">
        /// Provided prefix cannot be null or whitespace
        /// </exception>
        public PrefixService(string defaultPrefix, string dbLocation = "PrefixService.db")
        {
            if (string.IsNullOrWhiteSpace(dbLocation))
            {
                throw new NullReferenceException("Please provide a non, null database location or use the default provided");
            }

            var db = new LiteDatabase(Path.Combine(AppContext.BaseDirectory, dbLocation));

            InitDb(defaultPrefix, db);
        }

        public PrefixService(string defaultPrefix, LiteDatabase db)
        {
            InitDb(defaultPrefix, db);
        }

        public PrefixService(IOptions<BotConfig> config, LiteDatabase db) : this(config.Value.DefaultPrefix, db)
        {
            
        }

        private void InitDb(string defaultPrefix, LiteDatabase db)
        {
            if (string.IsNullOrWhiteSpace(defaultPrefix))
            {
                throw new NullReferenceException("Provided default prefix cannot be null or whitespace");
            }

            if (db == null)
            {
                throw new NullReferenceException("Please provide a database");
            }

            PrefixObject.DefaultPrefix = defaultPrefix;

            LiteDatabase = db.GetCollection<PrefixObj>("PrefixObject");
        }

        /// <summary>
        /// Gets the LiteDB document store
        /// </summary>
        [JsonIgnore]
        private ILiteCollection<PrefixObj> LiteDatabase { get; set; }

        /// <summary>
        /// Indicates whether the specified ID has a custom prefix
        /// </summary>
        /// <param name="guildId">
        /// The guild id.
        /// </param>
        /// <returns>
        /// <see cref="bool"/>.
        /// </returns>
        public bool HasCustomPrefix(ulong guildId)
        {
            PrefixObject.PrefixDictionary.TryGetValue(guildId, out var customPrefix);
            return !string.IsNullOrWhiteSpace(customPrefix);
        }

        /// <summary>
        /// Gets the default prefix.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetDefaultPrefix()
        {
            return PrefixObject.DefaultPrefix;
        }

        /// <summary>
        /// Gets the prefix for the specified ID or returns the default prefix
        /// </summary>
        /// <param name="guildId">
        /// The guild id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPrefix(ulong guildId)
        {
            PrefixObject.PrefixDictionary.TryGetValue(guildId, out var customPrefix);
            return string.IsNullOrWhiteSpace(customPrefix) ? PrefixObject.DefaultPrefix : customPrefix;
        }

        /// <summary>
        /// Prefix Set results for <see cref="SetPrefix"/>
        /// </summary>
        public enum PrefixSetResult
        {
            Success,
            Failed,
            Reset
        }

        /// <summary>
        /// Sets the prefix for a given ID
        /// NOTE: If the prefix is null or whitespace the guild's prefix will be reset.
        /// </summary>
        /// <param name="guildId">
        /// The guild id.
        /// </param>
        /// <param name="prefix">
        /// The prefix
        /// </param>
        /// <returns>
        /// The <see cref="PrefixSetResult"/>.
        /// </returns>
        public PrefixSetResult SetPrefix(ulong guildId, string prefix = null)
        {
            try
            {
                if (string.IsNullOrEmpty(prefix) || prefix == PrefixObject.DefaultPrefix)
                {
                    if (PrefixObject.PrefixDictionary.ContainsKey(guildId))
                    {
                        PrefixObject.PrefixDictionary.Remove(guildId);
                    }

                    Save();
                    return PrefixSetResult.Reset;
                }

                if (PrefixObject.PrefixDictionary.ContainsKey(guildId))
                {
                    PrefixObject.PrefixDictionary[guildId] = prefix;
                }
                else
                {
                    PrefixObject.PrefixDictionary.Add(guildId, prefix);
                }

                Save();
                return PrefixSetResult.Success;
            }
            catch
            {
                return PrefixSetResult.Failed;
            }
        }

        /// <summary>
        /// Sets the default prefix
        /// </summary>
        /// <param name="defaultPrefix">
        /// The default prefix
        /// </param>
        public void SetDefaultPrefix(string defaultPrefix)
        {
            if (string.IsNullOrWhiteSpace(defaultPrefix))
            {
                throw new NullReferenceException("Default Prefix cannot be null or whitespace");
            }

            PrefixObject.DefaultPrefix = defaultPrefix;
            Save();
        }

        /// <summary>
        /// Gets the current prefix dictionary
        /// </summary>
        /// <returns>
        /// The Prefix Dictionary
        /// </returns>
        public Dictionary<ulong, string> GetDictionary()
        {
            return PrefixObject.PrefixDictionary;
        }

        /// <summary>
        /// Initializes the prefix dictionary
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task InitializeAsync()
        {
            var db = LiteDatabase.FindAll().FirstOrDefault() ?? new PrefixObj
            {
                DefaultPrefix = PrefixObject.DefaultPrefix
            };
            PrefixObject.PrefixDictionary = db.PrefixDictionary;
            PrefixObject.DefaultPrefix = db.DefaultPrefix;

            return Task.CompletedTask;
        }

        /// <summary>
        /// Saves the current dictionary
        /// </summary>
        public void Save()
        {
            if (!LiteDatabase.Update(PrefixObject))
            {
                LiteDatabase.Insert(PrefixObject);
            }

            Update();
        }

        /// <summary>
        /// Updates the prefix dictionary to the most recently stored version
        /// </summary>
        private void Update()
        {
            var db = LiteDatabase.FindAll().FirstOrDefault();
            if (db == null)
            {
                throw new NullReferenceException("The PrefixService update action was unable to find a valid PrefixObject document");
            }

            PrefixObject.PrefixDictionary = db.PrefixDictionary;
            PrefixObject.DefaultPrefix = db.DefaultPrefix;
        }
    }
}
