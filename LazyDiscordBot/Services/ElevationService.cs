﻿using System.Linq;
using Discord;
using Discord.WebSocket;
using LazyDiscordBot.Models;
using Microsoft.Extensions.Options;

namespace LazyDiscordBot.Services
{
    public class ElevationService : IBotService
    {
        private readonly DiscordSocketClient _client;
        private readonly BotConfig _config;

        public ElevationService(DiscordSocketClient client, IOptions<BotConfig> config)
        {
            _client = client;
            _config = config.Value;
        }

        public bool IsAdmin(IUser user) 
            => IsAdmin(user.Id);

        public bool IsInTeam(IUser user) 
            => IsInTeam(user.Id);

        public bool IsOwner(IUser user) 
            => IsOwner(user.Id);

        public bool IsAdmin(ulong userId)
        {
            return IsInTeam(userId) || (_config.Admins != null && _config.Admins.Any(u => u.Id == userId));
        }

        public bool IsInTeam(ulong userId)
        {
            var application = _client.GetApplicationInfoAsync().GetAwaiter().GetResult();
            return IsOwner(userId, application) || (application.Team != null && application.Team.TeamMembers.Any(m => m.MembershipState == MembershipState.Accepted && m.User.Id == userId));
        }

        public bool IsOwner(ulong userId, IApplication application = null)
        {
            var ownerId = application?.Owner.Id ?? _client.GetApplicationInfoAsync().GetAwaiter().GetResult().Owner.Id;
            return ownerId == userId;
        }
    }
}
