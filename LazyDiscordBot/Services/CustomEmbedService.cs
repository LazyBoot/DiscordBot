﻿using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LazyDiscordBot.Attributes;
using LiteDB;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;

namespace LazyDiscordBot.Services
{
    public class CustomEmbedService : IBotService
    {
        private readonly CommandService _commandService;
        private ModuleInfo _customCommandsModule;
        private readonly ILiteCollection<CustomEmbedCommand> _commandCollection;
        public ConcurrentDictionary<string, CustomEmbedCommand> Commands { get; private set; }

        public class CustomEmbedCommand : IEquatable<CustomEmbedCommand>
        {
            [BsonId] public string BsonId => GuildId + Name;
            public string Name { get; set; }
            public EmbedBuilder Embed { get; set; }
            public ulong GuildId { get; set; }

            public bool Equals(CustomEmbedCommand other)
            {
                return GuildId == other?.GuildId && Name == other.Name;
            }
        }


        public CustomEmbedService(CommandService commandService, LiteDatabase db)
        {
            _commandService = commandService;
            _commandCollection = db.GetCollection<CustomEmbedCommand>("CustomEmbedCommands");
            Commands = new ConcurrentDictionary<string, CustomEmbedCommand>(_commandCollection.FindAll().ToDictionary(c => c.GuildId + c.Name));

            CreateModule();
        }

        private async void CreateModule()
        {
            if (Commands.Count == 0)
                return;

            _customCommandsModule = await _commandService.CreateModuleAsync(string.Empty, moduleBuilder =>
            {
                foreach (var (_, command) in Commands)
                {
                    moduleBuilder.AddCommand(command.Name,
                        (context, objects, arg3, arg4) => context.Channel.SendMessageAsync(embed: command.Embed.Build()),
                        builder => builder.WithName(command.Name).AddAttributes(new CustomCommandAttribute())
                                          .AddPrecondition(new RequireGuild(command.GuildId)));
                }
            });
        }


        public async Task<RuntimeResult> AddCommandAsync(ulong guildId, string name, string title, string message, Attachment attachment = null)
        {
            var searchResult = _commandService.Search(name);
            if (searchResult.IsSuccess &&
                searchResult.Commands.All(c => c.Command.Attributes.All(a => a.GetType() != typeof(CustomCommandAttribute))))
                return CommandResult.FromError("Command already exists!");
            else if (searchResult.IsSuccess)
                return CommandResult.FromError("Custom command already exists!");

            var embed = new EmbedBuilder()
                .WithTitle(title)
                .WithDescription(message);

            if (attachment != null && attachment.Width > 0)
                embed.WithImageUrl(attachment.Url);

            var customCommand = new CustomEmbedCommand() {GuildId = guildId, Name = name, Embed = embed};
            if (_commandCollection.FindOne(command => command.Equals(customCommand)) != null)
                return CommandResult.FromError("Custom command already exists!");

            if (!Commands.TryAdd(customCommand.GuildId + customCommand.Name, customCommand))
                return CommandResult.FromError("Custom command already exists!");

            _commandCollection.Insert(customCommand);

            await _commandService.RemoveModuleAsync(_customCommandsModule);
            CreateModule();
            return CommandResult.FromSuccess();
        }

        public async Task<RuntimeResult> UpdateCommandAsync(ulong guildId, string name, string title, string message, Attachment attachment = null)
        {
            var embed = new EmbedBuilder()
                .WithTitle(title)
                .WithDescription(message);

            if (attachment != null && attachment.Width > 0)
                embed.WithImageUrl(attachment.Url);

            var customCommand = new CustomEmbedCommand() {GuildId = guildId, Name = name, Embed = embed};
            var oldCommand = _commandCollection.FindOne(command => command.GuildId.Equals(guildId) && command.Equals(customCommand));

            if (oldCommand == null)
                return CommandResult.FromError("Custom command not found!");

            Commands.TryUpdate(guildId + name, customCommand, oldCommand);
            _commandCollection.Update(customCommand);
            Commands[name] = customCommand;

            await _commandService.RemoveModuleAsync(_customCommandsModule);
            CreateModule();
            return CommandResult.FromSuccess();
        }

        public async Task<RuntimeResult> RemoveCommandAsync(ulong guildId, string name)
        {
            var customCommand = _commandCollection.FindOne(command => command.GuildId.Equals(guildId) && command.Name.Equals(name));
            if (customCommand == null)
                return CommandResult.FromError("Custom command not found!");

            Commands.Remove(guildId + name, out _);
            _commandCollection.Delete(customCommand.GuildId + customCommand.Name);

            await _commandService.RemoveModuleAsync(_customCommandsModule);
            CreateModule();
            return CommandResult.FromSuccess();
        }

        public IEnumerable<CustomEmbedCommand> ListCommands(ulong guildId)
        {
            return Commands.Values.Where(c => c.GuildId == guildId);
        }
    }
}
