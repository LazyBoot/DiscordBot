﻿using System;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Models;
using LazyDiscordBot.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LazyDiscordBot;

[RequireIntents(GatewayIntents.DirectMessages)]
public class CheckRebootService : IBotService, IRunOnStartup, IStopOnShutdown
{
    private const string RebootReqFilePath = @"/var/run/reboot-required";
    private readonly bool _shouldRunCheck;
    private readonly ILogger<CheckRebootService> _logger;
    private readonly DiscordSocketClient _client;
    private FileSystemWatcher _watcher;

    private bool _isNotifying = false;

    public CheckRebootService(IOptions<BotConfig> config, ILogger<CheckRebootService> logger, DiscordSocketClient client)
    {
        _shouldRunCheck = config.Value.CheckReboot;
        _logger = logger;
        _client = client;
    }
    public Task Initialize()
    {
        if (!_shouldRunCheck) return Task.CompletedTask;

        _logger.LogInformation("Starting CheckReboot Service.");

        if (File.Exists(RebootReqFilePath))
            _ = NotifyOwner();

        _watcher = new FileSystemWatcher(Path.GetDirectoryName(RebootReqFilePath));
        _watcher.Filter = Path.GetFileName(RebootReqFilePath);
        _watcher.Created += Watcher_Created;
        _watcher.EnableRaisingEvents = true;

        return Task.CompletedTask;
    }

    private async void Watcher_Created(object sender, FileSystemEventArgs e)
    {
        await NotifyOwner();
    }

    public Task Stop()
    {
        if (_watcher is not null)
            _watcher.Dispose();

        return Task.CompletedTask;
    }

    public async Task NotifyOwner()
    {
        _logger.LogInformation("Sending notification to owner.");

        var appInfo = await _client.GetApplicationInfoAsync();
        await appInfo.Owner.SendMessageAsync("My server wants a reboot!");
    }
}
