﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Rest;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessages | GatewayIntents.GuildMessageReactions | GatewayIntents.DirectMessages | GatewayIntents.DirectMessageReactions | GatewayIntents.MessageContent)]
    public class QuoteService : IBotService
    {
        public readonly Regex Pattern = new Regex(
            @"(?<Prelink>\S+\s+\S*)?https?://(?:(?:ptb|canary)\.)?discord(?:app)?\.com/channels/(?<GuildId>\d+)/(?<ChannelId>\d+)/(?<MessageId>\d+)/?(?<Postlink>\S*\s+\S+)?",
            RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);

        private readonly IReadOnlyList<string> _validImageExtensions = new List<string> {"jpg", "png", "bmp", "gif"};

        private readonly DiscordSocketClient _discord;
        private readonly ILiteCollection<QuoteMessage> _quoteMessages;
        private readonly ILogger _logger;
        private readonly ElevationService _elevationService;


        public class QuoteMessage : IEquatable<QuoteMessage>
        {
            [BsonId] public ulong MessageId { get; set; }
            public ulong ChannelId { get; set; }
            public ulong? GuildId { get; set; }
            public ulong QuoteUser { get; set; }
            public ulong? TriggerMessageId { get; set; }
            public DateTime? QuoteTime { get; set; }

            public bool Equals(QuoteMessage other)
            {
                return MessageId == other?.MessageId;
            }
        }


        public QuoteService(ILogger<QuoteService> logger, LiteDatabase db, DiscordSocketClient discord, ElevationService elevationService)
        {
            _elevationService = elevationService;
            _logger = logger;
            _discord = discord;
            _quoteMessages = db.GetCollection<QuoteMessage>("QuoteMessages");

            _discord.ReactionAdded += Client_ReactionAdded;
            _discord.MessageReceived += Client_MessageReceived;
            _discord.MessageDeleted += Client_MessageDeleted;
        }

        private async Task Client_MessageDeleted(Cacheable<IMessage, ulong> rawMessage, Cacheable<IMessageChannel, ulong> messageChannel)
        {
            var quoteMessages = _quoteMessages.Find(m => m.TriggerMessageId != null && m.TriggerMessageId == rawMessage.Id);

            if (quoteMessages != null && quoteMessages.Any())
            {
                try
                {
                    var channel = await messageChannel.GetOrDownloadAsync();
                    foreach (var quoteMessage in quoteMessages)
                    {
                        await channel.DeleteMessageAsync(quoteMessage.MessageId);
                        _quoteMessages.DeleteMany(m => m.MessageId == quoteMessage.MessageId);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, "Exception on deleting message");
                }
            }

        }

        private async Task Client_MessageReceived(SocketMessage rawMessage)
        {
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            if(!Pattern.IsMatch(message.Content))
                return;

            var context = new SocketCommandContext(_discord, message);
            if (context.Guild is null || context.Guild.CurrentUser.GetPermissions(context.Channel as IGuildChannel).EmbedLinks)
            {
                await LinkQuote(context, message);
            }
        }

        private async Task Client_ReactionAdded(Cacheable<IUserMessage, ulong> rawMessage, Cacheable<IMessageChannel, ulong> messageChannel, SocketReaction reaction)
        {
            if (reaction.Emote.Name != "❌")
                return;

            var quoteMessage = _quoteMessages.FindOne(m => m.MessageId == rawMessage.Id);

            if (quoteMessage != null && (reaction.UserId == quoteMessage.QuoteUser || _elevationService.IsAdmin(reaction.UserId)))
            {
                try
                {
                    var channel = await messageChannel.GetOrDownloadAsync();
                    await channel.DeleteMessageAsync(rawMessage.Id);
                    _quoteMessages.DeleteMany(m => m.MessageId == rawMessage.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, "Exception on deleting message");
                }
            }
        }

        internal async Task LinkQuote(SocketCommandContext context, IMessage message)
        {
            await SendLinkQuote(context, message.Content).ConfigureAwait(false);
        }

        public async Task<RuntimeResult> SendLinkQuote(SocketCommandContext context, string messageLink)
        {
            foreach (Match match in Pattern.Matches(messageLink))
            {
                if (ulong.TryParse(match.Groups["GuildId"].Value, out var guildId)
                    && ulong.TryParse(match.Groups["ChannelId"].Value, out var channelId)
                    && ulong.TryParse(match.Groups["MessageId"].Value, out var messageId))
                {
                    try
                    {
                        return await SendLinkQuote(context, guildId, channelId, messageId);
                    }
                    catch (Exception ex)
                    {
                        const string errorText = "An error occurred while sending embed";
                        _logger.LogError(ex, errorText);
                        return CommandResult.FromError(errorText);
                    }
                }
            }

            return CommandResult.FromSuccess();
        }

        private async Task<RuntimeResult> SendLinkQuote(SocketCommandContext context, ulong guildId, ulong channelId, ulong messageId)
        {
            var guild = context.Client.GetGuild(guildId);
            if (guild == null)
                return CommandResult.FromError("Could not find guild... Is the bot not in that guild?");

            var channel = guild.GetTextChannel(channelId);
            if (channel == null)
                return CommandResult.FromError("Could not find channel");

            if (!guild.CurrentUser.GetPermissions(channel).ViewChannel)
                return CommandResult.FromError("Unable to get message. Bot does not have access to channel...");

            var message = await channel.GetMessageAsync(messageId);

            if (message == null)
                return CommandResult.FromError("Could not find message");

            return await SendQuote(context, message);
        }

        public async Task<RuntimeResult> SendQuote(SocketCommandContext context, IMessage message)
        {
            EmbedBuilder embedBuilder;

            embedBuilder = CreateQuoteEmbed(context, message);

            var reply = await context.Channel.SendMessageAsync(embed: embedBuilder.Build());

            RestUserMessage reply2 = null;
            if (message.Embeds.Any(em => em is Embed && !(em.Type == EmbedType.Image && em.Type == EmbedType.Gifv)))
            {
                reply2 = await context.Channel.SendMessageAsync(embed: message.Embeds.FirstOrDefault(em => em is Embed) as Embed);
            }

            _quoteMessages.DeleteMany(qm => qm.QuoteTime == null || qm.QuoteTime.Value > (DateTime.UtcNow + TimeSpan.FromDays(14)));

            _quoteMessages.Insert(new QuoteMessage
            {
                MessageId = reply.Id,
                QuoteTime = DateTime.UtcNow,
                ChannelId = context.Channel.Id,
                GuildId = context.Guild?.Id,
                QuoteUser = context.User.Id,
                TriggerMessageId = context.Message.Id
            });

            if (reply2 is not null)
                _quoteMessages.Insert(new QuoteMessage
                {
                    MessageId = reply2.Id,
                    QuoteTime = DateTime.UtcNow,
                    ChannelId = context.Channel.Id,
                    GuildId = context.Guild?.Id,
                    QuoteUser = context.User.Id,
                    TriggerMessageId = context.Message.Id
                });

            return CommandResult.FromSuccess();
        }

        public EmbedBuilder CreateQuoteEmbed(SocketCommandContext context, IMessage message)
        {
            string userDisplayName;
            if (message.Author is IGuildUser guildUser)
                userDisplayName = guildUser.DisplayName ?? guildUser.Username;
            else
                userDisplayName = message.Author.Discriminator == "0000" ? message.Author.Username : message.Author.ToString();

            var embedBuilder = new EmbedBuilder()
                               .WithAuthor(userDisplayName, message.Author.GetAvatarUrl() ?? message.Author.GetDefaultAvatarUrl())
                               .WithDescription(message.Content);


            if (message.Attachments.Any())
            {
                var image = message.Attachments.FirstOrDefault(a => a.Width > 0 && !a.IsSpoiler());
                if (image != null)
                {
                    embedBuilder.WithImageUrl(image.Url);
                }

                var attachments = new List<StringBuilder> { new StringBuilder() };
                var index = 0;
                foreach (var attachment in message.Attachments)
                {
                    var attachmentText = $"[{attachment.Filename}]({attachment.Url})";
                    if (attachments[index].Length + attachmentText.Length > 1000)
                    {
                        attachments.Add(new StringBuilder());
                        index++;
                    }
                    attachments[index].AppendLine(attachmentText);
                }

                embedBuilder.AddField("Attachments", attachments[0].ToString());

                if (attachments.Count > 1)
                {
                    for (int i = 1; i < attachments.Count; i++)
                    {
                        embedBuilder.AddField("Attachments (cont)", attachments[i].ToString());
                    }
                }
            }
            else if (message.Embeds.Any(em => em.Type == EmbedType.Image))
            {
                var embed = message.Embeds.FirstOrDefault(em => em.Type == EmbedType.Image);
                if (embed.Image.HasValue)
                    embedBuilder.WithImageUrl(embed.Image.Value.Url);
                else
                    embedBuilder.WithImageUrl(embed.Url);
            }
            else if (message.Embeds.Any(em => em.Type == EmbedType.Gifv))
            {
                var embed = message.Embeds.FirstOrDefault(em => em.Type == EmbedType.Gifv);
                if (embed.Video.HasValue)
                    embedBuilder.WithImageUrl(embed.Video.Value.Url);
            }
            else
            {
                Uri imageLink= null;
                if (message.Content.Split(' ').Any(link => Uri.TryCreate(link, UriKind.Absolute, out imageLink)))
                {
                    var filename = imageLink.Segments.LastOrDefault();
                    if (filename != null && _validImageExtensions.Any(ext => filename.EndsWith(ext, StringComparison.OrdinalIgnoreCase)))
                    {
                        embedBuilder.WithImageUrl(imageLink.ToString());
                    }
                }
            }

            var postedIn = $"**[#{message.Channel.Name} (go to message)]({message.GetJumpUrl()})**";

            if (message.Channel is ITextChannel guildChannel && guildChannel.GuildId != context.Guild.Id)
                postedIn += $"\nServer: {guildChannel.Guild.Name}";

            embedBuilder.AddField("Posted in", postedIn)
                        .WithFooter("React with ❌ to delete")
                        .WithTimestamp(message.Timestamp);

            return embedBuilder;
        }
    }
}
