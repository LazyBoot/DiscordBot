﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Webhook;
using Discord.WebSocket;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    public class HoggitLoggerService : IBotService
    {
        public class HoggitLoggerConfig
        {
            public string AuditChannel { get; set; } = null;

            [BsonId]
            public int _discarded { get; set; } = 1;

        }

        private ILogger<HoggitLoggerService> _logger;
        private readonly DiscordSocketClient _client;
        private ILiteCollection<HoggitLoggerConfig> _configDb;
        private HoggitLoggerConfig _config;
        private DiscordWebhookClient _webhookClient;

        public HoggitLoggerService(ILogger<HoggitLoggerService> logger, DiscordSocketClient client, LiteDatabase db)
        {
            _logger = logger;
            _client = client;
            _configDb = db.GetCollection<HoggitLoggerConfig>("HoggitLogger");
            _config = _configDb.FindAll().FirstOrDefault() ?? new HoggitLoggerConfig();

            if (!string.IsNullOrWhiteSpace(_config.AuditChannel))
                _webhookClient = new DiscordWebhookClient(_config.AuditChannel);
        }

        public async Task SendLogMessage(string message, bool allowPings = false)
        {
            if (_webhookClient == null)
                return;

            var allowedMentions = AllowedMentions.None;

            if (allowPings)
                allowedMentions = null;

            _logger.LogInformation(message);
            await _webhookClient.SendMessageAsync(message, username: _client.CurrentUser.Username, 
                avatarUrl: _client.CurrentUser.GetAvatarUrl() ?? _client.CurrentUser.GetDefaultAvatarUrl(),
                allowedMentions: allowedMentions);
        }

        public void SetLogWebhook(string url)
        {
            _config.AuditChannel = url;

            Save();

            if (!string.IsNullOrEmpty(_config.AuditChannel) && _webhookClient == null)
                _webhookClient = new DiscordWebhookClient(_config.AuditChannel);

            else if (string.IsNullOrEmpty(_config.AuditChannel))
                _webhookClient = null;
        }

        private void Save()
        {
            if (!_configDb.Update(_config))
            {
                _configDb.Insert(_config);
            }

            Update();
        }

        private void Update()
        {
            var db = _configDb.FindAll().FirstOrDefault();
            if (db == null)
            {
                throw new NullReferenceException("The HoggitLoggerService update action was unable to find a valid HoggitLoggerConfig document");
            }

            _config.AuditChannel = db.AuditChannel;
        }

    }
}
