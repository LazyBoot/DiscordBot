﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Services;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot;

public class TimerService : IBotService, IStopOnShutdown
{
    private ILogger<TimerService> _logger;
    private ConcurrentDictionary<Guid, TimerData> _timerData = new();

    public TimerService(ILogger<TimerService> logger)
    {
        _logger = logger;
    }

    public bool StartTimer(ISupportsTimer data, Action action)
    {
        var timer = _timerData.GetOrAdd(data.Id, new TimerData(data.Id));
        if (timer.Running)
            return false;

        timer.StartTimer(data.EndTime, action);
        return true;
    }

    public bool StopTimer(ISupportsTimer data) => StopTimer(data.Id);

    public bool StopTimer(Guid guid)
    {
        var result = _timerData.TryGetValue(guid, out var timer);
        if (!result)
            return false;

        result = timer.StopTimer();
        if (!result)
            return false;

        result = _timerData.TryRemove(timer.Id, out _);
        return result;
    }

    public Task Stop()
    {
        foreach (var timer in _timerData)
        {
            timer.Value.Dispose();
        }
        return Task.CompletedTask;
    }
}
