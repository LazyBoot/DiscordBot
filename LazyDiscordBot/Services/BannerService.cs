﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    public class BannerService : IBotService, IRunOnStartup, IStopOnShutdown
    {
        private readonly ILogger<BannerService> _logger;
        private readonly DiscordSocketClient _client;
        private readonly ILiteCollection<BannerData> _bannerStatus;
        private readonly Timer _timer;
        private readonly Random _random;
        private readonly IHttpClientFactory _httpClientFactory;

        public BannerService(ILogger<BannerService> logger, DiscordSocketClient client, LiteDatabase db, IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _client = client;
            _logger = logger;

            _timer = new Timer
            {
                Interval = TimeSpan.FromHours(4).TotalMilliseconds,
                AutoReset = true,
            };
            _timer.Elapsed += TimerElapsed;

            _bannerStatus = db.GetCollection<BannerData>("GuildBanners");

            _random = new Random();
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            _ = ChangeBanners();
        }

        public Task Initialize()
        {
            if (_bannerStatus.Count() > 0)
            { 
                _timer.Enabled = true;
                _logger.LogInformation("Banners found, starting timer");
            }
            else
                _logger.LogInformation("No guilds set up with banners, not starting timer");

            return Task.CompletedTask;
        }

        public Task Stop()
        {
            _timer.Dispose();

            return Task.CompletedTask;
        }


        private async Task ChangeBanners()
        {
            if (!_client.IsConnected())
                return;

            _logger.LogInformation("Checking banners");
            var bannersToChange = _bannerStatus.Find(b => b.LastChanged.Date.AddDays(b.ChangeIntervalDays) <= DateTime.UtcNow.Date).ToList();

            _logger.LogInformation("Found {count} banners to change", bannersToChange.Count);

            foreach (var bannerData in bannersToChange)
            {
                await ChangeBanner(bannerData);
            }
        }

        private async Task ChangeBanner(BannerData bannerData)
        {
            try
            {
                var guild = _client.GetGuild(bannerData.GuildId);
                var bannerChannel = guild.GetTextChannel(bannerData.BannerChannel);

                var banners = (await bannerChannel.GetMessagesAsync(500).FlattenAsync()).OrderBy(m => m.Timestamp.UtcDateTime)
                              .SelectMany(m => m.Attachments).Where(a => a.Height > 0).ToList();

                if (banners.Any())
                {
                    _logger.LogInformation("Changing banner for guild: {guild}", guild);

                    var bannerNum = _random.Next(banners.Count);
                    while (bannerData.LastBanner == banners[bannerNum].Id)
                        bannerNum = _random.Next(banners.Count);

                    var banner = banners[bannerNum];

                    var httpClient = _httpClientFactory.CreateClient();
                    await using var stream = await httpClient.GetStreamAsync(banner.Url);
                    await guild.ModifyAsync(g => g.Banner = new Image(stream));
                    bannerData.LastChanged = DateTime.UtcNow;
                    bannerData.LastBanner = banner.Id;
                    _bannerStatus.Update(bannerData);
                }
                else
                    _logger.LogInformation("No banners found for guild: {guild}", guild);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.ToString(), bannerData);
            }
        }

        public async Task<RuntimeResult> ForceChange(IGuild guild)
        {
            BannerData bannerData = GetBannerData(guild);
            if (bannerData == null)
                return CommandResult.FromError("Guild is not set up for banner rotation");

            await ChangeBanner(bannerData);
            return CommandResult.FromSuccess("Done");
        }

        internal BannerData GetBannerData(IGuild guild)
            => _bannerStatus.FindOne(b => b.GuildId == guild.Id);

        public async Task<RuntimeResult> AddGuild(IGuild guild, ITextChannel channel, int interval = 3)
        {
            if (_bannerStatus.FindOne(b => b.GuildId == guild.Id) != null)
                return CommandResult.FromError("Guild is already set up for banner rotation");

            var bannerData = new BannerData()
            {
                GuildId = guild.Id,
                BannerChannel = channel.Id,
                ChangeIntervalDays = interval
            };

            await ChangeBanner(bannerData);

            _bannerStatus.Insert(bannerData);

            if (_bannerStatus.Count() > 0)
            {
                _logger.LogInformation("Starting banner timer");
                _timer.Enabled = true;
            }

            return CommandResult.FromSuccess("Added");
        }

        public RuntimeResult RemoveGuild(IGuild guild)
        {
            var result = _bannerStatus.DeleteMany(b => b.GuildId == guild.Id);

            if (_bannerStatus.Count() == 0)
            {
                _logger.LogInformation("No guilds set up, stopping timer");
                _timer.Enabled = false;
            }

            return result > 0 ? CommandResult.FromSuccess("Removed") : CommandResult.FromError("Guild is not set up for banner rotation");
        }
    }

    internal class BannerData
    {
        [BsonId] public ulong GuildId { get; set; }
        public ulong BannerChannel { get; set; }
        public DateTime LastChanged { get; set; }
        public int ChangeIntervalDays { get; set; }
        public ulong? LastBanner { get; set; }
    }
}
