﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMembers | GatewayIntents.GuildMessages | GatewayIntents.MessageContent)]
    public class PingDetectorService : IBotService, IEnableDisableGuildService
    {
        public string ServiceName { get; } = "Ping Detector";

        public class PingDetectorConfig
        {
            public HashSet<ulong> EnabledGuilds { get; set; } = new HashSet<ulong>();

            [BsonId]
            public int _discarded { get; set; } = 1;

        }

        private readonly ILogger _logger;
        private readonly DiscordSocketClient _client;
        private readonly ILiteCollection<PingDetectorConfig> _configDb;
        private readonly PingDetectorConfig _config;

        public PingDetectorService(ILogger<PingDetectorService> logger, DiscordSocketClient client, LiteDatabase db)
        {
            _logger = logger;
            _client = client;

            _configDb = db.GetCollection<PingDetectorConfig>("PingDetector");
            _config = _configDb.FindAll().FirstOrDefault() ?? new PingDetectorConfig();

            _client.MessageUpdated += _client_MessageUpdated;
            _client.MessageDeleted += _client_MessageDeleted;
        }

        private async Task _client_MessageDeleted(Cacheable<IMessage, ulong> oldMessage, Cacheable<IMessageChannel, ulong> chan)
        {
            var messageChannel = await chan.GetOrDownloadAsync();

            if (!oldMessage.HasValue
                || oldMessage.Value.EditedTimestamp != null
                || !(messageChannel is ITextChannel channel)
                || oldMessage.Value.Source != MessageSource.User
                || !IsGuildEnabled(channel.GuildId))
                return;

            var removedUserPingsIds = oldMessage.Value.MentionedUserIds;
            var removedRolePingsIds = oldMessage.Value.MentionedRoleIds;

            if (!(removedRolePingsIds.Any() || removedUserPingsIds.Any())) return;

            await SendRemovedPingWarnings(channel, oldMessage.Value.Author, removedUserPingsIds, removedRolePingsIds, true);
        }

        private async Task _client_MessageUpdated(Cacheable<IMessage, ulong> oldMessage, SocketMessage rawMessage, ISocketMessageChannel messageChannel)
        {
            if (!oldMessage.HasValue
                || oldMessage.Value.EditedTimestamp != null
                || !(messageChannel is ITextChannel channel)
                || !(rawMessage is SocketUserMessage newMessage)
                || newMessage.Source != MessageSource.User
                || !IsGuildEnabled(channel.GuildId)
                || newMessage.Content == oldMessage.Value.Content)
                    return;

            var removedUserPingsIds = oldMessage.Value.MentionedUserIds.Except(newMessage.MentionedUsers.Select(u => u.Id));
            var removedRolePingsIds = oldMessage.Value.MentionedRoleIds.Except(newMessage.MentionedRoles.Select(r => r.Id));

            if (!(removedRolePingsIds.Any() || removedUserPingsIds.Any())) return;

            await SendRemovedPingWarnings(channel, newMessage.Author, removedUserPingsIds, removedRolePingsIds);
        }


        private async Task SendRemovedPingWarnings(ITextChannel channel, IUser author, IEnumerable<ulong> removedUserPingsIds, IEnumerable<ulong> removedRolePingsIds, bool deleted = false)
        {
            var builder = new StringBuilder("Stealth ping detected:");

            foreach (var user in removedUserPingsIds)
            {
                builder.Append($" {MentionUtils.MentionUser(user)}");
            }

            foreach (var role in removedRolePingsIds)
            {
                builder.Append($" {MentionUtils.MentionRole(role)}");
            }
            builder.AppendLine().Append($"- Message from: {author.Mention}");

            if (deleted)
                builder.Append(" (message deleted)");

            await channel.SendMessageAsync(builder.ToString());
        }

        public bool EnableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Add(guildId);
            
            if (result) Save();

            return result;
        }

        public bool DisableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Remove(guildId);

            if (result) Save();

            return result;
        }

        public bool IsGuildEnabled(ulong guildId)
            => _config.EnabledGuilds.Contains(guildId);


        public void Save()
        {
            if (!_configDb.Update(_config))
            {
                _configDb.Insert(_config);
            }

            Update();
        }

        /// <summary>
        /// Updates the prefix dictionary to the most recently stored version
        /// </summary>
        private void Update()
        {
            var db = _configDb.FindAll().FirstOrDefault();
            if (db == null)
            {
                throw new NullReferenceException("The PingDetectorService update action was unable to find a valid PingDetectorConfig document");
            }

            _config.EnabledGuilds = db.EnabledGuilds;
        }


    }
}
