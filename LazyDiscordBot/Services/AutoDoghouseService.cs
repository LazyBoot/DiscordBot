﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Humanizer;
using Humanizer.Localisation;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessages | GatewayIntents.MessageContent)]
    public class AutoDoghouseService : IBotService, IRunOnStartup
    {
        private const RegexOptions _options = RegexOptions.Compiled | RegexOptions.IgnoreCase;
        private readonly DiscordSocketClient _discord;
        private ILiteCollection<DhTrigger> _triggerCollection;
        private ILiteCollection<EmbedTrigger> _embedTriggerCollection;
        private readonly CommandService _commandService;
        private readonly PrefixService _prefixService;
        private readonly DoghouseService _doghouseService;
        private readonly DoghouseHistoryService _historyService;
        private readonly HoggitLoggerService _loggerService;
        private readonly ILogger<AutoDoghouseService> _logger;

        public ConcurrentDictionary<ulong, ConcurrentDictionary<string, DhTrigger>> Triggers { get; private set; }
        public ConcurrentDictionary<ulong, ConcurrentDictionary<ulong, EmbedTrigger>> EmbedTriggers { get; private set; }

        public class DhTrigger
        {
            [BsonId] public string BsonId => GuildId + Name.ToString();
            public Regex Name { get; set; }
            public string Message { get; set; }
            public ulong GuildId { get; set; }
            public TimeSpan DhTime { get; set; }

            public bool Equals(DhTrigger other)
            {
                return GuildId == other?.GuildId && Name == other.Name;
            }
        }

        public class EmbedTrigger
        {
            [BsonId] public string BsonId => GuildId + ChannelId.ToString();
            public ulong ChannelId { get; set; }
            public string Message { get; set; }
            public ulong GuildId { get; set; }
            public TimeSpan DhTime { get; set; }

            public bool Equals(EmbedTrigger other)
            {
                return GuildId == other?.GuildId && ChannelId == other.ChannelId;
            }
        }


        public AutoDoghouseService(DiscordSocketClient discord, LiteDatabase db, CommandService commandService, PrefixService prefixService,
                                   DoghouseService doghouseService, DoghouseHistoryService historyService, HoggitLoggerService loggerService,
                                   ILogger<AutoDoghouseService> logger)
        {
            _prefixService = prefixService;
            _doghouseService = doghouseService;
            _historyService = historyService;
            _loggerService = loggerService;
            _logger = logger;
            _commandService = commandService;
            _discord = discord;
            _triggerCollection = db.GetCollection<DhTrigger>("AutoDHTrigger");
            _embedTriggerCollection = db.GetCollection<EmbedTrigger>("AutoDHEmbedTrigger");

            Triggers = new ConcurrentDictionary<ulong, ConcurrentDictionary<string, DhTrigger>>();
            EmbedTriggers = new ConcurrentDictionary<ulong, ConcurrentDictionary<ulong, EmbedTrigger>>();
        }


        public Task Initialize()
        {
            foreach (var trigger in _triggerCollection.FindAll().ToDictionary(w => w.GuildId + w.Name.ToString()))
            {
                var guild = Triggers.GetOrAdd(trigger.Value.GuildId, (key) => new ConcurrentDictionary<string, DhTrigger>());
                guild.AddOrUpdate(trigger.Value.Name.ToString(), trigger.Value, (k, v) => trigger.Value);
            }
            foreach (var emTrigger in _embedTriggerCollection.FindAll().ToDictionary(w => w.GuildId + w.ChannelId.ToString()))
            {
                var guild = EmbedTriggers.GetOrAdd(emTrigger.Value.GuildId, (key) => new ConcurrentDictionary<ulong, EmbedTrigger>());
                guild.AddOrUpdate(emTrigger.Value.ChannelId, emTrigger.Value, (k, v) => emTrigger.Value);
            }
            _discord.MessageReceived += DiscordMessageReceived;
            _discord.MessageUpdated += DiscordMessageUpdated;

            return Task.CompletedTask;
        }

        private async Task DiscordMessageUpdated(Cacheable<IMessage, ulong> origMessage, SocketMessage newMessage, ISocketMessageChannel messageChannel)
        {
            await DiscordMessageReceived(newMessage);
        }

        private async Task DiscordMessageReceived(SocketMessage rawMessage)
        {
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;
            if (!(message.Channel is SocketTextChannel channel)) return;
            if (message.Author.Id == 0) return;

            if (!channel.Guild.CurrentUser.GuildPermissions.ManageRoles) return;

            var context = new SocketCommandContext(_discord, message);

            var guildUser = context.User as IGuildUser ?? await context.Client.Rest.GetGuildUserAsync(context.Guild.Id, context.User.Id);

            if (!Triggers.TryGetValue(context.Guild.Id, out var triggers) || context.Guild.CurrentUser.Hierarchy < guildUser.GetHierarchy())
            {
                await CheckEmbeds(context);
                return;
            }

            var role = context.Guild.GetRole(DoghouseService.DoghouseRole);
            foreach (var trigger in triggers.Where(trigger => trigger.Value.Name.IsMatch(context.Message.Content)))
            {
                _logger.LogInformation($"AutoDoghouse Triggered, {guildUser.Username} ({guildUser.Id})", guildUser, context.Message);
                await context.Message.DeleteAsync();
                await _doghouseService.AddDoghouse(context, role, guildUser, trigger.Value.DhTime, $"AutoDoghouse: {trigger.Value.Message}", overwriteIfLonger: true);

                var humanTime = trigger.Value.DhTime.TotalHours > 2
                    ? trigger.Value.DhTime.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour)
                    : trigger.Value.DhTime.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);

                var history = _historyService.FetchHistory(guildUser.Id) switch
                {
                    null => "(No known priors.)",
                    var h => $"({h.DoghouseCount} previous doghousings, totalling ~ {h.Duration.Humanize(2, maxUnit: TimeUnit.Day, minUnit: TimeUnit.Hour)}.)",
                };

                await _loggerService.SendLogMessage($"AUTO DOGHOUSE: {guildUser.Mention} ({guildUser.ToString()}) was added to {role.Mention} for {humanTime} in {(context.Channel as ITextChannel).Mention} reason: \"{Format.Sanitize(trigger.Value.Message)}\" ([go to](<{context.Message.GetJumpUrl()}>))\n{history}");
                return;
            }

            await CheckEmbeds(context);
        }

        private async Task CheckEmbeds(SocketCommandContext context)
        {
            if (!EmbedTriggers.TryGetValue(context.Guild.Id, out var emTriggers))
                return;

            if (!context.Message.Embeds.Any(em => em.Type == EmbedType.Video || em.Video.HasValue))
                return;

            var guildUser = context.User as IGuildUser ?? await context.Client.Rest.GetGuildUserAsync(context.Guild.Id, context.User.Id);
            var channelId = context.Channel.Id;
            if (context.Channel is SocketThreadChannel threadChannel)
                channelId = threadChannel.ParentChannel.Id;

            if (emTriggers.Any(tr => tr.Value.ChannelId == channelId))
            {
                await _loggerService.SendLogMessage($"Video Detected! {guildUser.Mention} ({guildUser.ToString()}) might have posted a video in {(context.Channel as ITextChannel).Mention} ([go to](<{context.Message.GetJumpUrl()}>))");
            }
        }

        public RuntimeResult AddTrigger(IGuild guild, string name, TimeSpan dhTime, string message)
        {
            var guildTriggers = Triggers.GetOrAdd(guild.Id, new ConcurrentDictionary<string, DhTrigger>());

            if (guildTriggers.ContainsKey(name))
                return CommandResult.FromError("Trigger already exists");

            var prefix = _prefixService.GetPrefix(guild.Id);
            if (name.StartsWith(prefix) && _commandService.Search(name.Substring(prefix.Length)).IsSuccess)
                return CommandResult.FromError("Trigger already exists");

            var trigger = new DhTrigger { GuildId = guild.Id, Name = new Regex(name, _options), Message = message, DhTime = dhTime };
            if (guildTriggers.TryAdd(trigger.Name.ToString(), trigger))
                _triggerCollection.Insert(trigger);

            return CommandResult.FromSuccess("Added trigger");
        }

        public RuntimeResult UpdateTrigger(IGuild guild, string name, TimeSpan dhTime, string message)
        {
            if (!Triggers.TryGetValue(guild.Id, out var guildTriggers))
                return CommandResult.FromError("Trigger does not exist");

            if (!guildTriggers.TryGetValue(name, out var oldTrigger))
                return CommandResult.FromError("Trigger does not exist");

            var trigger = new DhTrigger { GuildId = guild.Id, Name = new Regex(name, _options), Message = message };
            if (guildTriggers.TryUpdate(trigger.Name.ToString(), trigger, oldTrigger))
            {
                _triggerCollection.Update(trigger);
                return CommandResult.FromSuccess("Updated trigger");
            }

            return CommandResult.FromError("Error updating trigger");
        }


        public RuntimeResult DeleteTrigger(IGuild guild, string name)
            => DeleteTrigger(guild.Id, name);

        public RuntimeResult DeleteTrigger(ulong guildId, string name)
        {
            if (!Triggers.TryGetValue(guildId, out var guildTriggers))
                return CommandResult.FromError("Trigger does not exist");

            var triggerId = guildId + name;
            if (!guildTriggers.ContainsKey(name))
                return CommandResult.FromError("Trigger does not exist");

            if (guildTriggers.TryRemove(name, out _))
            {
                _triggerCollection.Delete(triggerId);
                return CommandResult.FromSuccess("Removed trigger");
            }

            return CommandResult.FromError("Error removing trigger");
        }

        public IEnumerable<DhTrigger> ListTriggers(IGuild guild)
            => ListTriggers(guild.Id);

        public IEnumerable<DhTrigger> ListTriggers(ulong guildId)
        {
            return Triggers.GetValueOrDefault(guildId)?.Select(w => w.Value) ?? Enumerable.Empty<DhTrigger>();
        }

        public RuntimeResult AddNoVideoChannel(IGuild guild, IChannel channel, TimeSpan dhTime)
        {
            var guildTriggers = EmbedTriggers.GetOrAdd(guild.Id, new ConcurrentDictionary<ulong, EmbedTrigger>());

            if (guildTriggers.ContainsKey(channel.Id))
                return CommandResult.FromError("Channel Trigger already enabled");

            var trigger = new EmbedTrigger { GuildId = guild.Id, ChannelId = channel.Id, Message = $"Video in #{channel.Name}", DhTime = dhTime };
            if (guildTriggers.TryAdd(trigger.ChannelId, trigger))
                _embedTriggerCollection.Insert(trigger);

            return CommandResult.FromSuccess("Channel Trigger Enabled");
        }

        public RuntimeResult UpdateNoVideoChannel(IGuild guild, IChannel channel, TimeSpan dhTime)
        {
            if (!EmbedTriggers.TryGetValue(guild.Id, out var guildTriggers))
                return CommandResult.FromError("Channel Trigger is not enabled");

            if (!guildTriggers.TryGetValue(channel.Id, out var oldTrigger))
                return CommandResult.FromError("Channel Trigger is not enabled");

            var trigger = new EmbedTrigger { GuildId = guild.Id, ChannelId = channel.Id, Message = $"Video in {channel.Name}", DhTime = dhTime };

            if (guildTriggers.TryUpdate(trigger.ChannelId, trigger, oldTrigger))
            {
                _embedTriggerCollection.Update(trigger);
                return CommandResult.FromSuccess("Updated trigger");
            }

            return CommandResult.FromError("Error updating trigger");
        }

        public RuntimeResult DeleteNoVideoChannel(IGuild guild, IChannel channel)
        {
            if (!EmbedTriggers.TryGetValue(guild.Id, out var guildTriggers))
                return CommandResult.FromError("Trigger does not exist");

            var triggerId = guild.Id + channel.Id.ToString();
            if (!guildTriggers.ContainsKey(channel.Id))
                return CommandResult.FromError("Trigger does not exist");

            if (guildTriggers.TryRemove(channel.Id, out _))
            {
                _embedTriggerCollection.Delete(triggerId);
                return CommandResult.FromSuccess("Disabled Channel Trigger");
            }

            return CommandResult.FromError("Error removing trigger");
        }

        public IEnumerable<EmbedTrigger> ListNoVideoTriggers(IGuild guild)
        {
            return EmbedTriggers.GetValueOrDefault(guild.Id)?.Select(w => w.Value) ?? Enumerable.Empty<EmbedTrigger>();
        }

    }
}
