using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using Discord.Commands;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Models;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMembers | GatewayIntents.GuildVoiceStates)]
    public class AudioService : IBotService
    {
        private readonly ConcurrentDictionary<ulong, IAudioClient> _connectedChannels = new ConcurrentDictionary<ulong, IAudioClient>();
        private readonly ConcurrentDictionary<ulong, AudioOutStream> _streams = new ConcurrentDictionary<ulong, AudioOutStream>();
        /*private readonly string _currentFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                                 Path.DirectorySeparatorChar + "Audio" + Path.DirectorySeparatorChar;*/
        private readonly string _currentFolder = Path.Combine(Program.ProgramPath, "Data", "Audio");

        private Process _ffmpeg;

        private readonly ILogger _logger;

        public AudioService(ILogger<AudioService> logger)
        {
            _logger = logger;
        }



        public async Task<(RuntimeResult, IAudioClient)> JoinAudio(SocketCommandContext context, IVoiceChannel target = null, bool autoConnect = false)
        {
            if (_connectedChannels.TryGetValue(context.Guild.Id, out IAudioClient _))
            {
                return (CommandResult.FromError(), null);
            }

            target ??= (context.Message.Author as IGuildUser)?.VoiceChannel;
            if (target == null)
            {
                return (CommandResult.FromError("You must be in a voice channel."), null);
            }


            if (target.Guild.Id != context.Guild.Id)
            {
                return (CommandResult.FromError(), null);
            }

            var channelPermissions = context.Guild.CurrentUser.GetPermissions(target);
            if (!(channelPermissions.Connect && channelPermissions.ViewChannel))
            {
                return (CommandResult.FromError("I don't have access to that channel"), null);
            }

            var audioClient = await target.ConnectAsync();
            if (audioClient == null)
                return (CommandResult.FromError(), null);

            if (_connectedChannels.TryAdd(context.Guild.Id, audioClient))
            {
                // If you add a method to log happenings from this service,
                // you can uncomment these commented lines to make use of that.
                _logger.LogDebug($"Connected to voice on {context.Guild.Name}.");

                if (!autoConnect)
                    await SendAudioFileAsync(context, Path.Combine(_currentFolder, "join.wav"), target);

                return (CommandResult.FromSuccess(), audioClient);
            }

            return (CommandResult.FromError(), null);
        }

        public async Task<RuntimeResult> LeaveAudio(ICommandContext context)
        {
            if (_streams.TryRemove(context.Guild.Id, out AudioOutStream stream))
                await stream.FlushAsync();

            if (_connectedChannels.TryRemove(context.Guild.Id, out IAudioClient client))
            {
                await client.StopAsync();
                _logger.LogDebug($"Disconnected from voice on {context.Guild.Name}.");

                return StopAudio();

            }
            return CommandResult.FromError("I am not in a voice channel");
        }

        public RuntimeResult StopAudio()
        {
            try
            {
                if (_ffmpeg != null && !_ffmpeg.HasExited)
                {
                    _ffmpeg.Kill();
                }
            }
            catch (InvalidOperationException)
            {
                _ffmpeg = null;
            }

            return CommandResult.FromSuccess();
        }

        public async Task<RuntimeResult> SendAudioAsync(SocketCommandContext context, string soundName, IVoiceChannel voiceChannel = null)
        {
            var file = GetSound(soundName)?.FileName;
            if (file == null)
            {
                return CommandResult.FromError("No sound by that name");
            }

            var path = Path.Combine(_currentFolder, file);

            return await SendAudioFileAsync(context, path, voiceChannel);
        }

        public async Task<RuntimeResult> SendAudioFileAsync(SocketCommandContext context, string path, IVoiceChannel voiceChannel = null)
        {
            var autoConnected = false;
            if (!File.Exists(path))
            {
                return CommandResult.FromError("File does not exist.");
            }

            if (!_connectedChannels.TryGetValue(context.Guild.Id, out IAudioClient client))
            {
                var channel = voiceChannel ?? (context.Message.Author as IGuildUser)?.VoiceChannel;

                RuntimeResult connected;
                (connected, client) = await JoinAudio(context, channel, true);
                if (!connected.IsSuccess)
                    return connected;

                autoConnected = true;

            }

            _logger.LogDebug($"Starting playback of {path} in {context.Guild.Name}");
            try
            {
                if (_ffmpeg != null && !_ffmpeg.HasExited)
                {
                    _ffmpeg.Kill();
                    _ffmpeg.Dispose();
                }
            }
            catch (InvalidOperationException)
            {
                _ffmpeg = null;
            }

            using (_ffmpeg = CreateProcess(path))
            {
                var stream = _streams.GetOrAdd(context.Guild.Id, client.CreatePCMStream(AudioApplication.Mixed, bufferMillis: 300, packetLoss: 5));
                try
                {
                    //await client.SetSpeakingAsync(true);
                    await _ffmpeg.StandardOutput.BaseStream.CopyToAsync(stream);
                }
                finally
                {
                    //await client.SetSpeakingAsync(false);

                    if (autoConnected)
                    {
                        await LeaveAudio(context);
                    }
                }
            }

            return CommandResult.FromSuccess();
        }

        private Process CreateProcess(string path)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-hide_banner -nostdin -loglevel panic -i \"{path}\" -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
        }

        public IAudioClient GetClient(IGuild guild)
        {
            if (_connectedChannels.TryGetValue(guild.Id, out IAudioClient client))
                return client;

            return null;
        }

        public CommandResult AddAudioFile(IMessageChannel channel, string file, string soundName, string description)
        {
            var audioFiles = GetSounds();

            if (GetSound(soundName, audioFiles) != null)
            {
                CommandResult.FromError("Sound name is already taken.");
            }

            soundName = soundName.ToLowerInvariant();

            var audioFile = string.IsNullOrEmpty(description) ? new AudioFile(file, soundName) : new AudioFile(file, soundName, description);

            if (!File.Exists(Path.Combine(_currentFolder, audioFile.FileName)))
            {
                return CommandResult.FromError("That file does not exist.");
            }

            audioFiles.Add(audioFile);

            SaveAudioFiles(audioFiles);

            return CommandResult.FromSuccess($"Added sound `{audioFile.Name}`");
        }

        private void SaveAudioFiles(List<AudioFile> audioFiles)
        {
            using var writer = new StreamWriter(Path.Combine(_currentFolder, @"audioFiles.json"));
            writer.WriteLine(JsonSerializer.Serialize(audioFiles, new JsonSerializerOptions { WriteIndented = true }));
        }

        private AudioFile GetSound(string soundName)
        {
            var sounds = GetSounds();
            return GetSound(soundName, sounds);
        }

        private AudioFile GetSound(string soundName, List<AudioFile> sounds)
        {
            return sounds.SingleOrDefault(s => s.Name == soundName.ToLowerInvariant());
        }

        public List<AudioFile> GetSounds()
        {
            List<AudioFile> audioFiles;
            var jsonFile = Path.Combine(_currentFolder, @"audioFiles.json");
            if (!File.Exists(jsonFile))
                return new List<AudioFile>();

            using (var reader = new StreamReader(jsonFile))
            {
                audioFiles = JsonSerializer.Deserialize<List<AudioFile>>(reader.ReadToEnd());
            }

            return audioFiles;
        }

        public AudioFile RemoveSound(string soundName)
        {
            var audioFiles = GetSounds();
            var sound = GetSound(soundName, audioFiles);

            if (sound == null) return null;

            audioFiles.Remove(sound);
            SaveAudioFiles(audioFiles);

            return sound;
        }
    }
}
