using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace LazyDiscordBot.Services
{

    public class DoghouseHistoryService : IBotService
    {
        private readonly ILogger<DoghouseHistoryService> _logger;
        private readonly ILiteCollection<DoghouseHistoryData> _historyCollection;

        public DoghouseHistoryService(LiteDatabase db, ILogger<DoghouseHistoryService> logger)
        {
            _logger = logger;
            _historyCollection = db.GetCollection<DoghouseHistoryData>("DoghouseHistory");
            _logger.LogInformation("DoghouseHistoryService Created");
        }

        private static DoghouseHistoryData ConvertDoghouseData(DoghouseData doghouseData)
        {
            List<DoghouseData> doghouses = new List<DoghouseData>() { doghouseData };
            return new DoghouseHistoryData(doghouseData.UserId, doghouses);
        }

        public DoghouseHistoryData SubmitHistory(DoghouseData data)
        {
            _logger.LogDebug($"History Submission: [{data}]");
            var history = FetchHistory(data.UserId);
            if (history != null)
            {
                _logger.LogDebug($"Doghousee already has history. [{history}]");
                history.AddDoghouseData(data);
            }
            else
            {
                history = ConvertDoghouseData(data);
            }
            _logger.LogDebug($"Committing History. [{history}]");
            _historyCollection.Upsert(history.UserId, history);
            _logger.LogDebug("History Committed.");
            return history;
        }

        public DoghouseHistoryData FetchHistory(ulong userId)
        {
            _logger.LogDebug($"History Query for User: [{userId}]");
            var history = _historyCollection.FindById(userId);
            _logger.LogDebug($"History found: [{history}]");
            return history;
        }
    
    }


}
