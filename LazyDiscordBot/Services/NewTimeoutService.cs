using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMembers)]
    public class NewTimeoutService : IBotService, IEnableDisableGuildService
    {
        public string ServiceName { get; } = "New User Timeout";

        public class NewTimeoutConfig
        {
            public HashSet<ulong> EnabledGuilds { get; set; } = new HashSet<ulong>();

            [BsonId]
            public int _discarded { get; set; } = 1;

        }

        private readonly ILogger _logger;
        private readonly DiscordSocketClient _client;
        private readonly ILiteCollection<NewTimeoutConfig> _configDb;
        private readonly NewTimeoutConfig _config;


        public NewTimeoutService(ILogger<NewTimeoutService> logger, LiteDatabase db, DiscordSocketClient client)
        {
            _logger = logger;
            _client = client;

            _configDb = db.GetCollection<NewTimeoutConfig>("NewTimeout");
            _config = _configDb.FindAll().FirstOrDefault() ?? new NewTimeoutConfig();

            _client.UserJoined += _client_UserJoined;
        }

        private async Task _client_UserJoined(SocketGuildUser user)
        {
            if (!IsGuildEnabled(user.Guild.Id))
                return;

            var accountAge = user.CreatedAt.ToUniversalTime().AddHours(36) - DateTimeOffset.UtcNow;
            if (accountAge.TotalMinutes > 0)
            {
                await user.SetTimeOutAsync(accountAge);
            }
        }

        public bool EnableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Add(guildId);

            if (result) Save();

            return result;
        }

        public bool DisableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Remove(guildId);

            if (result) Save();

            return result;
        }

        public bool IsGuildEnabled(ulong guildId)
            => _config.EnabledGuilds.Contains(guildId);


        public void Save()
        {
            if (!_configDb.Update(_config))
            {
                _configDb.Insert(_config);
            }

            Update();
        }

        /// <summary>
        /// Updates the prefix dictionary to the most recently stored version
        /// </summary>
        private void Update()
        {
            var db = _configDb.FindAll().FirstOrDefault();
            if (db == null)
            {
                throw new NullReferenceException("The LinkFixerService update action was unable to find a valid LinkFixerConfig document");
            }

            _config.EnabledGuilds = db.EnabledGuilds;
        }

    }
}
