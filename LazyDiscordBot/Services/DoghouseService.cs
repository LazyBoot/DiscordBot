﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Fergun.Interactive;
using Fergun.Interactive.Selection;
using Humanizer;
using Humanizer.Localisation;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Models;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessageReactions | GatewayIntents.GuildMembers | GatewayIntents.GuildBans)]
    public class DoghouseService : IBotService, IRunOnStartup, IStopOnShutdown
    {
        private readonly ILogger<DoghouseService> _logger;
        private readonly ILiteCollection<DoghouseData> _dhCollection;
        private readonly ILiteCollection<InertData> _inCollection;
        private readonly DiscordSocketClient _client;
        private readonly InteractiveService Interactive;
        private readonly DoghouseHistoryService _historyService;
        private readonly TimerService _timerService;

#if DEBUG
        public const ulong Guild = Guilds.LazyTest;
        public const ulong DoghouseRole = HoggitRoles.TestRole;
#else
        public const ulong Guild = Guilds.Hoggit;
        public const ulong DoghouseRole = HoggitRoles.Doghouse;
#endif
        public const ulong InertRole = HoggitRoles.Inert;

        private ConcurrentDictionary<Guid, DoghouseData> Doghouses { get; set; }
        private ConcurrentDictionary<Guid, InertData> Inerts { get; set; }

        public DoghouseService(ILogger<DoghouseService> logger, LiteDatabase db, DiscordSocketClient client, InteractiveService interactive,
                                DoghouseHistoryService historyService, TimerService timerService)
        {
            _client = client;
            Interactive = interactive;
            _logger = logger;
            _historyService = historyService;
            _timerService = timerService;

            _dhCollection = db.GetCollection<DoghouseData>("Doghouse");
            _inCollection = db.GetCollection<InertData>("Inert");

            Doghouses = new ConcurrentDictionary<Guid, DoghouseData>(_dhCollection.FindAll().ToDictionary(d => d.Id));
            Inerts = new ConcurrentDictionary<Guid, InertData>(_inCollection.FindAll().ToDictionary(d => d.Id));
        }

        public Task Initialize()
        {
            _logger.LogInformation("Starting Doghouse Service");

            if (_client.Guilds.All(g => g.Id != Guild))
                return Task.CompletedTask;

            foreach (var (id, dh) in Doghouses)
            {
                StartTimer(dh);
            }

            _client.UserJoined += UserJoined;
            _client.GuildMemberUpdated += _client_GuildMemberUpdated;
            _client.UserBanned += _client_UserBanned;

            return Task.CompletedTask;
        }

        public Task Stop()
        {
            return Task.CompletedTask;
        }

        private bool StartTimer(DoghouseData doghouseData)
        {
            if (doghouseData.EndTime == DateTime.MaxValue)
                return true;

            return _timerService.StartTimer(doghouseData, async () => await ProcessDoghouse(doghouseData));
        }


        private Task _client_UserBanned(SocketUser user, SocketGuild guild)
        {
            if (guild.Id != Guild)
                return Task.CompletedTask;

            foreach (var doghouseData in Doghouses.Values.Where(dh => dh.UserId == user.Id))
            {
                _logger.LogInformation($"Doghouse removed from {user.Id} -- banned");
                ClearDoghouseState(doghouseData);
            }

            foreach (var inertData in Inerts.Values.Where(dh => dh.UserId == user.Id))
            {
                _logger.LogInformation($"Inert removed from {user} -- banned");
                Inerts.TryRemove(inertData.Id, out _);
                _inCollection.Delete(inertData.Id);
            }

            return Task.CompletedTask;
        }

        private Task _client_GuildMemberUpdated(Cacheable<SocketGuildUser, ulong> oldUserCache, SocketGuildUser newUser)
        {
            if (newUser.Guild.Id != Guild || !oldUserCache.HasValue)
                return Task.CompletedTask;

            var oldUser = oldUserCache.Value;
            var hoggit = _client.GetGuild(Guild);

            // Doghouse checks
            var role = hoggit.GetRole(DoghouseRole);
            if (role != null && newUser.Roles.Any(r => r.Id == role.Id) && oldUser.Roles.All(r => r.Id != role.Id) && Doghouses.All(u => u.Value.UserId != newUser.Id))
            {
                var doghouseData = new DoghouseData(newUser.Id, _client.CurrentUser.Id, TimeSpan.MaxValue, "Manually added");
                _logger.LogInformation($"Doghouse added to {newUser.Id}");

                Doghouses.TryAdd(doghouseData.Id, doghouseData);
                _dhCollection.Insert(doghouseData.Id, doghouseData);
            }

            if (role != null && oldUser.Roles.Any(r => r.Id == role.Id) && newUser.Roles.All(r => r.Id != role.Id))
            {
                foreach (var doghouseData in Doghouses.Values.Where(dh => dh.UserId == newUser.Id))
                {
                    _logger.LogInformation($"Doghouse removed from {newUser.Id}");
                    ClearDoghouseState(doghouseData);
                }
            }

            // Inert (no reactions)
            role = hoggit.GetRole(InertRole);
            if (role != null && newUser.Roles.Any(r => r.Id == role.Id) && oldUser.Roles.All(r => r.Id != role.Id) && Inerts.All(u => u.Value.UserId != newUser.Id))
            {
                _logger.LogInformation($"Inert added to {newUser}");
                var inertData = new InertData(newUser.Id, TimeSpan.MaxValue);
                Inerts.TryAdd(inertData.Id, inertData);
                _inCollection.Insert(inertData.Id, inertData);
            }

            if (role != null && oldUser.Roles.Any(r => r.Id == role.Id) && newUser.Roles.All(r => r.Id != role.Id))
            {
                foreach (var inertData in Inerts.Values.Where(dh => dh.UserId == newUser.Id))
                {
                    _logger.LogInformation($"Inert removed from {newUser}");
                    Inerts.TryRemove(inertData.Id, out _);
                    _inCollection.Delete(inertData.Id);
                }
            }

            return Task.CompletedTask;
        }

        private async Task UserJoined(SocketGuildUser user)
        {
            if (user.Guild.Id != Guild)
                return;

            var hoggit = _client.GetGuild(Guild);

            await JoinAddDoghouse(user, hoggit);
            await JoinAddInert(user, hoggit);
        }

        private async Task JoinAddInert(SocketGuildUser user, SocketGuild hoggit)
        {
            if (Inerts.All(u => u.Value.UserId != user.Id))
                return;

            var role = hoggit.GetRole(InertRole);

            if (user.Roles.Any(r => r.Id == role.Id))
                return;



            await JoinAddRole(user, role);
        }

        private async Task JoinAddDoghouse(SocketGuildUser user, SocketGuild hoggit)
        {
            if (Doghouses.All(u => u.Value.UserId != user.Id))
                return;

            var role = hoggit.GetRole(DoghouseRole);

            if (user.Roles.Any(r => r.Id == role.Id))
                return;

            await JoinAddRole(user, role);
        }

        private async Task JoinAddRole(SocketGuildUser user, IRole role)
        {

            try
            {
                await user.AddRoleAsync(role);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error reapplying doghouse");
            }
        }

        private async Task ProcessDoghouse(DoghouseData doghouse)
        {
            var hoggit = _client.GetGuild(Guild);
            var role = hoggit.GetRole(DoghouseRole);

            IGuildUser user = hoggit.GetUser(doghouse.UserId);
            user ??= await _client.Rest.GetGuildUserAsync(hoggit.Id, doghouse.UserId);

            _logger.LogInformation($"Doghouse for [{doghouse.UserId}] expired, removing.");

            if (user == null)
            {
                ClearDoghouseState(doghouse);
                return;
            }

            await RemoveDoghouse(role, user);
        }

        public async Task<RuntimeResult> AddDoghouse(SocketCommandContext context, IRole doghouse, IGuildUser user, TimeSpan duration, string reason, bool alwaysOverwrite = false, bool overwriteIfLonger = false)
        {
            try
            {
                DoghouseData doghouseData = null;
                DoghouseData oldDoghouseData = null;
                var overwrite = false;

                if (Doghouses.Any(dh => dh.Value.UserId == user.Id))
                {
                    oldDoghouseData = Doghouses.Values.FirstOrDefault(x => x.UserId == user.Id);

                    using var cancellationTokenSource = new System.Threading.CancellationTokenSource();

                    var timeLeft = oldDoghouseData.EndTime.ToUniversalTime() - DateTime.UtcNow;

                    var humanTimeLeft = timeLeft.TotalHours > 2
                        ? timeLeft.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour)
                        : timeLeft.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);

                    if (overwriteIfLonger && oldDoghouseData.EndTime > DateTime.UtcNow + duration)
                    {
                        return CommandResult.FromError();
                    }
                    else if (overwriteIfLonger && oldDoghouseData.EndTime < DateTime.UtcNow + duration)
                    {
                        alwaysOverwrite = true;
                    }

                    if (!alwaysOverwrite)
                    {
                        var emotes = new[] { CommandResult.SuccessReaction, CommandResult.FailedReaction };
                        var builder = new EmoteSelectionBuilder()
                            .WithUsers(context.User)
                            .WithOptions(emotes)
                            .WithInputType(InputType.Buttons)
                            .WithActionOnSuccess(ActionOnStop.DeleteMessage)
                            .WithActionOnTimeout(ActionOnStop.DeleteMessage)
                            .WithSelectionPage(new PageBuilder().WithDescription($"User is already doghoused for {humanTimeLeft}, overwrite?"));

                        var reply = await Interactive.SendSelectionAsync(builder.Build(), context.Channel, timeout: TimeSpan.FromSeconds(10));

                        if (reply.IsSuccess && reply.Value == CommandResult.SuccessReaction)
                            overwrite = true;

                        if (!overwrite)
                            return CommandResult.FromError("User already doghoused");
                    }
                    else
                        overwrite = true;

                    doghouseData = oldDoghouseData.Copy(duration, reason);
                    _timerService.StopTimer(oldDoghouseData.Id);
                }
                else
                    doghouseData = new DoghouseData(user.Id, context.Message.Author.Id, duration, reason);

                var timerResult = StartTimer(doghouseData);
                if (!timerResult)
                    return CommandResult.FromError("Error starting doghouse timer!");

                if (overwrite)
                {
                    Doghouses.TryUpdate(doghouseData.Id, doghouseData, oldDoghouseData);
                    _dhCollection.Update(doghouseData.Id, doghouseData);
                }
                else
                {
                    Doghouses.TryAdd(doghouseData.Id, doghouseData);
                    _dhCollection.Insert(doghouseData.Id, doghouseData);
                }

                if (!user.RoleIds.Any(r => r == doghouse.Id))
                    await user.AddRoleAsync(doghouse);

                return CommandResult.FromSuccess($"Doghoused {user}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to add doghouse to user {User}", user);
                return CommandResult.FromError($"Unable to add doghouse to user {user}");
            }
        }

        public async Task<RuntimeResult> RemoveDoghouse(IRole doghouse, IGuildUser user)
        {
            var doghouseData = Doghouses.Values.FirstOrDefault(dh => dh.UserId == user.Id);
            if (doghouseData == null)
                return CommandResult.FromError($"User is not in the doghouse");

            try
            {
                ClearDoghouseState(doghouseData);

                await user.RemoveRoleAsync(doghouse);

                return CommandResult.FromSuccess($"Removed {user} from the doghouse");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to remove doghouse from user {User}", user);
                return CommandResult.FromError($"Unable to remove doghouse from user {user}");
            }
        }

        public RuntimeResult RemoveDoghouse(IRole doghouse, ulong userId)
        {
            var doghouseData = Doghouses.Values.FirstOrDefault(dh => dh.UserId == userId);
            if (doghouseData == null)
                return CommandResult.FromError($"User is not in the doghouse");

            try
            {
                ClearDoghouseState(doghouseData);

                return CommandResult.FromSuccess($"Removed {userId} from the doghouse");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to remove doghouse from user {User}", userId);
                return CommandResult.FromError($"Unable to remove doghouse from user {userId}");
            }
        }

        public void ClearDoghouseState(DoghouseData doghouseData)
        {
            _logger.LogInformation($"Cleared doghouse from {doghouseData.UserId}");
            Doghouses.TryRemove(doghouseData.Id, out _);
            _dhCollection.Delete(doghouseData.Id);
            _timerService.StopTimer(doghouseData.Id);
            doghouseData.EndTime = DateTime.UtcNow;
            _historyService.SubmitHistory(doghouseData);
        }

        public List<DoghouseData> GetDoghouseDatas()
        {
            return new List<DoghouseData>(Doghouses.Values);
        }
    }
}
