using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildBans)]
    public class MessageDeleteNotifyService : IBotService
    {
        private const string _placeHolder = "Deleted User";
        private readonly ILogger<MessageDeleteNotifyService> _logger;
        private readonly DiscordSocketClient _client;
        private ILiteCollection<MessageDeleteNotifyConfig> _configDb;

        public MessageDeleteNotifyService(ILogger<MessageDeleteNotifyService> logger, LiteDatabase db, DiscordSocketClient client)
        {
            _logger = logger;
            _client = client;

            _configDb = db.GetCollection<MessageDeleteNotifyConfig>("MessageDelteNotifyService");

            _client.AuditLogCreated += _client_AuditLogCreated;
        }

        private async Task _client_AuditLogCreated(SocketAuditLogEntry entry, SocketGuild guild)
        {
            if (!IsGuildEnabled(guild.Id, out ITextChannel logChannel) || !guild.CurrentUser.GuildPermissions.ViewAuditLog)
                return;


            if (entry.Data is SocketMessageDeleteAuditLogData messageData)
            {
                var target = guild.GetUser(messageData.Target.Id);
#if !DEBUG
                if (entry.User.IsBot || target.IsBot)
                    return;
#endif

                await logChannel.SendMessageAsync($"{entry.User.Mention} [{entry.User}] deleted messages by {target.Mention ?? _placeHolder} [{target.ToString() ?? _placeHolder}]", allowedMentions: AllowedMentions.None);
            }
        }

        internal async Task Test(SocketGuild guild)
        {
            var logEntry = (await guild.GetAuditLogsAsync(1, actionType: ActionType.MessageDeleted).FlattenAsync()).FirstOrDefault();

            if (!IsGuildEnabled(guild.Id, out ITextChannel logChannel) || !guild.CurrentUser.GuildPermissions.ViewAuditLog)
                return;

            if (logEntry.Data is MessageDeleteAuditLogData messageData)
            {
                var target = guild.GetUser(messageData.Target.Id);
                await logChannel.SendMessageAsync($"TEST MESSAGE: {logEntry.User.Mention} [{logEntry.User}] deleted messages by {target.Mention ?? _placeHolder} [{target.ToString() ?? _placeHolder}]", allowedMentions: AllowedMentions.None);
            }
        }

        public bool IsGuildEnabled(ulong id, out ITextChannel logChannel)
        {

            if (_configDb.Exists(g => g.GuildId == id && g.LogChannelId != 0))
            {
                logChannel = _client.GetGuild(id).GetTextChannel(_configDb.FindOne(g => g.GuildId == id).LogChannelId);
                return true;
            }

            logChannel = null;
            return false;
        }

        public bool Enable(IGuild guild, ITextChannel channel)
        {

            if (_configDb.Exists(g => g.GuildId == guild.Id))
            {
                var config = _configDb.FindOne(g => g.GuildId == guild.Id);
                config.GuildId = guild.Id;
                config.LogChannelId = channel.Id;
                return _configDb.Update(config);
            }
            else
            {
                var config = new MessageDeleteNotifyConfig
                {
                    Id = Guid.NewGuid(),
                    GuildId = guild.Id,
                    LogChannelId = channel.Id,
                };

                _configDb.Insert(config);
                return true;
            }
        }

        public bool Disable(IGuild guild)
        {
            return _configDb.DeleteMany(g => g.GuildId == guild.Id) > 0;
        }

        internal bool CleanDB()
        {
            return _configDb.DeleteMany(q => true) > 0;
        }
    }

    public class MessageDeleteNotifyConfig
    {
        public ulong LogChannelId { get; set; }

        [BsonId]
        public Guid Id { get; set; }

        public ulong GuildId { get; set; }
    }


}
