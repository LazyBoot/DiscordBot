﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessages | GatewayIntents.MessageContent)]
    public partial class LinkFixerService : IBotService, IEnableDisableGuildService
    {
        public string ServiceName { get; } = "Link Fixer";

        [GeneratedRegex("https?://(?<prefix>m|amp)\\.(?<site>youtube\\.com|twitch\\.tv)/\\S+", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant)]
        private static partial Regex Pattern();

        public class LinkFixerConfig
        {
            public HashSet<ulong> EnabledGuilds { get; set; } = new HashSet<ulong>();

            [BsonId]
            public int _discarded { get; set; } = 1;

        }

        private readonly ILogger _logger;
        private readonly DiscordSocketClient _client;
        private readonly ILiteCollection<LinkFixerConfig> _configDb;
        private readonly LinkFixerConfig _config;


        public LinkFixerService(ILogger<LinkFixerService> logger, LiteDatabase db, DiscordSocketClient client)
        {
            _logger = logger;
            _client = client;

            _configDb = db.GetCollection<LinkFixerConfig>("LinkFixer");
            _config = _configDb.FindAll().FirstOrDefault() ?? new LinkFixerConfig();

            _client.MessageReceived += _client_MessageReceived;
        }

        private async Task _client_MessageReceived(SocketMessage rawMessage)
        {
            if (!(rawMessage is SocketUserMessage message)
                || message.Source != MessageSource.User
                || !(message.Channel is ITextChannel channel)
                || !IsGuildEnabled(channel.GuildId))
                return;

            if (!Pattern().IsMatch(message.Content))
                return;

            var replyMessage = new StringBuilder();

            foreach (Match match in Pattern().Matches(message.Content))
            {
                replyMessage.Append("Non-mobile link: <");
                replyMessage.Append(match.Value.Replace($"{match.Groups["prefix"]}.", string.Empty));
                replyMessage.AppendLine(">");
            }

            if (replyMessage.Length > 0)
                await message.ReplyAsync(replyMessage.ToString(), allowedMentions: AllowedMentions.None);
        }

        public bool EnableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Add(guildId);

            if (result) Save();

            return result;
        }

        public bool DisableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Remove(guildId);

            if (result) Save();

            return result;
        }

        public bool IsGuildEnabled(ulong guildId)
            => _config.EnabledGuilds.Contains(guildId);


        public void Save()
        {
            if (!_configDb.Update(_config))
                _configDb.Insert(_config);

            Update();
        }

        /// <summary>
        /// Updates the prefix dictionary to the most recently stored version
        /// </summary>
        private void Update()
        {
            var db = _configDb.FindAll().FirstOrDefault();
            if (db == null)
                throw new NullReferenceException("The LinkFixerService update action was unable to find a valid LinkFixerConfig document");

            _config.EnabledGuilds = db.EnabledGuilds;
        }

    }
}
