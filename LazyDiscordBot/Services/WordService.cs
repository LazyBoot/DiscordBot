﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Models;
using LiteDB;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessages | GatewayIntents.MessageContent)]
    public class WordService : IBotService
    {
        private const RegexOptions _options = RegexOptions.Compiled | RegexOptions.IgnoreCase;
        private readonly DiscordSocketClient _discord;
        private ILiteCollection<WordCommand> _wordCollection;
        private readonly CommandService _commandService;
        private readonly PrefixService _prefixService;
        public ConcurrentDictionary<ulong, ConcurrentDictionary<string, WordCommand>> Words { get; private set; }

        public class WordCommand
        {
            [BsonId] public string BsonId => GuildId + Name.ToString();
            public Regex Name { get; set; }
            public string Message { get; set; }
            public ulong GuildId { get; set; }

            public bool Equals(WordCommand other)
            {
                return GuildId == other?.GuildId && Name == other.Name;
            }
        }


        public WordService(DiscordSocketClient discord, LiteDatabase db, CommandService commandService, PrefixService prefixService)
        {
            _prefixService = prefixService;
            _commandService = commandService;
            _discord = discord;
            _wordCollection = db.GetCollection<WordCommand>("commandWordsRX");

            Words = new ConcurrentDictionary<ulong, ConcurrentDictionary<string, WordCommand>>();
        }


        public void Initialize()
        {
            foreach (var word in _wordCollection.FindAll().ToDictionary(w => w.GuildId + w.Name.ToString()))
            {
                var guild = Words.GetOrAdd(word.Value.GuildId, (key) => new ConcurrentDictionary<string, WordCommand>());
                guild.AddOrUpdate(word.Value.Name.ToString(), word.Value, (k, v) => word.Value);
            }
            _discord.MessageReceived += DiscordMessageReceived;
        }

        private async Task DiscordMessageReceived(SocketMessage rawMessage)
        {
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;
            if (!(message.Channel is IGuildChannel)) return;

            var context = new SocketCommandContext(_discord, message);

            var argPos = 0;
            if (message.HasMentionPrefix(_discord.CurrentUser, ref argPos) ||
                message.HasStringPrefix(_prefixService.GetPrefix((message.Channel as IGuildChannel)?.Guild.Id ?? 0), ref argPos)
            )
            {
                return;
            }


            if (!Words.TryGetValue(context.Guild.Id, out var words))
                return;

            foreach (var word in words.Where(word => word.Value.Name.IsMatch(context.Message.Content)))
            {
                await message.ReplyAsync(word.Value.Message);
                break;
            }
        }

        public RuntimeResult AddWord(IGuild guild, string name, string message)
        {
            var guildWords = Words.GetOrAdd(guild.Id, new ConcurrentDictionary<string, WordCommand>());

            if (guildWords.ContainsKey(name))
                return CommandResult.FromError("Word already exists");

            var prefix = _prefixService.GetPrefix(guild.Id);
            if (name.StartsWith(prefix) && _commandService.Search(name.Substring(prefix.Length)).IsSuccess)
                return CommandResult.FromError("Word already exists");

            var word = new WordCommand { GuildId = guild.Id, Name = new Regex(name, _options), Message = message };
            if (guildWords.TryAdd(word.Name.ToString(), word))
                _wordCollection.Insert(word);

            return CommandResult.FromSuccess("Added word");
        }

        public RuntimeResult UpdateWord(IGuild guild, string name, string message)
        {
            if (!Words.TryGetValue(guild.Id, out var guildWords))
                return CommandResult.FromError("Word does not exist");

            if (!guildWords.TryGetValue(name, out var oldWord))
                return CommandResult.FromError("Word does not exist");

            var word = new WordCommand { GuildId = guild.Id, Name = new Regex(name, _options), Message = message };
            if (guildWords.TryUpdate(word.Name.ToString(), word, oldWord))
            {
                _wordCollection.Update(word);
                return CommandResult.FromSuccess("Updated word");
            }

            return CommandResult.FromError("Error updating word");
        }

        public RuntimeResult DeleteWord(IGuild guild, string name)
        {
            if (!Words.TryGetValue(guild.Id, out var guildWords))
                return CommandResult.FromError("Word does not exist");

            var wordId = guild.Id + name;
            if (!guildWords.ContainsKey(name))
                return CommandResult.FromError("Word does not exist");

            if (guildWords.TryRemove(name, out _))
            {
                _wordCollection.Delete(wordId);
                return CommandResult.FromSuccess("Removed word");
            }

            return CommandResult.FromError("Error removing word");
        }

        public IEnumerable<WordCommand> ListWords(IGuild guild)
        {
            return Words.GetValueOrDefault(guild.Id)?.Select(w => w.Value) ?? Enumerable.Empty<WordCommand>();
        }
    }
}
