﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.Commands.Builders;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Models;
using LazyDiscordBot.Preconditions;
using LiteDB;

namespace LazyDiscordBot.Services
{
    public class CustomCommandService : IBotService
    {
        private readonly CommandService _commandService;
        private ModuleInfo _customCommandsModule;
        private readonly ILiteCollection<CustomCommand> _commandCollection;
        public ConcurrentDictionary<string, CustomCommand> Commands { get; private set; }

        public class CustomCommand : IEquatable<CustomCommand>
        {
            [BsonId] public string BsonId => GuildId + Name;
            public string Name { get; set; }
            public string Message { get; set; }
            public ulong GuildId { get; set; }

            public bool Equals(CustomCommand other)
            {
                return GuildId == other?.GuildId && Name == other.Name;
            }
        }


        public CustomCommandService(CommandService commandService, LiteDatabase db)
        {
            _commandService = commandService;
            _commandCollection = db.GetCollection<CustomCommand>("CustomCommands");
            Commands = new ConcurrentDictionary<string, CustomCommand>(_commandCollection.FindAll().ToDictionary(c => c.GuildId + c.Name));

            CreateModule();
        }

        private async void CreateModule()
        {
            if (Commands.Count == 0)
                return;

            _customCommandsModule = await _commandService.CreateModuleAsync(string.Empty, moduleBuilder =>
            {
                foreach (var (_, command) in Commands)
                {
                    moduleBuilder.AddCommand(command.Name,
                        (context, objects, arg3, arg4) => context.Channel.SendMessageAsync(command.Message),
                        builder => builder.WithName(command.Name).AddAttributes(new CustomCommandAttribute())
                                          .AddPrecondition(new RequireGuild(command.GuildId))
                                          .AddParameter("placeholder", typeof(string), parameterBuilder => parameterBuilder.WithIsRemainder(true).WithIsOptional(true)));
                }
            });
        }


        public async Task<RuntimeResult> AddCommandAsync(ulong guildId, string name, string message)
        {
            var searchResult = _commandService.Search(name);
            if (searchResult.IsSuccess &&
                searchResult.Commands.All(c => c.Command.Attributes.All(a => a.GetType() != typeof(CustomCommandAttribute))))
                return CommandResult.FromError("Command already exists!");
            else if (searchResult.IsSuccess)
                return CommandResult.FromError("Custom command already exists!");

            var customCommand = new CustomCommand() {GuildId = guildId, Name = name, Message = message};
            if (_commandCollection.FindOne(command => command.Equals(customCommand)) != null)
                return CommandResult.FromError("Custom command already exists!");

            if (!Commands.TryAdd(customCommand.GuildId + customCommand.Name, customCommand))
                return CommandResult.FromError("Custom command already exists!");

            _commandCollection.Insert(customCommand);

            await _commandService.RemoveModuleAsync(_customCommandsModule);
            CreateModule();
            return CommandResult.FromSuccess();
        }

        public async Task<RuntimeResult> UpdateCommandAsync(ulong guildId, string name, string message)
        {
            var customCommand = new CustomCommand() {GuildId = guildId, Name = name, Message = message};
            var oldCommand = _commandCollection.FindOne(command => command.GuildId.Equals(guildId) && command.Equals(customCommand));

            if (oldCommand == null)
                return CommandResult.FromError("Custom command not found!");

            Commands.TryUpdate(guildId + name, customCommand, oldCommand);
            _commandCollection.Update(customCommand);
            Commands[name] = customCommand;

            await _commandService.RemoveModuleAsync(_customCommandsModule);
            CreateModule();
            return CommandResult.FromSuccess();
        }

        public async Task<RuntimeResult> RemoveCommandAsync(ulong guildId, string name)
        {
            var customCommand = _commandCollection.FindOne(command => command.GuildId.Equals(guildId) && command.Name.Equals(name));
            if (customCommand == null)
                return CommandResult.FromError("Custom command not found!");

            Commands.Remove(guildId + name, out _);
            _commandCollection.Delete(customCommand.GuildId + customCommand.Name);

            await _commandService.RemoveModuleAsync(_customCommandsModule);
            CreateModule();
            return CommandResult.FromSuccess();
        }

        public IEnumerable<CustomCommand> ListCommands(ulong guildId)
        {
            return Commands.Values.Where(c => c.GuildId == guildId);
        }
    }
}
