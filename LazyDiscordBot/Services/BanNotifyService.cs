using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using Humanizer;
using Humanizer.Localisation;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.Interfaces_Abstracts;
using LiteDB;
using Microsoft.Extensions.Logging;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMembers | GatewayIntents.GuildBans)]
    public class BanNotifyService : IBotService, IEnableDisableGuildService
    {
        public string ServiceName { get; } = "Ban Notification";

        private readonly ILogger _logger;
        private readonly DiscordSocketClient _client;
        private readonly HoggitLoggerService _hoggitLogger;
        private readonly ILiteCollection<BanNotifyConfig> _configDb;
        private readonly BanNotifyConfig _config;

        public BanNotifyService(ILogger<BanNotifyService> logger, LiteDatabase db, DiscordSocketClient client, HoggitLoggerService hoggitLogger)
        {
            _logger = logger;
            _client = client;
            _hoggitLogger = hoggitLogger;
            _configDb = db.GetCollection<BanNotifyConfig>("BanNotifyService");
            _config = _configDb.FindAll().FirstOrDefault() ?? new BanNotifyConfig();

            _client.UserBanned += _client_UserBanned;
            _client.GuildMemberUpdated += _client_GuildMemberUpdated;
        }

        private async Task _client_GuildMemberUpdated(Cacheable<SocketGuildUser, ulong> oldUserCache, SocketGuildUser newUser)
        {
            if (!IsGuildEnabled(newUser.Guild.Id) || !oldUserCache.HasValue)
                return;

            var guild = newUser.Guild;
            var oldUser = oldUserCache.Value;

            if (!newUser.TimedOutUntil.HasValue || (oldUser.TimedOutUntil.HasValue && newUser.TimedOutUntil.Value == oldUser.TimedOutUntil.Value))
                return;

            if (newUser.TimedOutUntil.Value.ToUniversalTime() < DateTimeOffset.UtcNow)
                return;

            string reason = null;
            IUser byUser = null;

            var duration = newUser.TimedOutUntil.Value.ToUniversalTime() - DateTimeOffset.UtcNow;

            string humanTime;
            if (duration.TotalHours > 2)
                humanTime = duration.Humanize(5, maxUnit: TimeUnit.Year, minUnit: TimeUnit.Hour);
            else if (duration.TotalMinutes <= 1)
                humanTime = duration.Humanize(1, maxUnit: TimeUnit.Minute, minUnit: TimeUnit.Second);
            else
                humanTime = duration.Humanize(2, maxUnit: TimeUnit.Hour, minUnit: TimeUnit.Minute);

            if (guild.CurrentUser.GuildPermissions.ViewAuditLog)
            {
                var logEntry = (await guild.GetAuditLogsAsync(1, actionType: ActionType.MemberUpdated).FlattenAsync())
                    .FirstOrDefault(x => x.Data is MemberUpdateAuditLogData updateData && updateData.Target.Id == newUser.Id);

                if (logEntry is not null)
                {
                    byUser = logEntry.User;
                    if (string.IsNullOrWhiteSpace(reason) && !string.IsNullOrWhiteSpace(logEntry.Reason))
                        reason = logEntry.Reason.Trim();
                }
            }

            var outText = $"TIMEOUT: {newUser.Mention} ({newUser.ToString()}) timed out for {humanTime}";

            if (!string.IsNullOrWhiteSpace(reason))
                outText += $" reason: [{Format.Sanitize(reason)}]";

            if (byUser is not null && byUser.Id != guild.CurrentUser.Id)
                outText += $" by [{byUser.Mention} ({byUser.ToString()})]";

            await _hoggitLogger.SendLogMessage(outText + "!");
        }

        private async Task _client_UserBanned(SocketUser user, SocketGuild guild)
        {
            if (!IsGuildEnabled(guild.Id))
                return;

            _logger.LogInformation($"User banned: {user.Id}");


            string outText = $"USER BANNED: {user.Mention} ({user.ToString()}) was banned";
            string reason = null;
            IUser byUser = null;

            if (guild.CurrentUser.GuildPermissions.BanMembers)
            {
                var ban = await guild.GetBanAsync(user);
                if (ban is not null && ban.Reason is not null)
                {
                    reason = ban.Reason.Trim();
                }
            }

            if (guild.CurrentUser.GuildPermissions.ViewAuditLog)
            {
                var logEntry = (await guild.GetAuditLogsAsync(1, actionType: ActionType.Ban).FlattenAsync()).FirstOrDefault();
                if (logEntry.Data is BanAuditLogData banData && banData.Target.Id == user.Id)
                {
                    byUser = logEntry.User;
                    if (string.IsNullOrWhiteSpace(reason) && !string.IsNullOrWhiteSpace(logEntry.Reason))
                        reason = logEntry.Reason.Trim();
                }
            }

            if (!string.IsNullOrWhiteSpace(reason))
                outText += $" for [{reason}]";

            if (byUser is not null)
                outText += $" by [{byUser.Mention} ({byUser.ToString()})]";

            await _hoggitLogger.SendLogMessage(outText + "!");
        }

        public bool EnableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Add(guildId);

            if (result) Save();

            return result;
        }

        public bool DisableGuild(ulong guildId)
        {
            var result = _config.EnabledGuilds.Remove(guildId);

            if (result) Save();

            return result;
        }

        public bool IsGuildEnabled(ulong guildId)
            => _config.EnabledGuilds.Contains(guildId) && guildId == DoghouseService.Guild;


        public void Save()
        {
            if (!_configDb.Update(_config))
            {
                _configDb.Insert(_config);
            }

            Update();
        }

        /// <summary>
        /// Updates the prefix dictionary to the most recently stored version
        /// </summary>
        private void Update()
        {
            var db = _configDb.FindAll().FirstOrDefault();
            if (db == null)
            {
                throw new NullReferenceException("The BanNotifyService update action was unable to find a valid BanNotifyConfig document");
            }

            _config.EnabledGuilds = db.EnabledGuilds;
        }
    }

    public class BanNotifyConfig
    {
        public HashSet<ulong> EnabledGuilds { get; set; } = new HashSet<ulong>();

        [BsonId]
        public int _discarded { get; set; } = 1;
    }

}
