using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LazyDiscordBot.Attributes;
using LazyDiscordBot.ExtensionClasses;
using LazyDiscordBot.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LazyDiscordBot.Services
{
    [RequireIntents(GatewayIntents.GuildMessages | GatewayIntents.DirectMessages | GatewayIntents.GuildMessageReactions | GatewayIntents.DirectMessageReactions | GatewayIntents.MessageContent)]
    public class CommandHandlingService : IBotService
    {
        private readonly CommandService _commands;
        private readonly DiscordSocketClient _discord;
        private readonly IServiceProvider _services;
        private readonly PrefixService _prefixService;
        private readonly ILogger<CommandHandlingService> _logger;
        private BotConfig _config;
        private readonly WordService _wordService;
        private readonly LogWebhookClient _logWebhook;

        public CommandHandlingService(IServiceProvider services, CommandService commands, DiscordSocketClient discord,
            PrefixService prefixService, ILogger<CommandHandlingService> logger, IOptions<BotConfig> config, WordService wordService,
            LogWebhookClient logWebhook)
        {
            _wordService = wordService;
            _logWebhook = logWebhook;
            _config = config.Value;
            _commands = commands;
            _discord = discord;
            _services = services;
            _prefixService = prefixService;
            _logger = logger;
            _commands.Log += Program.LogInfo;
            _commands.CommandExecuted += CommandExecutedAsync;
            _discord.MessageReceived += MessageReceivedAsync;
        }

        private Task SendLogMessage(ICommandContext context, IResult result)
        {
            var embed = new EmbedBuilder()
                .WithAuthor(context.User)
                .WithTitle($"Executed command")
                .WithDescription(context.Message.ToString());

            if (context.Guild is not null)
                embed.WithFooter($"In {context.Guild.Name} - #{context.Channel.Name}");
            else if (context.Channel is IDMChannel)
                embed.WithFooter("In DMs");
            else
                embed.WithFooter("In an unknown channel");

            if (result.IsSuccess)
                embed.WithColor(Color.Green);
            else
            {
                embed.WithColor(Color.Red);

                if (result is ExecuteResult execResult && execResult.Exception != null)
                {
                    embed.AddField("Error", $"{execResult.Exception.GetType()}\n--> {execResult.Exception.Message}");

                    if (execResult.Exception.InnerException != null)
                        embed.AddField("Inner Exception", $"{execResult.Exception.InnerException.GetType()}\n--> {execResult.Exception.InnerException.Message}");
                }

                else if (result is PreconditionGroupResult groupResult && !groupResult.IsSuccess)
                {
                    var commandResult = groupResult.PreconditionResults.FirstOrDefault(pr => !pr.IsSuccess && !string.IsNullOrWhiteSpace(pr.ErrorReason));
                    if (commandResult != null)
                    {
                        embed.AddField("Error", commandResult.ErrorReason);
                    }
                }

                else if (!string.IsNullOrWhiteSpace(result.ErrorReason))
                    embed.AddField("Error", result.ErrorReason);
            }

            try
            {
                return _logWebhook.SendMessageAsync(string.Empty, embeds: new[] { embed.Build() }, 
                    username: _discord.CurrentUser.Username, avatarUrl: _discord.CurrentUser.GetAvatarUrl() ?? _discord.CurrentUser.GetDefaultAvatarUrl());
            }
            catch (Exception)
            {
                if (_config.LogChannel != null)
                    return ((SocketTextChannel)_discord.GetChannel((ulong)_config.LogChannel))
                        .SendMessageAsync(string.Empty, embed: embed.Build());

                return null;
            }
        }

        public async Task InitializeAsync()
        {
            _logger.LogInformation("Starting Command Handling Service");

            _commands.AddTypeReaders();
            _wordService.Initialize();
            await _prefixService.InitializeAsync();
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        public async Task MessageReceivedAsync(SocketMessage rawMessage)
        {
            // Ignore system messages, or messages from other bots
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            // This value holds the offset where the prefix ends
            var argPos = 0;
            if (!message.HasMentionPrefix(_discord.CurrentUser, ref argPos) &&
                !message.HasStringPrefix(_prefixService.GetPrefix((message.Channel as IGuildChannel)?.Guild.Id ?? 0), ref argPos)
            )
            {
                return;
            }

            var context = new SocketCommandContext(_discord, message);
            await _commands.ExecuteAsync(context, argPos,
                _services); // we will handle the result in CommandExecutedAsync
        }

        public async Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, IResult result)
        {
            // command is unspecified when there was a search failure (command not found); we don't care about these errors
            if (!command.IsSpecified)
                return;

            await SendLogMessage(context, result);

            // the command was successful, we don't care about this result, unless we want to log that a command succeeded.
            if (result.IsSuccess && string.IsNullOrWhiteSpace(result.ErrorReason))
                return;

            if (result.IsSuccess)
            {
                await context.Channel.SendMessageAsync(result.ToString());
                return;
            }

            if (string.IsNullOrWhiteSpace(result.ErrorReason))
                return;

            if (result is ExecuteResult execResult && execResult.Exception != null)
            {
                _logger.LogError(execResult.Exception, $"An exception was thrown in command: \"{command.GetValueOrDefault()?.Name}\"");
                await context.Channel.SendMessageAsync("Error: An exception was thrown in the command");
                return;
            }

            if (result is PreconditionGroupResult groupResult && !groupResult.IsSuccess)
            {
                var commandResult = groupResult.PreconditionResults.FirstOrDefault(pr => !pr.IsSuccess && !string.IsNullOrWhiteSpace(pr.ErrorReason));
                if (commandResult != null)
                    await context.Channel.SendMessageAsync($"Error: {commandResult.ErrorReason}");

                return;
            }

            // the command failed, let's notify the user that something happened.
            await context.Channel.SendMessageAsync($"Error: {result}");
        }
    }
}
