﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Discord.Commands;
using LazyDiscordBot.Models;
using Newtonsoft.Json;

namespace LazyDiscordBot.TypeReaders
{
    public class ServerInfoTypeReader : TypeReader
    {
        private const string InvalidServer = "Unknown server";

        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string serverName, IServiceProvider services)
        {
            var servers = ServerInfo.GetAll(context.Guild.Id);

            var server = servers.Get(serverName);

            return server != null && server.StatsUrl != null
                ? Task.FromResult(TypeReaderResult.FromSuccess(server))
                : Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, InvalidServer));
        }
    }
}
