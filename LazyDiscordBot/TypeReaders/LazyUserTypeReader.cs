﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Rest;
using Discord.WebSocket;

namespace LazyDiscordBot.TypeReaders
{
    public class LazyUserTypeReader<T> : UserTypeReader<T> where T : class, IUser
    {
        public override async Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            var result = await base.ReadAsync(context, input, services);

            if (!result.IsSuccess && context.Client is DiscordSocketClient client)
            {
                var results = new Dictionary<ulong, TypeReaderValue>();
                if (MentionUtils.TryParseUser(input, out var id))
                {
                    if (context.Guild != null)
                    {
                        RestGuildUser restGuildUser = await client.Rest.GetGuildUserAsync(context.Guild.Id, id).ConfigureAwait(false);
                        AddResult(results, restGuildUser as T, 1.00f);
                    }
                    else
                        AddResult(results, await client.Rest.GetUserAsync(id).ConfigureAwait(false) as T, 1.00f);
                }

                if (ulong.TryParse(input, NumberStyles.None, CultureInfo.InvariantCulture, out id))
                {
                    if (context.Guild != null)
                        AddResult(results, await client.Rest.GetGuildUserAsync(context.Guild.Id, id).ConfigureAwait(false) as T, 0.90f);
                    else
                        AddResult(results, await client.Rest.GetUserAsync(id).ConfigureAwait(false) as T, 0.90f);
                }

                if (results.Count > 0)
                    result = TypeReaderResult.FromSuccess(results.Values.ToImmutableArray());
                else
                    result = TypeReaderResult.FromError(CommandError.ObjectNotFound, "User not found.");
            }

            return result;
        }

        private void AddResult(Dictionary<ulong, TypeReaderValue> results, T user, float score)
        {
            if (user != null && !results.ContainsKey(user.Id))
                results.Add(user.Id, new TypeReaderValue(user, score));
        }
    }
}
