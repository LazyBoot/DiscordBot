using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord.Commands;

namespace LazyDiscordBot.TypeReaders;

public partial class LazyDateTimeOffsetTypeReader : TypeReader
{
    private const string InvalidTime = "Failed to parse DateTimeOffset.";

    [GeneratedRegex(@"(\d?\d)(\d\d)(z|[\-\+]\d?\d)$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant)]
    private static partial Regex Pattern();


    public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
    {
        if (Pattern().IsMatch(input))
            input = Pattern().Replace(input, @"$1:$2$3");

        return DateTimeOffset.TryParse(input, out var result)
            ? Task.FromResult(TypeReaderResult.FromSuccess(result))
            : Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, InvalidTime));
    }
}
