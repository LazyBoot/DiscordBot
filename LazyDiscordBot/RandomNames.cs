﻿using System;
using System.Collections.Generic;
using System.Linq;
using Discord.Commands;

namespace LazyDiscordBot
{
    public class RandomNames
    {
        public static Random Random = new Random();

        //public static async Task DrawRandomName(SocketMessage message)
        //{
        //    var names = message.Content.ToLowerInvariant().Substring(8).Split(" ");
        //    var randomIndex = Random.Next(names.Length);
        //    var randomName = names[randomIndex];

        //    await message.Channel.SendMessageAsync($"Random name:{randomName}");
        //}
        public static string DrawLotteryName(string[] inputs)
        {
            var names = new List<string>();
            if (inputs.Length % 2 != 0) return "You need to enter odds for all entries";
            for (var i = 0; i < inputs.Length; i += 2)
            {
                var name = inputs[i];
                if (!ushort.TryParse(inputs[i + 1], out var odds))
                    return $"Invalid number, make sure odds is a number between {ushort.MinValue} and {ushort.MaxValue}!";
                for (var j = 0; j < odds; j++)
                {
                    names.Add(name);
                }
            }

            var randomIndex = Random.Next(names.Count);
            var randomName = names[randomIndex];

            return $"The winner is: :tada: {randomName} :tada:   Go claim your prize :flag_bv:  :birthday:";
        }

        public static string DrawLotteryFromNames(SocketCommandContext context, string[] namesIn)
        {
            var mentioned = context.Message.MentionedRoles;

            List<string> namesList;

            if (namesIn == null || namesIn.Length == 0)
                return "No names provided";
            else
                namesList = new List<string>(namesIn);

            if (mentioned != null)
            {
                var users = mentioned.SelectMany(role => role.Members).ToList();

                var names = (from user in users.Distinct()
                             select user).ToList();

                namesList.RemoveAll(n => mentioned.Any(r => n == r.Mention));
                foreach (var name in names)
                {
                    if (namesList.Contains($"<@{name.Id}>")) continue;
                    namesList.Add(name.Mention);
                }
            }

            var allNames = namesList.Distinct().ToList();
            var randomIndex = Random.Next(allNames.Count);
            var randomName = allNames[randomIndex];
            allNames.Remove(randomName);

            return $"The winner is: :tada: {randomName} :tada:" +
                   (allNames.Count > 0 ? $"{Environment.NewLine}Commiserations to: {string.Join(", ", allNames)}" : string.Empty);
        }

    }

}
