using System;

namespace LazyDiscordBot.ExtensionClasses;

internal static class DateTimeExtensions
{
    internal static DateTime DateTimeFromUnixTime(this int timestamp)
    {
        return DateTime.UnixEpoch.AddSeconds(timestamp);
    }

    internal static DateTime DateTimeFromUnixTime(this string timestamp)
    {
        var time = int.Parse(timestamp);
        return DateTimeFromUnixTime(time);
    }
}
