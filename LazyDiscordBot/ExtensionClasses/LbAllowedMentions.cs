using Discord;

namespace LazyDiscordBot.ExtensionClasses
{
    class LbAllowedMentions : AllowedMentions
    {
        public static readonly AllowedMentions ReplyOnly = new AllowedMentions { MentionRepliedUser = true, AllowedTypes = AllowedMentionTypes.None };
    }
}