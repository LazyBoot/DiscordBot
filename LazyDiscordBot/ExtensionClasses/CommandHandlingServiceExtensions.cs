using System;
using Discord;
using Discord.Commands;
using LazyDiscordBot.Models;
using LazyDiscordBot.TypeReaders;

namespace LazyDiscordBot.ExtensionClasses
{
    internal static class CommandHandlingServiceExtensions
    {
        internal static void AddTypeReaders(this CommandService service)
        {
            service.AddTypeReader<ServerInfo>(new ServerInfoTypeReader());
            service.AddTypeReader<IUser>(new LazyUserTypeReader<IUser>(), true);
            service.AddTypeReader<IGuildUser>(new LazyUserTypeReader<IGuildUser>(), true);
            service.AddTypeReader<DateTimeOffset>(new LazyDateTimeOffsetTypeReader(), true);
        }
    }
}
