﻿using System.Threading.Tasks;
using Discord.Commands;
using LazyDiscordBot.Models;

namespace LazyDiscordBot.ExtensionClasses
{
    public static class RuntimeResultExtensions
    {
        public static Task<RuntimeResult> ToTask(this RuntimeResult result)
            => Task.FromResult<RuntimeResult>(result);
    }
}
