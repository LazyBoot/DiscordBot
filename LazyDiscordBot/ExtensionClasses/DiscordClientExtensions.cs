﻿using Discord;

namespace LazyDiscordBot.ExtensionClasses
{
    public static class DiscordClientExtensions
    {
        internal static bool IsConnected(this IDiscordClient client)
        {
            return client.ConnectionState == ConnectionState.Connected;
        }
    }
}
