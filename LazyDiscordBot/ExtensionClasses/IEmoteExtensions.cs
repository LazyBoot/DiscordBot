﻿using Discord;

namespace LazyDiscordBot.ExtensionClasses
{
    public static class IEmoteExtensions
    {
        internal static IEmote GetEmote(this string inEmote)
        {
            if (Emote.TryParse(inEmote, out Emote emote))
                return emote;

            var emoji = new Emoji(inEmote);
            return emoji;
        }
    }
}
