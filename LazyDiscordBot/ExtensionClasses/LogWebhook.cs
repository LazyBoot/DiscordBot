﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;
using Discord.Webhook;
using LazyDiscordBot.Models;
using Microsoft.Extensions.Options;

namespace LazyDiscordBot.ExtensionClasses
{
    public class LogWebhookClient : DiscordWebhookClient
    {
        public LogWebhookClient(IWebhook webhook) : base(webhook)
        {
        }

        public LogWebhookClient(string webhookUrl) : base(webhookUrl)
        {
        }

        public LogWebhookClient(ulong webhookId, string webhookToken) : base(webhookId, webhookToken)
        {
        }

        public LogWebhookClient(IWebhook webhook, DiscordRestConfig config) : base(webhook, config)
        {
        }

        public LogWebhookClient(string webhookUrl, DiscordRestConfig config) : base(webhookUrl, config)
        {
        }

        public LogWebhookClient(ulong webhookId, string webhookToken, DiscordRestConfig config) : base(webhookId, webhookToken, config)
        {
        }
    }
}
