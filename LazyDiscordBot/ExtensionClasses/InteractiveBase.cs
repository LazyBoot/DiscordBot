﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Fergun.Interactive;

namespace LazyDiscordBot.ExtensionClasses
{
    public class InteractiveBase : ModuleBase<SocketCommandContext>
    {
        public InteractiveService Interactive { get; set; }

        public Task ReplyAndDeleteAsync(string message, TimeSpan timeout)
        {
            return Interactive.DelayedSendMessageAndDeleteAsync(Context.Channel, deleteDelay: timeout, text: message);
        }
    }
}
