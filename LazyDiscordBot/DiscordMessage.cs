﻿// ReSharper disable InconsistentNaming
namespace LazyDiscordBot
{
    public class DiscordMessage
    {
        public string content;
        public string username;
        public string avatar_url;
    }
}