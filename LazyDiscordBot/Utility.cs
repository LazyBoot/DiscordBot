﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using LiteDB;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LazyDiscordBot
{
    static class Utility
    {
        public static string[] GetAllLocalIPv4()
        {
            var ipAddrList = new List<string>();
            foreach (var item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if ((item.NetworkInterfaceType == NetworkInterfaceType.Ethernet || item.NetworkInterfaceType == NetworkInterfaceType.Wireless80211) && item.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (var ip in item.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddrList.Add(ip.Address.ToString());
                        }
                    }
                }
            }
            return ipAddrList.ToArray();
        }

        public static async Task<bool> Update(SocketCommandContext context, IHostApplicationLifetime hostApp)
        {
            var updateError = false;
            var db = Program.UpdateStatus;
            var status = db.FindById(1);
            if (status == null)
            {
                status = new UpdateStatus() { Id = 1, WasUpdate = false };
                db.Insert(1, status);
            }

            var message = await context.Channel.SendMessageAsync("Starting update...");

            string updateFile = Path.Combine(Program.ProgramPath, "Data", "update", @"startUpdate");
            try
            {
                status.WasUpdate = true;
                status.GuildId = context.Guild.Id;
                status.ChannelId = context.Channel.Id;
                db.Update(1, status);

                File.WriteAllLines(updateFile, new[] { "start update" });

                hostApp.StopApplication();
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.Error(string.Empty, ex);
                updateError = true;
            }
            catch (UnauthorizedAccessException ex)
            {
                Log.Error(string.Empty, ex);
                updateError = true;
            }

            if (updateError)
            {
                await message.ModifyAsync(m => m.Content = "Error requesting update.");
                status.Clear();
                db.Update(1, status);
                await context.Client.SetStatusAsync(UserStatus.Online);
                await context.Client.SetGameAsync(null);
            }

            return true;
        }

        public static IEnumerable<Type> GetBotServices<T>()
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(t => t.GetInterface(typeof(T).Name) is not null && !t.IsAbstract);
        }



        public class UpdateStatus
        {
            [BsonId]
            public int Id { get; set; }
            public bool WasUpdate { get; set; }
            public ulong GuildId { get; set; }
            public ulong ChannelId { get; set; }

            public void Clear()
            {
                WasUpdate = false;
                GuildId = 0;
                ChannelId = 0;
            }
        }

    }
}
