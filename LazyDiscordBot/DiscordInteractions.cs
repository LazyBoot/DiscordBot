﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using LazyDiscordBot.Interfaces_Abstracts;
using LazyDiscordBot.Models;
using LazyDiscordBot.Services;
using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LazyDiscordBot
{
    public class DiscordInteractions : IHostedService
    {
        public DiscordSocketClient Client; //= new DiscordSocketClient();
        private readonly BotConfig _tokens;
        private readonly CommandHandlingService _command;
        private readonly IServiceProvider _services;
        private readonly ILogger<DiscordInteractions> _logger;

        public DiscordInteractions(DiscordSocketClient client, IOptions<BotConfig> tokens, IServiceProvider services, CommandHandlingService command,
                                   ILogger<DiscordInteractions> logger, LiteDatabase db)
        {
            _services = services;
            _logger = logger;
            _command = command;
            Client = client;
            _tokens = tokens.Value;

            Program.UpdateStatus = db.GetCollection<Utility.UpdateStatus>("UpdateStatus");
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Client.Log += Program.LogInfo;

            Client.Ready += Ready;
            Client.JoinedGuild += Client_JoinedGuild;
            Client.LeftGuild += Client_LeftGuild;

            await Client.LoginAsync(TokenType.Bot, _tokens.BotToken);
            await Client.StartAsync();

            await _command.InitializeAsync();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Exiting.");

            foreach (var serviceType in Utility.GetBotServices<IStopOnShutdown>())
            {
                await (_services.GetRequiredService(serviceType) as IStopOnShutdown)?.Stop();
            }

            await Client.SetStatusAsync(UserStatus.Invisible);
            await Client.SetGameAsync(null);
            await Client.StopAsync();
            await Client.LogoutAsync();
        }

        private Task Client_JoinedGuild(SocketGuild guild)
        {
            return SendGuildEmbed(guild, false);
        }

        private Task Client_LeftGuild(SocketGuild guild)
        {
            return SendGuildEmbed(guild, true);
        }

        private Task<IUserMessage> SendGuildEmbed(IGuild guild, bool left = false)
        {
            if (_tokens.LogChannel == null)
                return Task.FromResult<IUserMessage>(null);

            var embed = new EmbedBuilder()
                .WithAuthor(guild.ToString(), guild.IconUrl);

            if (left)
            {
                embed = embed
                        .WithTitle("Left Guild")
                        .WithColor(Color.Red);
            }
            else
            {
                embed = embed
                        .WithTitle("Joined Guild")
                        .WithColor(Color.Green);
            }

            var owner = Client.GetApplicationInfoAsync().GetAwaiter().GetResult().Owner;


            return ((ITextChannel)Client.GetChannel(_tokens.LogChannel.Value))
                .SendMessageAsync(owner.Mention, embed: embed.Build());
        }

        private async Task Ready()
        {
            try {
                try
                {
                    // Init Services
                    foreach (var serviceType in Utility.GetBotServices<IRunOnStartup>())
                    {
                        await (_services.GetRequiredService(serviceType) as IRunOnStartup)?.Initialize();
                    }

                    Client.Ready -= Ready;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error initializing services");
                }

                try
                {
                    // Reset status
                    await Client.SetStatusAsync(UserStatus.Online);
                    await Client.SetGameAsync(null);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error resetting status");
                }

                // Send online message
                var updateStatus = Program.UpdateStatus.FindById(1);
                if (updateStatus != null && updateStatus.WasUpdate)
                {
                    try
                    {
                        RestGuild guild = await Client.Rest.GetGuildAsync(updateStatus.GuildId);
                        RestTextChannel channel = await guild.GetTextChannelAsync(updateStatus.ChannelId);
                        await channel.SendMessageAsync("Update Complete, back online");
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Error trying to send online status");
                    }

                    updateStatus.Clear();
                    Program.UpdateStatus.Update(1, updateStatus);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in ready handler");
            }
        }

    }
}
