using System;
using Discord;

namespace LazyDiscordBot.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RequireIntents : Attribute
    {
        public GatewayIntents Intents { get; private set; }

        public RequireIntents(GatewayIntents intents)
        {
            Intents = intents;
        }
    }
}